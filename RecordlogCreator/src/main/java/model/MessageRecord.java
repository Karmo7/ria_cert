package model;

public class MessageRecord {

	private long time;
	private String queryId;
	private String message;
	private String signature;
	private String hashChain;
	private String hashChainResult;
	private String signatureHash;
	private String timestamp;
	private String timestampHashChain;
	private boolean response;
	private String memberClass;
	private String memberCode;
	private String subsystemCode;

	public MessageRecord() {

	}

	public long getTime() {
		return time;
	}


	public String getQueryId() {
		return queryId;
	}


	public String getMessage() {
		return message;
	}


	public String getSignature() {
		return signature;
	}


	public String getHashChain() {
		return hashChain;
	}


	public String getHashChainResult() {
		return hashChainResult;
	}


	public String getSignatureHash() {
		return signatureHash;
	}


	public String getTimestamp() {
		return timestamp;
	}


	public String getTimestampHashChain() {
		return timestampHashChain;
	}


	public boolean isResponse() {
		return response;
	}


	public String getMemberClass() {
		return memberClass;
	}


	public String getMemberCode() {
		return memberCode;
	}


	public String getSubsystemCode() {
		return subsystemCode;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public void setHashChain(String hashChain) {
		this.hashChain = hashChain;
	}

	public void setHashChainResult(String hashChainResult) {
		this.hashChainResult = hashChainResult;
	}

	public void setSignatureHash(String signatureHash) {
		this.signatureHash = signatureHash;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setTimestampHashChain(String timestampHashChain) {
		this.timestampHashChain = timestampHashChain;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public void setMemberClass(String memberClass) {
		this.memberClass = memberClass;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public void setSubsystemCode(String subsystemCode) {
		this.subsystemCode = subsystemCode;
	}

	@Override
	public String toString() {
		return "MessageRecord [time=" + time + ", queryId=" + queryId + ", message=" + message + ", signature="
				+ signature + ", hashChain=" + hashChain + ", hashChainResult=" + hashChainResult + ", signatureHash="
				+ signatureHash + ", timestamp=" + timestamp + ", timestampHashChain=" + timestampHashChain
				+ ", response=" + response + ", memberClass=" + memberClass + ", memberCode=" + memberCode
				+ ", subsystemCode=" + subsystemCode + "]";
	}


}
