package model;

/**
 * Created by Marten on 6.04.2016.
 */
public class MessageRecordDuple {

	private MessageRecord request;
	private MessageRecord response;

	public MessageRecord getRequest() {
		return request;
	}

	public void setRequest(MessageRecord request) {
		this.request = request;
	}

	public MessageRecord getResponse() {
		return response;
	}

	public void setResponse(MessageRecord response) {
		this.response = response;
	}
}
