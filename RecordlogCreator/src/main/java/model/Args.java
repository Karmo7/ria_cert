package model;

import java.util.Random;

/**
 * Created by Marten on 9.04.2016.
 */
public class Args {

	public static final Integer DEFAULT_FREQUENCY = 1000;
	public static final String DEFAULT_URL = "jdbc:postgresql://localhost:5432/messagelog";
	public static final String DEFAULT_USERNAME = "postgres";
	public static final String DEFAULT_PASSWORD = "postgres";

	private static final String RANDOM = "random";

	private Integer frequency = DEFAULT_FREQUENCY;
	private String url = DEFAULT_URL;
	private String username = DEFAULT_USERNAME;
	private String password = DEFAULT_PASSWORD;

	private boolean spamMode = false;

	private String[] args;


	private Args(String[] args) {
		this.args = args;
		validate();
		if (!isEmpty()) {
			parse();
		}
	}

	public static Args parse(String[] args) {
		return new Args(args);
	}

	private void validate() {
		if (args == null) {
			throw new IllegalArgumentException("args cannot be null!");
		}
	}

	private void parse() {
		if (args.length == 1) {
			parseFrequency();
		} else if (args.length > 1) {
			parseArgs(args);
		}
	}

	private void parseArgs(String[] args) {
		for (int i = 0; i < args.length; i += 2) {
			parseArg(args, i);
		}
	}

	private void parseArg(String[] args, int index) {
		int argslength = args.length;
		int nextElementIndex = index + 1;
		if (elementExists(argslength, nextElementIndex)) {
			if (isFrequencyFlag(args[index]) && elementExists(argslength, index)) {
				parseFrequency(nextElementIndex);
			} else if (isSpamMode(args[index]) && elementExists(argslength, index)) {
				spamMode = parseSpamModeFlag(args[nextElementIndex]);
			} else if (isUsernameFlag(args[index]) && elementExists(argslength, index)) {
				username = args[nextElementIndex];
			} else if (isPasswordFlag(args[index]) && elementExists(argslength, index)) {
				password = args[nextElementIndex];
			} else if (isHostFlag(args[index])) {
				url = DEFAULT_URL.replace("localhost:5432", args[nextElementIndex]);
			} else {
				throw new IllegalArgumentException("Unknown flag: " + args[index]);
			}
		} else {
			throw new NumberFormatException();
		}
	}

	private boolean parseSpamModeFlag(String arg) {
		return new Boolean(arg);
	}

	private boolean isSpamMode(String arg) {
		return "-spam".equalsIgnoreCase(arg);
	}

	private boolean isHostFlag(String arg) {
		return "-h".equalsIgnoreCase(arg);
	}

	private boolean isPasswordFlag(String arg) {
		return "-p".equalsIgnoreCase(arg);
	}

	private boolean isUsernameFlag(String arg) {
		return "-u".equals(arg);
	}

	private boolean isFrequencyFlag(String arg) {
		return "-f".equals(arg);
	}

	private boolean elementExists(int argslength, int i) {
		return i < argslength;
	}

	private void parseFrequency() {
		parseFrequency(0);
	}

	private void parseFrequency(int index) {
		if (RANDOM.equalsIgnoreCase(args[index])) {
			new Thread(this::run).start();
		} else {
			frequency = Integer.parseInt(args[index]);
		}
	}

	private void run() {
		try {
			while (true) {
				int high = 500;
				int low = 10;
				frequency = new Random().nextInt(high - low) + low;
				System.out.println("run frequency: " + frequency);
				Thread.sleep(10000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public Integer getFrequency() {
		return frequency;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}


	public String getUrl() {
		return url;
	}

	public boolean isSpamMode() {
		return spamMode;
	}

	public boolean isEmpty() {
		return args == null || args.length == 0;
	}
}
