package model;

/**
 * Created by Marten on 6.04.2016.
 */
public class MessageDuple {

	private String requestMessage;
	private String responseMessage;

	public String getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
}
