package service.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Marten on 7.04.2016.
 */
public class MessageLoader {

	private MessageLoader() {
	}

	public static String load(String fileName) {
		if (fileName == null) {
			throw new IllegalArgumentException("fileName cannot be null!");
		}
		Path file = Paths.get(fileName);
		try {
			return new String(Files.readAllBytes(file));
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Technical error: cannot find file: " + fileName);
		}
	}

}
