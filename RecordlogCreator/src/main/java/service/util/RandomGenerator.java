package service.util;

import service.creator.MessageCreatorFactory;

import java.util.Random;

/**
 * Created by Marten on 7.04.2016.
 */
public class RandomGenerator {
	Random random;

	public RandomGenerator() {
		random = new Random();
	}

	public Integer integerBetween(int low, int high) {
		return random.nextInt(high - low) + low;
	}

	public Integer nextTemplateNumber() {
		return random.nextInt(MessageCreatorFactory.MAX_ID);
	}

	public Integer sex() {
		return integerBetween(3, 5);
	}

	public Integer day() {
		return integerBetween(1, 29);
	}

	public Integer month() {
		return integerBetween(1, 13);
	}

	public Integer year() {
		return integerBetween(0, 100);
	}
}
