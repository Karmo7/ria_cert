package service.creator;

import model.Isikukood;
import model.MessageDuple;
import model.MessageRecordDuple;
import service.util.MessageLoader;

/**
 * Created by Marten on 7.04.2016.
 */
public class IdMessageCreator extends MessageCreator {
	private static final String ID_REQUEST_TEMPLATE = "templates/id_request_template";
	private static final String ID_RESPONSE_TEMPLATE = "templates/id_response_template";
	private static final String ISIKUKOOD = "$ISIKUKOOD$";
	private static final String SUGU = "$SUGU$";
	private static final String VANUS = "$VANUS$";

	private Isikukood isikukood;

	@Override
	public MessageDuple create(MessageRecordDuple recordDuple) {
		MessageDuple messageDuple = new MessageDuple();
		messageDuple.setRequestMessage(createRequestMessage(recordDuple));
		messageDuple.setResponseMessage(createResponseMessage(recordDuple));

		return messageDuple;
	}

	private String createRequestMessage(MessageRecordDuple recordDuple) {
		String requestMsg = MessageLoader.load(ID_REQUEST_TEMPLATE);
		requestMsg = setCommonAttributes(recordDuple, requestMsg);
		return createRequestMessage(requestMsg);
	}

	private String createRequestMessage(String requestMsg) {
		isikukood = new Isikukood(IsikukoodCreator.create());
		return requestMsg.replace(ISIKUKOOD, isikukood.getIsikukood());
	}

	private String createResponseMessage(MessageRecordDuple recordDuple) {
		String responseMsg = MessageLoader.load(ID_RESPONSE_TEMPLATE);
		responseMsg = setCommonAttributes(recordDuple, responseMsg);
		return createResponseMessage(responseMsg);
	}

	private String createResponseMessage(String responseMsg) {
		responseMsg = responseMsg.replace(ISIKUKOOD, isikukood.getIsikukood());
		responseMsg = responseMsg.replace(SUGU, isikukood.getSex());
		responseMsg = responseMsg.replace(VANUS, String.valueOf(isikukood.getAge()));
		return responseMsg;
	}
}
