package service.creator;

import model.MessageDuple;
import model.MessageRecordDuple;
import service.util.MessageLoader;
import service.util.RandomGenerator;

/**
 * Created by Marten on 6.04.2016.
 */
public class SquareMessageCreator extends MessageCreator {

	private static final String SQUARED_REQUEST_TEMPLATE = "templates/squared_request_template";
	private static final String SQUARED_RESPONSE_TEMPLATE = "templates/squared_response_template";

	private static final String REQUEST_CONTENT = "$REQUEST_CONTENT$";
	private static final String RESPONSE_CONTENT = "$RESPONSE_CONTENT$";

	private Integer requestSeed;

	@Override
	public MessageDuple create(MessageRecordDuple recordDuple) {
		MessageDuple messageDuple = new MessageDuple();
		messageDuple.setRequestMessage(createRequestMessage(recordDuple));
		messageDuple.setResponseMessage(createResponseMessage(recordDuple));
		return messageDuple;
	}

	private String createRequestMessage(MessageRecordDuple messageDuple) {
		String requestMsg = MessageLoader.load(SQUARED_REQUEST_TEMPLATE);
		requestMsg = setCommonAttributes(messageDuple, requestMsg);
		requestMsg = setRequestContent(requestMsg);
		return requestMsg;
	}

	private String setRequestContent(String requestMsg) {
		requestSeed = new RandomGenerator().integerBetween(1, 8);
		return requestMsg.replace(REQUEST_CONTENT, requestSeed.toString());
	}

	private String createResponseMessage(MessageRecordDuple messageDuple) {
		String responseMsg = MessageLoader.load(SQUARED_RESPONSE_TEMPLATE);
		responseMsg = setCommonAttributes(messageDuple, responseMsg);
		responseMsg = setResponseContent(responseMsg);
		return responseMsg;
	}

	private String setResponseContent(String responseMsg) {
		return responseMsg.replace(RESPONSE_CONTENT, String.valueOf(Math.pow(requestSeed.doubleValue(), 2.0)));
	}

}
