package service.creator;

import model.MessageDuple;
import model.MessageRecordDuple;
import service.util.MessageLoader;

/**
 * Created by Marten on 10.05.2016.
 */
public class SpamMessageCreator extends MessageCreator {

    private static final String SPAM_MESSAGE = "templates/spam_message";

    @Override
    public MessageDuple create(MessageRecordDuple recordDuple) {
        MessageDuple messageDuple = new MessageDuple();
        String spamMessage = MessageLoader.load(SPAM_MESSAGE);
        messageDuple.setRequestMessage(spamMessage);
        messageDuple.setResponseMessage(spamMessage);
        return messageDuple;
    }

}
