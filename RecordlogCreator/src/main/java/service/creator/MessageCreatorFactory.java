package service.creator;

import service.util.RandomGenerator;

/**
 * Created by Marten on 7.04.2016.
 */
public class MessageCreatorFactory {

	public static final int MAX_ID = 3;

	private static final int SQUARED = 0;
	private static final int ID = 1;
	private static final int FAULT = 2;

	private MessageCreatorFactory() {
	}

	public static MessageCreator get() {
		return get(false);
	}

	public static MessageCreator get(boolean spamMode) {
		if (spamMode) {
			return getSpamMessageCreator();
		} else {
			return getRandomMessageCreator();
		}
	}

	private static MessageCreator getRandomMessageCreator() {
		int template = new RandomGenerator().nextTemplateNumber();

		switch (template) {
            case SQUARED:
                return new SquareMessageCreator();
            case ID:
                return new IdMessageCreator();
            case FAULT:
                return new FaultMessageCreator();
            default:
                return null;
        }
	}

	private static MessageCreator getSpamMessageCreator() {
		return new SpamMessageCreator();
	}
}
