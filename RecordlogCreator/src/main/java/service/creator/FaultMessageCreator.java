package service.creator;

import model.MessageDuple;
import model.MessageRecordDuple;
import service.SoapFault;

/**
 * Created by Marten on 21.04.2016.
 */
public class FaultMessageCreator extends MessageCreator {

	@Override
	public MessageDuple create(MessageRecordDuple recordDuple) {
		MessageDuple messageDuple = new MessageDuple();
		messageDuple.setRequestMessage(SoapFault.createDefaultFaultXml());
		messageDuple.setResponseMessage(SoapFault.createDefaultFaultXml());
		return messageDuple;
	}
}
