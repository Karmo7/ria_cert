package service.creator;

import model.MessageDuple;
import model.MessageRecord;
import model.MessageRecordDuple;
import service.util.RandomGenerator;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * Created by Marten on 6.04.2016.
 */
public class MessageRecordCreator {

	private static final String SIGNATURE = "DEFAULT_SIGN-";
	private static final String MEMBERCLASS = "NGO";
	private static final String MEMBERCODE = "TTUTUDENGID-";
	private static final String SUBSYSTEMCODE = "xtee-";

	public MessageRecordDuple create() {
		return create(false);
	}

	public MessageRecordDuple create(boolean spamMode) {
		String queryId = getUniqueId();
		MessageRecordDuple recordDuple = createMessageRecordDuple(queryId);
		addMessages(recordDuple, spamMode);
		return recordDuple;
	}

	private MessageRecordDuple createMessageRecordDuple(String queryId) {
		MessageRecordDuple recordDuple = new MessageRecordDuple();
		recordDuple.setRequest(createRequest(queryId));
		recordDuple.setResponse(createResponse(queryId));
		return recordDuple;
	}

	private void addMessages(MessageRecordDuple recordDuple, boolean spamMode) {
		MessageCreator messageCreator = MessageCreatorFactory.get(spamMode);
		MessageDuple messages = messageCreator.create(recordDuple);
		recordDuple.getRequest().setMessage(messages.getRequestMessage());
		recordDuple.getResponse().setMessage(messages.getResponseMessage());
	}

	private MessageRecord createRequest(String queryId) {
		MessageRecord request = new MessageRecord();
		setCommonAttributes(queryId, request);
		request.setTime(getTimeNow());
		request.setResponse(isNotResponse());
		return request;
	}

	private MessageRecord createResponse(String queryId) {
		MessageRecord response = new MessageRecord();
		setCommonAttributes(queryId, response);
		response.setTime(getTimeNow() + getRandomTime());
		response.setResponse(isResponse());
		return response;
	}

	private Long getRandomTime() {
		return new Long(new Random().nextInt(2000) + 1000);
	}

	private void setCommonAttributes(String queryId, MessageRecord messageRecord) {
		messageRecord.setQueryId(queryId);
		messageRecord.setSignature(getSignature(queryId));
		messageRecord.setMemberClass(MEMBERCLASS);
		messageRecord.setMemberCode(getMembercode());
		messageRecord.setSubsystemCode(getSubsystemCode());
	}

	private boolean isResponse() {
		return Boolean.TRUE;
	}

	private boolean isNotResponse() {
		return Boolean.FALSE;
	}

	private String getSignature(String queryId) {
		return SIGNATURE + queryId;
	}

	/**
	 *
	 * Uses java.util.Date because xroad-6 uses it.
     */
	private long getTimeNow() {
		return new Date().getTime();
	}

	private String getUniqueId() {
		return UUID.randomUUID().toString();
	}

	private String getMembercode() {
		return MEMBERCODE + getRandomCode();
	}

	private String getSubsystemCode() {
		return SUBSYSTEMCODE + getRandomSubsystemCode();
	}

	private String getRandomCode() {
		return String.valueOf(new RandomGenerator().integerBetween(11, 15));
	}

	private String getRandomSubsystemCode() {
		return String.valueOf(new RandomGenerator().integerBetween(1, 4));
	}
}
