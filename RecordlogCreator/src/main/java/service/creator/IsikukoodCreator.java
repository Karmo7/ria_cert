package service.creator;

import service.util.RandomGenerator;

/**
 * Created by Marten on 7.04.2016.
 */
public class IsikukoodCreator {

	private IsikukoodCreator() {
	}

	public static String create() {
		String isikukood = "";

		RandomGenerator random = new RandomGenerator();
		isikukood += random.sex();
		isikukood += formatNumber(random.year());
		isikukood += formatNumber(random.month());
		isikukood += formatNumber(random.day());
		isikukood += "0000";

		return isikukood;
	}

	private static String formatNumber(Integer dateNumber) {
		if (dateNumber < 10) {
			return "0" + dateNumber;
		} else {
			return dateNumber.toString();
		}
	}
}
