package service.creator;

import model.MessageDuple;
import model.MessageRecord;
import model.MessageRecordDuple;

/**
 * Created by Marten on 7.04.2016.
 */
public abstract class MessageCreator {

	private static final String CLIENT_MEMBERCODE = "$CLIENT_MEMBERCODE$";
	private static final String CLIENT_SUBSYSTEM_CODE = "$CLIENT_SUBSYSTEM_CODE$";
	private static final String SERVICE_MEMBERCODE = "$SERVICE_MEMBERCODE$";
	private static final String SERVICE_SUBSYSTEM_CODE = "$SERVICE_SUBSYSTEM_CODE$";

	abstract MessageDuple create(MessageRecordDuple recordDuple);

	protected String setCommonAttributes(MessageRecordDuple messageDuple, String message) {
		MessageRecord request = messageDuple.getRequest();
		MessageRecord response = messageDuple.getResponse();

		message = message.replace(CLIENT_MEMBERCODE, request.getMemberCode());
		message = message.replace(CLIENT_SUBSYSTEM_CODE, request.getSubsystemCode());
		message = message.replace(SERVICE_MEMBERCODE, response.getMemberCode());
		message = message.replace(SERVICE_SUBSYSTEM_CODE, response.getSubsystemCode());
		return message;
	}
}
