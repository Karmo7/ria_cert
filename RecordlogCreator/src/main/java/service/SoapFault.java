/**
 * The MIT License
 * Copyright (c) 2015 Estonian Information System Authority (RIA), Population Register Centre (VRK)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package service;

/**
 * Soap interface implementation representing an error message.
 */
public class SoapFault {

    static final String SOAP_NS_SOAP_ENV =
            "http://schemas.xmlsoap.org/soap/envelope/";

    public static final String NS_XROAD = "http://x-road.eu/xsd/xroad.xsd";
    private static final String DEFAULT_FAULT_CODE = "Server.ClientProxy.UnknownMember";
    private static final String DEFAULT_FAULT_STRING = "Client 'MEMBER:EE/GOV/MEMBER3' not found";

    private final String faultCode;
    private final String faultString;

    public SoapFault(String faultCode, String faultString) {
        this.faultCode = faultCode;
        this.faultString = faultString;
    }

    /**
     * Gets the fault code of the SOAP fault.
     * @return a String with the fault code
     */
    public String getCode() {
        return faultCode;
    }

    /**
     * Gets the fault string of the SOAP fault.
     * @return a String giving an explanation of the fault
     */
    public String getString() {
        return faultString;
    }

    /**
     * Get default SOAP fault
     * @return
     */
    public static String createDefaultFaultXml() {
        return createFaultXml(DEFAULT_FAULT_CODE, DEFAULT_FAULT_STRING);
    }

    /**
     * Creates a SOAP fault message.
     * @param faultCode code of the new SOAP fault
     * @param faultString string of the new SOAP fault
     * @return a String containing XML of the SOAP fault represented by the given parameters
     */
    public static String createFaultXml(String faultCode, String faultString) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<SOAP-ENV:Envelope "
                    + "xmlns:SOAP-ENV=\"" + SOAP_NS_SOAP_ENV + "\" "
                    + "xmlns:xroad=\"" + NS_XROAD + "\">"
                    + "<SOAP-ENV:Body>"
                        + "<SOAP-ENV:Fault>"
                            + "<faultcode>" + faultCode + "</faultcode>"
                            + "<faultstring>"
                            + faultString
                            + "</faultstring>"
                         + "</SOAP-ENV:Fault>"
                     + "</SOAP-ENV:Body>"
                 + "</SOAP-ENV:Envelope>";
    }
    

}
