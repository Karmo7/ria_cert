package dao;

import model.Args;
import model.MessageRecord;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Marten on 07.04.2016.
 */
public class MessageRecordDao {

	private Args args;

	private static final java.lang.String INSERT = "INSERT INTO LOGRECORD" +
			" (DISCRIMINATOR, TIME, ARCHIVED, QUERYID," +
			" MESSAGE, RESPONSE,MEMBERCLASS," +
			" MEMBERCODE, SUBSYSTEMCODE)" +
			" VALUES(?,?,?,?,?,?,?,?,?)";


	public MessageRecordDao(Args args) {
		this.args = args;
	}

	private Connection getConnection() {
		try {
			Properties user = new Properties();
			user.setProperty("user", args.getUsername());
			user.setProperty("password", args.getPassword());

			return DriverManager.getConnection(args.getUrl(), user);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Could not create connection!", e);
		}
	}

	public void insert(MessageRecord messageRecord) {
		try (Connection connection = getConnection();
			 PreparedStatement stmt = connection.prepareStatement(INSERT)) {

			setParams(messageRecord, stmt);
			int response = stmt.executeUpdate();
			if (response == 0) {
				System.err.println("MessageRecordDao.insert: ERROR!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void setParams(MessageRecord messageRecord, PreparedStatement stmt) throws SQLException {
		stmt.setString(1, MessageRecordDefault.DISCRIMINATOR);
		stmt.setLong(2, messageRecord.getTime());
		stmt.setBoolean(3, MessageRecordDefault.ARCHIVED);
		stmt.setString(4, messageRecord.getQueryId());
		stmt.setString(5, messageRecord.getMessage());
		stmt.setBoolean(6, messageRecord.isResponse());
		stmt.setString(7, messageRecord.getMemberClass());
		stmt.setString(8, messageRecord.getMemberCode());
		stmt.setString(9, messageRecord.getSubsystemCode());
	}

	private static class MessageRecordDefault {
		static final String DISCRIMINATOR = "m";
		static final Boolean ARCHIVED = Boolean.TRUE;
	}

}
