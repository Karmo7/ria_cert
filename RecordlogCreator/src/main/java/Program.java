import dao.MessageRecordDao;
import model.Args;
import model.MessageRecordDuple;
import service.creator.MessageRecordCreator;

/**
 * Created by Marten on 6.04.2016.
 */
public class Program {

	public static void main(String[] args) {
		Args arguments = Args.parse(args);
		while (true) {
			loop(arguments);
		}
	}

	private static void loop(Args args) {
		try {
			MessageRecordDuple duple = new MessageRecordCreator().create(args.isSpamMode());
			insertMessages(duple, args);
			Thread.sleep(args.getFrequency());
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeException("Program closed unexpectedly!");
		}
	}

	private static void insertMessages(MessageRecordDuple duple, Args args) {
		MessageRecordDao messageRecordDao = new MessageRecordDao(args);
		messageRecordDao.insert(duple.getRequest());
		messageRecordDao.insert(duple.getResponse());
	}
}
