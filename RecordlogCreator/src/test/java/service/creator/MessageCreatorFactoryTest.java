package service.creator;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Marten on 9.04.2016.
 */
public class MessageCreatorFactoryTest {

	@Test
	public void testMessageCreatorNotNull() {
		for (int i = 0; i < 10; i++) {
			createAndTest();
		}
	}

	private void createAndTest() {
		MessageCreator messageCreator = MessageCreatorFactory.get();
		assertNotNull(messageCreator);
	}
}
