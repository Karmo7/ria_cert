package service.creator;

import model.MessageDuple;
import model.MessageRecord;
import model.MessageRecordDuple;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 9.04.2016.
 */
public class IdMessageCreatorTest {

	private static final String ISIKUKOOD_SERVICE = "isikukoodService";
	private static final String MEMBER_CODE = "#MEMBER_CODE#";
	private static final String SUB_SYSTEM_CODE = "#SUBSYSTEM_CODE#";

	private MessageRecordDuple messageRecordDuple;

	@Before
	public void init() {
		messageRecordDuple = getMessageRecordDuple();
	}

	@Test
	public void messagesNotNullTest() {
		MessageDuple messages = createMessageDuple();

		assertNotNull(messages.getRequestMessage());
		assertNotNull(messages.getResponseMessage());
	}

	@Test
	public void correctServiceTest() {
		MessageDuple messages = createMessageDuple();

		assertTrue(messages.getRequestMessage().contains(ISIKUKOOD_SERVICE));
		assertTrue(messages.getResponseMessage().contains(ISIKUKOOD_SERVICE));
	}

	@Test
	public void containsRequiredInformationTest() {
		MessageDuple messages = createMessageDuple();

		assertTrue(messages.getRequestMessage().contains(MEMBER_CODE));
		assertTrue(messages.getRequestMessage().contains(SUB_SYSTEM_CODE));
	}

	private MessageDuple createMessageDuple() {
		MessageCreator messageCreator = new IdMessageCreator();
		MessageRecordDuple messageRecordDuple = getMessageRecordDuple();
		return messageCreator.create(messageRecordDuple);
	}

	private MessageRecordDuple getMessageRecordDuple() {
		MessageRecordDuple messageRecordDuple = new MessageRecordDuple();
		MessageRecord messageRecord = new MessageRecord();
		messageRecord.setMemberCode(MEMBER_CODE);
		messageRecord.setSubsystemCode(SUB_SYSTEM_CODE);
		messageRecordDuple.setRequest(messageRecord);
		messageRecordDuple.setResponse(messageRecord);
		return messageRecordDuple;
	}
}
