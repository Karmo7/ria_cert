package service.creator;

import model.MessageDuple;
import model.MessageRecordDuple;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 21.04.2016.
 */
public class FaultMessageCreatorTest {

	@Test
	public void testCreate() throws Exception {
		FaultMessageCreator creator = new FaultMessageCreator();
		MessageDuple messageDuple = creator.create(new MessageRecordDuple());

		assertNotNull(messageDuple.getRequestMessage());
		assertNotNull(messageDuple.getResponseMessage());

		assertTrue(messageDuple.getRequestMessage().contains("<SOAP-ENV:Fault>"));
	}
}
