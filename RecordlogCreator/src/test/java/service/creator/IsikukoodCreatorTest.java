package service.creator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Marten on 9.04.2016.
 */
public class IsikukoodCreatorTest {

	@Test
	public void test() {
		String isikukood = IsikukoodCreator.create();
		for (int i = 0; i < 10; i++) {
			test(isikukood);
		}
	}

	private void test(String isikukood) {
		assertNotNull(isikukood);
		assertEquals(11, isikukood.length());
	}

}
