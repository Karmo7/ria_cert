package service.creator;

import model.MessageRecordDuple;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 9.04.2016.
 */
public class MessageRecordCreatorTest {

	private MessageRecordDuple messageRecordDuple;

	@Before
	public void init() {
		MessageRecordCreator messageRecordCreator = new MessageRecordCreator();
		messageRecordDuple = messageRecordCreator.create();
	}

	@Test
	public void createNotNullTest() throws Exception {
		assertNotNull(messageRecordDuple.getRequest());
		assertNotNull(messageRecordDuple.getResponse());
	}

	@Test
	public void createIsResponseTest() {
		assertFalse(messageRecordDuple.getRequest().isResponse());
		assertTrue(messageRecordDuple.getResponse().isResponse());
	}

	@Test
	public void queryIdNotNullTest() throws Exception {
		assertNotNull(messageRecordDuple.getRequest().getQueryId());
		assertNotNull(messageRecordDuple.getResponse().getQueryId());
	}

	@Test
	public void queryIdSameTest() throws Exception {
		assertEquals(messageRecordDuple.getRequest().getQueryId(), messageRecordDuple.getResponse().getQueryId());
	}

	@Test
	public void membercodeNotNullTest() throws Exception {
		assertNotNull(messageRecordDuple.getRequest().getMemberCode());
		assertNotNull(messageRecordDuple.getResponse().getMemberCode());
	}

	@Test
	public void subSystemCodeNotNullTest() throws Exception {
		assertNotNull(messageRecordDuple.getRequest().getSubsystemCode());
		assertNotNull(messageRecordDuple.getResponse().getSubsystemCode());
	}

	@Test
	public void timeNotNullTest() throws Exception {
		assertNotNull(messageRecordDuple.getRequest().getTime());
		assertNotNull(messageRecordDuple.getResponse().getTime());
	}
}
