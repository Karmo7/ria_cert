package service.creator;

import model.Args;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 9.04.2016.
 */
public class ArgsTest {

	@Test
	public void testEmptyArgsDefaultValues() throws Exception {
		Args args = Args.parse(new String[]{});
		assertEquals(Args.DEFAULT_FREQUENCY, args.getFrequency());
		assertEquals(Args.DEFAULT_URL, args.getUrl());
		assertEquals(Args.DEFAULT_USERNAME, args.getUsername());
		assertEquals(Args.DEFAULT_PASSWORD, args.getPassword());
		assertTrue(args.isEmpty());
	}

	@Test
	public void testFrequencyOnly() throws Exception {
		Args args = Args.parse(new String[]{"10000"});
		assertEquals(new Integer(10000), args.getFrequency());
	}

	@Test
	public void testFrequencyOnlyWithFlag() throws Exception {
		Args args = Args.parse(new String[]{"-f", "20000"});
		assertEquals(new Integer(20000), args.getFrequency());
	}

	@Test
	public void testAllFlagsSet() throws Exception {
		Args args = Args.parse(new String[]{"-f", "30000", "-u", "username", "-p", "password"});
		assertEquals(new Integer(30000), args.getFrequency());
		assertEquals("username", args.getUsername());
		assertEquals("password", args.getPassword());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testNullArgs() throws Exception {
		Args.parse(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalParam() throws Exception {
		Args.parse(new String[]{"-g asd"});
	}

	@Test(expected = NumberFormatException.class)
	public void testNoValue() throws Exception {
		Args.parse(new String[]{"-f"});
	}

	@Test(expected = NumberFormatException.class)
	public void testNoMultiParam() throws Exception {
		Args.parse(new String[]{"-f", "40000", "-u"});
	}
}
