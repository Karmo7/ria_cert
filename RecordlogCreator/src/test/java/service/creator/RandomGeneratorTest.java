package service.creator;

import org.junit.Test;
import service.util.RandomGenerator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 9.04.2016.
 */
public class RandomGeneratorTest {

	@Test
	public void integerBetweenTest() throws Exception {
		int randomNumber = new RandomGenerator().integerBetween(0, 1);
		assertEquals(0, randomNumber);
	}

	@Test
	public void nextTemplateNumberRangeTest() throws Exception {
		for (int i = 0; i < 10; i++) {
			int nextTemplateNumber = new RandomGenerator().nextTemplateNumber();
			assertTrue(nextTemplateNumber >= 0 && nextTemplateNumber < MessageCreatorFactory.MAX_ID);
		}
	}

	@Test
	public void dayRangeTest() throws Exception {
		for (int i = 0; i < 25; i++) {
			int day = new RandomGenerator().day();
			assertTrue(day >= 1 && day <= 28);
		}
	}

	@Test
	public void monthRangeTest() throws Exception {
		for (int i = 0; i < 10; i++) {
			int month = new RandomGenerator().month();
			assertTrue(month >= 1 && month <= 12);
		}
	}

	@Test
	public void yearRangeTest() throws Exception {
		for (int i = 0; i < 100; i++) {
			int year = new RandomGenerator().year();
			assertTrue(year >= 0 && year <= 99);
		}
	}

	@Test
	public void sexRangeTest() throws Exception {
		for (int i = 0; i < 10; i++) {
			int sex = new RandomGenerator().sex();
			assertTrue(sex >= 3 && sex <= 4);
		}
	}
}
