

CREATE TABLE logrecord (
    id serial NOT NULL,
    discriminator character varying(255) NOT NULL,
    "time" bigint,
    archived boolean,
    queryid character varying(255),
    message text,
    signature text,
    hashchain text,
    hashchainresult text,
    signaturehash text,
    timestamprecord bigint,
    timestamphashchain text,
    response boolean,
    "timestamp" text,
    memberclass character varying(255),
    membercode character varying(255),
    subsystemcode character varying(255)
);



ALTER TABLE ONLY logrecord
    ADD CONSTRAINT logrecordpk PRIMARY KEY (id);



CREATE INDEX "LOGRECORD_TIMESTAMPRECORD_fkey" ON logrecord USING btree (timestamprecord);


ALTER TABLE ONLY logrecord
    ADD CONSTRAINT fk_qo6ack8sad6fqib90xghdaylh FOREIGN KEY (timestamprecord) REFERENCES logrecord(id);

