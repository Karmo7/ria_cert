
Single arg:
single numeric value(i.e. 10000) -- frequency 10sec

-------------------------------------------
Multiple args:

-f <numeric value> -- frequency
-u <string value>  -- postgres username
-p <string value>  -- postgres password

------------------------------------------
Defaults:
	frequency = 10000ms
	username = postgres
	password = postgres

------------------------------------------
Examples:
1) 500
2) -f 500 -u testuser -p testpassword
3) -f random


