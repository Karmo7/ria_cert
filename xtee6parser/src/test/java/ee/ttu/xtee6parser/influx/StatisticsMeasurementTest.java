package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.model.MessageStatistics;
import org.influxdb.dto.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Karmo on 018 18 04 2016.
 */
public class StatisticsMeasurementTest {

	private StatisticsMeasurement statisticsMeasurement;
	private MessageStatistics messageStatistics = new MessageStatistics(12, 13);
	private long messageLogTime = 1459584317000L;

	@Before
	public void setUp() throws Exception {
		statisticsMeasurement = new StatisticsMeasurement();
	}

	@Test
	public void getMeasurementPoints() throws Exception {
		List<Point> measurmentPoints = statisticsMeasurement.getMeasurementPoints(
				"messagestats", messageStatistics);
		assertEquals(1, measurmentPoints.size());
		Point point = measurmentPoints.get(0);
		String pointExpected = "messagestats bodykurtosis=13.0,bodyskewness=12.0 ";
		assertEquals(pointExpected, point.lineProtocol().substring(0, point.lineProtocol().length() - 19));
	}

}
