package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.model.MessageCount;
import org.influxdb.dto.Point;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Karmo on 007 7 05 2016.
 */
public class MessageFaultMeasurementTest {
	private static final String MEASUREMENT_NAME = "asdf";

	private MessageFaultMeasurement messageFaultMeasurement;
	private MessageCount messageCount = new MessageCount(24L, 13L);
	private long messageLogTime = 1459584317000L;

	@Before
	public void setUp() throws Exception {
		messageFaultMeasurement = new MessageFaultMeasurement(MEASUREMENT_NAME);
	}

	@Test
	public void getMeasurementPoints() throws Exception {
		List<Point> measurmentPoints = messageFaultMeasurement
				.getMeasurementPoints(messageCount);
		assertEquals(1, measurmentPoints.size());
		Point point = measurmentPoints.get(0);
		String pointExpected = MEASUREMENT_NAME + " faultymessagecount=13.0," +
				"messagecount=24.0,successfulmessagecount=11.0 ";
		assertEquals(pointExpected, point.lineProtocol().substring(0, point.lineProtocol().length() - 19));
	}

}
