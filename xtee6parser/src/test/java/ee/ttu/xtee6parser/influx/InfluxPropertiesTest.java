package ee.ttu.xtee6parser.influx;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.assertEquals;

/**
 * Created by margus on 2.05.2016.
 */
public class InfluxPropertiesTest {

	private InfluxProperties influxProperties;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void influxPropertiesTest() throws Exception {
		String input = "url=http://192.168.33.12:8086\n" +
				"user=influx\n" +
				"password=influx2\n" +
				"dbName=xroad\n" +
				"retension-policy=test";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);

		assertEquals("http://192.168.33.12:8086", influxProperties.getUrl());
		assertEquals("influx", influxProperties.getUser());
		assertEquals("influx2", influxProperties.getPassword());
		assertEquals("xroad", influxProperties.getDbName());
		assertEquals("test", influxProperties.getRetensionPolicy());

	}

	@Test
	public void influxPropertiesRetensionPolicyMissingTest() throws Exception {
		String input = "url=http://192.168.33.12:8086\n" +
				"user=influx\n" +
				"password=influx2\n" +
				"dbName=xroad\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);
		assertEquals("default", influxProperties.getRetensionPolicy());
	}

	@Test(expected = IllegalArgumentException.class)
	public void influxPropertieUrlMissingTest() throws Exception {
		String input = "user=influx\n" +
				"password=influx2\n" +
				"dbName=xroad\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);

	}

	@Test(expected = IllegalArgumentException.class)
	public void influxPropertieUserMissingTest() throws Exception {
		String input = "url=http://192.168.33.12:8086\n" +
				"password=influx2\n" +
				"dbName=xroad\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);

	}

	@Test(expected = IllegalArgumentException.class)
	public void influxPropertiePasswordMissingTest() throws Exception {
		String input = "url=http://192.168.33.12:8086\n" +
				"user=influx\n" +
				"dbName=xroad\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);
	}

	@Test(expected = IllegalArgumentException.class)
	public void influxPropertiDbNameMissingTest() throws Exception {
		String input = "url=http://192.168.33.12:8086\n" +
				"user=influx\n" +
				"password=influx2\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		influxProperties = new InfluxProperties(stream);
	}
}
