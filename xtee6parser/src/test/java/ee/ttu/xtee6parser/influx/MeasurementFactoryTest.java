package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.program.ParserProperties;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.influxdb.dto.Point;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by margus on 2.04.2016.
 */
public class MeasurementFactoryTest {


	private InfluxDao mockInfluxDao;
	private List<MessageRecord> messageRecords;
	private MessageRecord requestMessageRecord;
	private MessageRecord responseMessageRecord;
	private MeasurementFactory measurementFactory;
	private ParserProperties mockParserProperties;

	@Before
	public void setUp() throws Exception {
		mockInfluxDao = mock(InfluxDao.class);
		LocalDateTime returnDate = LocalDateTime.ofEpochSecond(1459584317L, 845000000, ZoneOffset.UTC);
		when(mockInfluxDao.getRequestMessageTime(anyString())).thenReturn(returnDate);
		mockParserProperties = mock(ParserProperties.class);
		when(mockParserProperties.getHostname()).thenReturn("localhost");
		measurementFactory = new MeasurementFactory(mockInfluxDao, mockParserProperties);
		fillMessageRecords();
	}

	@Test
	public void getMeasurmentPointsTest() {
		List<Point> measurmentPoints = measurementFactory.getMeasurmentPoints(messageRecords);
		assertEquals(2, measurmentPoints.size());
		Point firstPoint = measurmentPoints.get(0);
		Point secondPoint = measurmentPoints.get(1);
		String firstPointExpected = "messagelog,host=localhost,membercode=TTUTUDENGID1,messagehash=2c6fc4154d6e6a27519f8e36579fcc48," +
				"queryid=1,response=0,servicename=getRandom,serviceversion=v2,subsystemcode=xtee " +
				"count=1.0,messagesize=1301.0,responsetime=0.0 1459584317842000000";
		String secondPointExpected = "messagelog,host=localhost,membercode=TTUTUDENGID1,messagehash=28f7393d42ba0c45915599c8111e30fb," +
				"queryid=1,response=1,servicename=getRandom,serviceversion=v2,subsystemcode=xtee " +
				"count=1.0,messagesize=1685.0,responsetime=11045.0 1459584328887000000";

		assertEquals(firstPointExpected, firstPoint.lineProtocol());
		assertEquals(secondPointExpected, secondPoint.lineProtocol());
	}

	@Test
	public void getMeasurmentPointsOnlyResponseTest() {
		List<MessageRecord> messageRecords = new ArrayList<>();
		messageRecords.add(responseMessageRecord);
		List<Point> measurmentPoints = measurementFactory.getMeasurmentPoints(messageRecords);
		assertEquals(1, measurmentPoints.size());
		Point measurmentPoint = measurmentPoints.get(0);
		String pointExpected = "messagelog,host=localhost,membercode=TTUTUDENGID1,messagehash=28f7393d42ba0c45915599c8111e30fb," +
				"queryid=1,response=1,servicename=getRandom,serviceversion=v2,subsystemcode=xtee " +
				"count=1.0,messagesize=1685.0,responsetime=11042.0 1459584328887000000";
		assertEquals(pointExpected, measurmentPoint.lineProtocol());
	}

	@Test
	public void getMeasurmentPointsValuesMissing() {
		List<MessageRecord> messageRecords = new ArrayList<>();
		responseMessageRecord.setMemberCode(null);
		responseMessageRecord.setSubsystemCode(null);
		responseMessageRecord.setMessage(null);
		messageRecords.add(responseMessageRecord);
		List<Point> measurmentPoints = measurementFactory.getMeasurmentPoints(messageRecords);
		assertEquals(1, measurmentPoints.size());
		Point measurmentPoint = measurmentPoints.get(0);
		String pointExpected = "messagelog,host=localhost,membercode=MEMCODEMISSING,messagehash=MESSAGEHASHMISSING," +
				"queryid=1,response=1,servicename=SERVICECODEMISSING,serviceversion=SERVICEVERSIONMISSING,subsystemcode=SYSCODEMISSING " +
				"count=1.0,messagesize=0.0,responsetime=11042.0 1459584328887000000";
		assertEquals(pointExpected, measurmentPoint.lineProtocol());
	}


	private void fillMessageRecords() throws IOException {
		messageRecords = new ArrayList<>();
		String requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_1);
		String responseMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_RESPONSE_1);

		System.out.println(responseMessage.getBytes());
		requestMessageRecord = new MessageRecord(1459584317842L, "1", requestMessage, "", "", "", "", "", "", false, "NGO", "TTUTUDENGID1", "xtee");
		responseMessageRecord = new MessageRecord(1459584328887L, "1", responseMessage, "", "", "", "", "", "", true, "NGO", "TTUTUDENGID1", "xtee");
		messageRecords.add(requestMessageRecord);
		messageRecords.add(responseMessageRecord);
	}


}
