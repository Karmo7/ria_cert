package ee.ttu.xtee6parser.jdbc;

import ee.ttu.xtee6parser.exception.XteeParserException;
import org.junit.Test;

import static ee.ttu.xtee6parser.utilities.properties.DbPropertiesFields.*;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Marten on 15.05.2016.
 */
public class DbPropertiesTest {


    @Test(expected = XteeParserException.class)
    public void testPropertiesFileNotFound() throws Exception {
        new DbProperties("DBConnection-test0");
    }

    @Test
    public void testLoadedValues() throws Exception {
        DbProperties properties = new DbProperties("DBConnection-test1.txt");
        assertEquals("jdbc:postgresql://127.0.0.1:5432/test", properties.getUrl());
        assertEquals("jdbc:postgresql://127.0.0.1:5432/test", properties.getFullUrl());
        assertEquals("testuser", properties.getUsername());
        assertEquals("testdriver", properties.getDriver());
        assertEquals("testpassword", properties.getPassword());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDefaultValuesUserRequired() throws Exception {
        DbProperties properties = new DbProperties("DBConnection-test2.txt");
        assertEquals(DB_USERNAME.getValue(), properties.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDefaultValues() throws Exception {
        DbProperties properties = new DbProperties("DBConnection-test3.txt");

        assertEquals(DB_URL.getValue(), properties.getUrl());
        assertEquals(DB_URL.getValue(), properties.getFullUrl());
        assertEquals(DB_DRIVER.getValue(), properties.getDriver());
        assertEquals(DB_PASSWORD.getValue(), properties.getPassword());
        assertEquals(DB_USERNAME.getValue(), properties.getUsername());
    }

}
