package ee.ttu.xtee6parser.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Karmo on 018 18 04 2016.
 */
public class MessageStatisticsTest {

	@Test
	public void getBodySkewness() throws Exception {
		assertEquals(12, new MessageStatistics(12, 13).getBodySkewness(), 0.0001);
	}

	@Test
	public void getBodySkewness_NanReturnsZero() throws Exception {
		assertEquals(0, new MessageStatistics(Double.NaN, 13).getBodySkewness()
				, 0.0001);
	}

	@Test
	public void getBodyKurtosis() throws Exception {
		assertEquals(13, new MessageStatistics(12, 13).getBodyKurtosis(), 0.0001);
	}

	@Test
	public void getBodyKurtosis_NanReturnsZero() throws Exception {
		assertEquals(0, new MessageStatistics(12, Double.NaN).getBodyKurtosis()
				, 0.0001);
	}

	@Test
	public void isValid() throws Exception {
		assertTrue(new MessageStatistics(12, 13).isValid());
		assertFalse(new MessageStatistics(Double.NaN, 13).isValid());
		assertFalse(new MessageStatistics(12, Double.NaN).isValid());
		assertFalse(new MessageStatistics(Double.NaN, Double.NaN).isValid());
	}

}
