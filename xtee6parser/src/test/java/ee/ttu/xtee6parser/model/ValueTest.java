package ee.ttu.xtee6parser.model;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by Marten on 1.05.2016.
 */
public class ValueTest {

	@Test
	public void testValueDoesntEqualNoElement() throws Exception {
		Value value = new Value("testvalue1");
		Value value2 = new Value("testvalue2");
		assertFalse(value.equals(value2));
	}

	@Test
	public void testValueEqualsNoElement() throws Exception {
		Value value = new Value("testvalue");
		Value value2 = new Value("testvalue");
		assertTrue(value.equals(value2));
	}

	@Test
	public void testValueDoesntEqualWithElement() throws Exception {
		Value value = new Value("testvalue", "element.element2");
		Value value2 = new Value("testvalue2", "element.element2");
		assertFalse(value.equals(value2));
	}

	@Test
	public void testValueEqualsWithEqualElements() throws Exception {
		Value value = new Value("testvalue", "element.element2");
		Value value2 = new Value("testvalue", "element.element2");
		assertTrue(value.equals(value2));
	}

	@Test
	public void testValueDoesntEqualWithNotEqualsElements() throws Exception {
		Value value = new Value("testvalue", "element.element");
		Value value2 = new Value("testvalue2", "element.element2");
		assertFalse(value.equals(value2));
	}

	@Test
	public void testValueEqualsWithNotEqualElements() throws Exception {
		Value value = new Value("testvalue", "element.element");
		Value value2 = new Value("testvalue", "element.element2");
		assertFalse(value.equals(value2));
	}

}
