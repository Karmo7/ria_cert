package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.StatisticsMeasurement;
import ee.ttu.xtee6parser.model.MessageStatistics;
import javafx.util.Pair;
import org.influxdb.dto.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karmo on 024 24 04 2016.
 */
public class StatisticsMeasurementSpy extends StatisticsMeasurement {

	List<Pair<String, MessageStatistics>> spiedMeasurements = new ArrayList<>();

	@Override
	public List<Point> getMeasurementPoints(String measurement, MessageStatistics messageStatistics) {
		spiedMeasurements.add(new Pair<>(measurement, messageStatistics));
		return new ArrayList<>();
	}
}
