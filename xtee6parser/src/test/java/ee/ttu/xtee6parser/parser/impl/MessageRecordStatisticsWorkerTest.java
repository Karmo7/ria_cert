package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.MessageFaultMeasurement;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.utilities.MessageRecordElementTagCounterTest;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

/**
 * Created by Karmo on 024 24 04 2016.
 */
public class MessageRecordStatisticsWorkerTest {

	final static int MIN_STAT_MEASUREMENT_COUNT = 5;
	final static int MIN_MEASUREMENT_COUNT = MIN_STAT_MEASUREMENT_COUNT + 1;
	final static String FAULT_MEASUREMENT_NAME = "asd";

	StatisticsMeasurementSpy statisticsMeasurementSpy = new StatisticsMeasurementSpy();
	MessageFaultMeasurement messageFaultMeasurement
			= spy(new MessageFaultMeasurement(FAULT_MEASUREMENT_NAME));
	InfluxDao influxDao;
	MessageRecordStatisticsWorker worker;
	private List<MessageRecord> sampleMessageRecods;

	@Before
	public void setUp() throws Exception {
		influxDao = mock(InfluxDao.class);
		worker = new MessageRecordStatisticsWorker(
				influxDao, statisticsMeasurementSpy, messageFaultMeasurement);
		sampleMessageRecods = MessageRecordElementTagCounterTest.getMessageRecords();
	}

	@Test
	public void run_withEmptyArray_givesMinMeasurementCount() throws Exception {
		worker.run(new ArrayList<>());
		assertEquals(MIN_STAT_MEASUREMENT_COUNT,
				statisticsMeasurementSpy.spiedMeasurements.size());
	}

	@Test
	public void run_withCountableMessageRecords_addsMeasurementForEachElement() throws Exception {
		worker.run(sampleMessageRecods);
		int expected = MIN_STAT_MEASUREMENT_COUNT + 5;
		assertEquals(expected, statisticsMeasurementSpy.spiedMeasurements.size());
	}

	@Test
	public void run_savesEachMeasurement() throws Exception {
		worker.run(sampleMessageRecods);
		int expected = MIN_MEASUREMENT_COUNT + 5;
		verify(influxDao, times(expected)).saveMeasurementPoints(anyObject());
	}

	@Test
	public void run_addsMessageFaultMeasurement() {
		worker.run(sampleMessageRecods);
		verify(messageFaultMeasurement, times(1)).getMeasurementPoints(anyObject());
	}
}
