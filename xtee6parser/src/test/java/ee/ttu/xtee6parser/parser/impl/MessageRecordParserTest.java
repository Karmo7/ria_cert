package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.MeasurementFactory;
import ee.ttu.xtee6parser.jdbc.XteeDao;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.*;

/**
 * Created by margus on 9.04.2016.
 */
public class MessageRecordParserTest {

	private InfluxDao mockInfluxDao;
	private XteeDao mockXteeDao;
	private MeasurementFactory mockMeasurementFactory;
	private LocalDateTime lastSavedMeasurementPointTime;

	@Before
	public void setUp() throws Exception {
		mockInfluxDao = mock(InfluxDao.class);
		mockXteeDao = mock(XteeDao.class);
		mockMeasurementFactory = mock(MeasurementFactory.class);
		lastSavedMeasurementPointTime = LocalDateTime.parse("2016-04-01T10:15:30");
		when(mockInfluxDao.getLastSavedMeasurementPointTime()).thenReturn(lastSavedMeasurementPointTime);
		when(mockMeasurementFactory.getMeasurmentPoints(anyList())).thenReturn(new ArrayList<>());
	}

	private MessageRecordParser createParser(long pollDepth) {
		return spy(new MessageRecordParser(mockInfluxDao, mockXteeDao,
				new ArrayList<>(Arrays.asList(new MessageRecordStandardWorker(
						mockMeasurementFactory, mockInfluxDao))), pollDepth));
	}

	@Test
	public void doParseFirstTime() {
		long pollDepth = 0;
		when(mockInfluxDao.getLastSavedMeasurementPointTime()).thenReturn(null);
		createParser(pollDepth).parse();

		verify(mockXteeDao, times(0)).getAllMessageRecordsSinceDate(any());
		verify(mockXteeDao, times(1)).getAllMessageRecords();
		verify(mockMeasurementFactory, times(1)).getMeasurmentPoints(anyList());
		verify(mockInfluxDao, times(1)).saveMeasurementPoints(new ArrayList<>());
	}

	@Test
	public void doParseNotFirstTime() {
		long pollDepth = 0;
		createParser(pollDepth).parse();

		verify(mockXteeDao, times(1)).getAllMessageRecordsSinceDate(lastSavedMeasurementPointTime);
		verify(mockXteeDao, times(0)).getAllMessageRecords();
		verify(mockMeasurementFactory, times(1)).getMeasurmentPoints(anyList());
		verify(mockInfluxDao, times(1)).saveMeasurementPoints(new ArrayList<>());
	}

	@Test
	public void givenPollDepth_ignoreLastParseTime() {
		long pollDepth = 57;
		MessageRecordParser parser = createParser(pollDepth);
		LocalDateTime time = LocalDateTime.now();
		when(parser.getCurrentTime()).thenReturn(time);

		parser.parse();

		verify(mockXteeDao, times(1)).getAllMessageRecordsSinceDate(time.minusSeconds(pollDepth));
		verify(mockXteeDao, times(0)).getAllMessageRecords();
		verify(mockMeasurementFactory, times(1)).getMeasurmentPoints(anyList());
		verify(mockInfluxDao, times(1)).saveMeasurementPoints(new ArrayList<>());
	}
}
