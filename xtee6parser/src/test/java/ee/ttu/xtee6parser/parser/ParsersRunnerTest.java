package ee.ttu.xtee6parser.parser;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by margus on 9.04.2016.
 */
public class ParsersRunnerTest {

	private ParsersRunner parsersRunner;
	private Parser mockParser;
	private long pollInterval;

	@Before
	public void setUp() throws Exception {
		mockParser = mock(Parser.class);
		List<Parser> parsers = new ArrayList<>();
		parsers.add(mockParser);
		pollInterval = 10000L;
		parsersRunner = new ParsersRunner(parsers, pollInterval);
	}

	@Test
	public void runParsersTest() throws Exception {
		parsersRunner.runParsers();
		verify(mockParser, times(1)).parse();

	}
}
