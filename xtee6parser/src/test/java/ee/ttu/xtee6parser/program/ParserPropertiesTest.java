package ee.ttu.xtee6parser.program;

import ee.ttu.xtee6parser.utilities.HostnameResolver;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by margus on 9.04.2016.
 */
public class ParserPropertiesTest {

	private ParserProperties properties;
	private HostnameResolver mockHostnameResolver;

	@Before
	public void setUp() throws Exception {
		mockHostnameResolver = mock(HostnameResolver.class);
	}

	@Test
	public void parserPropertiesTest() throws Exception {
		String input = "pollinterval_seconds=10\npolldepth_seconds=20\nhostname=localhost\n" +
				"load_db_properties_from_xroad_conf=true\nxroad_proxy_conf_file=proxy.ini\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
		assertEquals(10L, properties.getPollIntervalSeconds());
		assertEquals(20L, properties.getPollDepthSeconds());
		assertEquals("localhost", properties.getHostname());
		assertTrue(properties.loadDBPropertiesFromXroad());
		assertEquals("proxy.ini", properties.getXroadProxyConfFileLocation());
	}

	@Test(expected = IllegalArgumentException.class)
	public void incorretPollIntervalFormatTest() throws Exception {
		String input = "pollinterval_seconds=1sd0\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
	}

	@Test(expected = RuntimeException.class)
	public void pollIntervalEmptyTest() throws Exception {
		String input = "pollinterval_seconds=\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
	}

	@Test(expected = RuntimeException.class)
	public void pollIntervalMissingTest() throws Exception {
		String input = "pla=1sd0\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
	}

	public void whenPollDepthIsEmpty_shouldDefaultToPollInterval() throws Exception {
		String input = "pollinterval_seconds=15\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
		assertEquals(15, properties.getPollDepthSeconds());
	}

	@Test
	public void parserPropertiesEmptyHostnameTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		when(mockHostnameResolver.getHostname()).thenReturn("mockHost");
		properties = new ParserProperties(stream, mockHostnameResolver);

		assertEquals("mockHost", properties.getHostname());
		verify(mockHostnameResolver, times(1)).getHostname();
	}

	@Test(expected = RuntimeException.class)
	public void errorWhenResolvingHostnameTest() throws Exception {
		String input = "pla=1sd0\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		when(mockHostnameResolver.getHostname()).thenThrow(new RuntimeException());
		properties = new ParserProperties(stream, mockHostnameResolver);
	}

	@Test
	public void loadDBFalseTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=localhost\nload_db_properties_from_xroad_conf=false\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
		assertFalse(properties.loadDBPropertiesFromXroad());
	}

	@Test
	public void loadDBMissingTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=localhost\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
		assertFalse(properties.loadDBPropertiesFromXroad());
	}

	@Test
	public void loadDBWrongFormatTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=localhost\nload_db_properties_from_xroad_conf=faslse\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
		assertFalse(properties.loadDBPropertiesFromXroad());
	}

	@Test(expected = IllegalArgumentException.class)
	public void missingXroadConfLocationTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=localhost\nload_db_properties_from_xroad_conf=true\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
	}

	@Test
	public void missingXroadConfLocationButNotNeededTest() throws Exception {
		String input = "pollinterval_seconds=10\nhostname=localhost\nload_db_properties_from_xroad_conf=false\n";
		InputStream stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		properties = new ParserProperties(stream, mockHostnameResolver);
	}


}
