package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by margus on 19.03.2016.
 */

public class SoapXmlParserUtilTest {

	private SoapXmlParserUtil soapXmlParserUtil;
	private String requestMessage;

	@Test
	public void getFirstElementContentByTagNameNSTest() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_1);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		assertEquals("getRandom", soapXmlParserUtil.getFirstElementContentByTagNameNS("http://x-road.eu/xsd/identifiers", "serviceCode"));
	}

	@Test
	public void getFirstElementContentByTagNameNSIfEmptyTest() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_EMPTY_SERVICECODE);
		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		assertNull(soapXmlParserUtil.getFirstElementContentByTagNameNS("http://x-road.eu/xsd/identifiers", "serviceCode"));
	}

	@Test
	public void getFirstElementContentByTagNameNSIfDoesNotExistsTest() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_MISSING_SERVICECODE);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		assertNull(soapXmlParserUtil.getFirstElementContentByTagNameNS("http://x-road.eu/xsd/identifiers", "serviceCode"));
	}

	@Test
	public void getAllNamespacesInHeaderTest() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_1);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		List<String> namespaces = soapXmlParserUtil.getAllNamespaces();
		assertEquals(5, namespaces.size());
		assertTrue(namespaces.contains("http://schemas.xmlsoap.org/soap/envelope/"));
		assertTrue(namespaces.contains("http://x-road.eu/xsd/xroad.xsd"));
		assertTrue(namespaces.contains("http://x-road.eu/xsd/identifiers"));
		assertTrue(namespaces.contains("http://v6Example.x-road.eu/producer"));
		assertTrue(namespaces.contains("http://www.w3.org/XML/1998/namespace"));

	}

	@Test
	public void getAllNamespacesInsideNode() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_FIN_REQUEST_1);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		List<String> namespaces = soapXmlParserUtil.getAllNamespaces();
		assertEquals(5, namespaces.size());
		assertTrue(namespaces.contains("http://schemas.xmlsoap.org/soap/envelope/"));
		assertTrue(namespaces.contains("http://x-road.eu/xsd/xroad.xsd"));
		assertTrue(namespaces.contains("http://x-road.eu/xsd/identifiers"));
		assertTrue(namespaces.contains("http://vrk-test.x-road.fi/producer"));
		assertTrue(namespaces.contains("http://www.w3.org/XML/1998/namespace"));
	}

	@Test
	public void getNamespaceFirstNodeNameNamespaceInNode() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_FIN_REQUEST_1);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		assertEquals("getOrganization", soapXmlParserUtil.getNamespaceFirstNodeName("http://vrk-test.x-road.fi/producer"));

	}

	@Test
	public void getNamespaceFirstNodeNameNamespaceInHeader() throws Exception {
		requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_REQUEST_1);

		soapXmlParserUtil = new SoapXmlParserUtil(requestMessage);
		assertEquals("getRandom", soapXmlParserUtil.getNamespaceFirstNodeName("http://v6Example.x-road.eu/producer"));

	}

}
