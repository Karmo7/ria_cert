package ee.ttu.xtee6parser.utilities.grouped_value_counter;

import com.google.common.collect.Lists;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.utilities.MessageRecordElementTagCounterTest;
import ee.ttu.xtee6parser.utilities.StringUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 19.04.2016.
 */
public class MessageRecordGroupedByElementValueCounterTest {

	private static final String separator = "\\.";

	@Test
	public void testCount() throws IOException {
		List<MessageRecord> messageRecords = MessageRecordElementTagCounterTest.getMessageRecords();
		Map<String, Count> countMap = new MessageRecordGroupedByElementValueCounter(messageRecords).count();
		assertEquals(5, countMap.keySet().size());

		Count count = countMap.get(getAllTags().get(0));
		assertTrue(count.getConverted().length == 1);
		assertTrue(count.getRaw().length == 1);

		count = countMap.get(getAllTags().get(1));
		assertTrue(count.getConverted().length == 2);
		assertTrue(count.getRaw().length == 2);

		count = countMap.get(getAllTags().get(2));
		assertTrue(count.getConverted().length == 1);
		assertTrue(count.getRaw().length == 1);

		count = countMap.get(getAllTags().get(3));
		assertTrue(count.getConverted().length == 1);
		assertTrue(count.getRaw().length == 1);

		count = countMap.get(getAllTags().get(4));
		assertTrue(count.getConverted().length == 1);
		assertTrue(count.getRaw().length == 1);
	}

	public List<String> getAllTags() {
		return Lists.newArrayList(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.f"),
				StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content2"),
				StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.d.g"),
				StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.a.b"),
				StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content"));
	}

}
