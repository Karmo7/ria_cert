package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.parser.XmlElementParser;
import ee.ttu.xtee6parser.utilities.parser.XmlElementValueParser;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Marten on 13.04.2016.
 */
public class XmlElementParserTest {

	private XmlElementParser xmlElementParser;

	@Test
	public void getAllElementTest() throws Exception {
		String requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES);
		xmlElementParser = new XmlElementParser(requestMessage, true, true);
		List<Value> elementValues = xmlElementParser.getAllElementValues();

		assertEquals(5, elementValues.size());
	}

	@Test
	public void getAllElementValuesTest() throws Exception {
		String requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_2);
		xmlElementParser = new XmlElementValueParser(requestMessage, true, true);
		List<Value> elementValues = xmlElementParser.getAllElementValues();

		assertEquals(2, elementValues.size());
	}

	@Test(expected = RuntimeException.class)
	public void getAllElementValuesNoBodyTest() throws Exception {
		String requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_NO_BODY);
		xmlElementParser = new XmlElementParser(requestMessage, true, true);
		xmlElementParser.getAllElementValues();
	}

	@Test(expected = RuntimeException.class)
	public void getAllElementNoBodyTest() throws Exception {
		String requestMessage = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_NO_BODY);
		xmlElementParser = new XmlElementValueParser(requestMessage, true, true);
		xmlElementParser.getAllElementValues();
	}
}
