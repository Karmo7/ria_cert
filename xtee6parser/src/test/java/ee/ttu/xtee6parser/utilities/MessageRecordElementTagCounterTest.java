package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.counter.MessageRecordElementCounter;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import ee.ttu.xtee6parser.utilities.xpath.XPathUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.ttu.xtee6parser.model.Value.of;
import static org.junit.Assert.assertEquals;

/**
 * Created by Marten on 2.04.2016.
 */
public class MessageRecordElementTagCounterTest {

	private static final String separator = "\\.";

	@Test
	public void countByTagTest() throws Exception {
		List<MessageRecord> messageRecords = getMessageRecords();
		Map<Value, Long> countMap = new MessageRecordElementCounter(messageRecords).count();
		countMap.forEach(this::assertTag);
	}


	@Test
	public void countByTagEmptyFilterTagTest() throws Exception {
		List<MessageRecord> messageRecords = getMessageRecordsElementPathEmptyTag();
		Map<Value, Long> countMap = new MessageRecordElementCounter(messageRecords, XPathUtil.PATH_SERVICE_MEMBERCODE).count();
		countMap.forEach(this::assertTagNoOrEmptyTag);
	}

	@Test
	public void countByTagNoFilterTagTest() throws Exception {
		List<MessageRecord> messageRecords = getMessageRecordsElementPathNoTag();
		Map<Value, Long> countMap = new MessageRecordElementCounter(messageRecords, XPathUtil.PATH_SERVICE_MEMBERCODE).count();
		countMap.forEach(this::assertTagNoOrEmptyTag);
	}

	@Test
	public void countByTagFilterTest() throws Exception {
		List<MessageRecord> messageRecords = getMessageRecordsFilter();
		Map<Value, Long> countMap = new MessageRecordElementCounter(messageRecords, XPathUtil.PATH_SERVICE_MEMBERCODE).count();
		countMap.forEach(this::assertTagFilter);
	}

	private void assertTagFilter(Value key, Long count) {
		if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse"), XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid1111"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse"), XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid2222"))) {
			assertEquals(new Long(1), count);
		} else {
			throw new IllegalArgumentException("unknown tag: " + key.getValue() + " / " + key.getElementFilter());
		}
	}

	private void assertTagNoOrEmptyTag(Value key, Long count) {
		if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.f")))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content2")))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.d.g")))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.a.b")))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content")))) {
			assertEquals(new Long(2), count);
		} else {
			throw new IllegalArgumentException("unknown tag: " + key.getValue() + " / " + key.getElementFilter());
		}
	}

	private void assertTag(Value key, Long count) {
		if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.f")))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content2")))) {
			assertEquals(new Long(4), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.d.g")))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.a.b")))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content")))) {
			assertEquals(new Long(4), count);
		} else {
			throw new IllegalArgumentException("unknown tag: " + key.getValue() + " / " + key.getElementFilter());
		}
	}

	public static List<MessageRecord> getMessageRecords() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();

		String xml;
		MessageRecord messageRecord;

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES);
		messageRecord = new MessageRecord(xml);

		for (int i = 0; i < 3; i++) {
			messageRecords.add(messageRecord);
		}

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_2);
		messageRecord = new MessageRecord(xml);
		messageRecords.add(messageRecord);

		return messageRecords;
	}

	public static List<MessageRecord> getMessageRecordsElementPathEmptyTag() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();

		for (int i = 0; i < 2; i++) {
			String xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_EMPTY_TAG);
			messageRecords.add(new MessageRecord(xml));
		}

		return messageRecords;
	}

	public static List<MessageRecord> getMessageRecordsElementPathNoTag() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();


		for (int i = 0; i < 2; i++) {
			String xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_NO_TAG);
			messageRecords.add(new MessageRecord(xml));
		}

		return messageRecords;
	}

	public static List<MessageRecord> getMessageRecordsFilter() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();

		String xml;
		MessageRecord messageRecord;

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_FILTER_1);
		messageRecord = new MessageRecord(xml);

		for (int i = 0; i < 3; i++) {
			messageRecords.add(messageRecord);
		}

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_FILTER_2);
		messageRecord = new MessageRecord(xml);
		messageRecords.add(messageRecord);

		return messageRecords;
	}
}
