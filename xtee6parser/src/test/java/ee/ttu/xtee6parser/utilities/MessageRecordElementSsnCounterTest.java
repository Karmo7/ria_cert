package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.counter.MessageRecordElementCounter;
import ee.ttu.xtee6parser.utilities.counter.MessageRecordElementSsnCounter;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import ee.ttu.xtee6parser.utilities.xpath.XPathUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static ee.ttu.xtee6parser.model.Value.of;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Marten on 14.04.2016.
 */
public class MessageRecordElementSsnCounterTest {

	@Test
	public void testCount() throws Exception {
		MessageRecordElementCounter counter = new MessageRecordElementSsnCounter(getMessageRecords());
		Map<Value, Long> countMap = counter.count();
		countMap.forEach(this::assertSsn);
	}

	private void assertSsn(Value key, Long count) {
		if (key.equals(of("39209050000"))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of("47412090000"))) {
			assertEquals(new Long(1), count);
		} else if (key.equals(of("EE61201010000"))) {
			assertEquals(new Long(1), count);
		}
	}

	public List<MessageRecord> getMessageRecords() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();
		String xml;

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_SSN_1);
		messageRecords.add(new MessageRecord(xml));

		xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_SSN_2);
		messageRecords.add(new MessageRecord(xml));

		return messageRecords;
	}

	@Test
	public void testCountFilter() throws Exception {
		MessageRecordElementCounter counter = new MessageRecordElementSsnCounter(getMessageRecords(), XPathUtil.PATH_SERVICE_MEMBERCODE);
		Map<Value, Long> countMap = counter.count();
		countMap.forEach(this::assertSsnFilter);
	}

	private void assertSsnFilter(Value key, Long count) {
		if (key.equals(of("39209050000", XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid1111"))) {
			assertEquals(new Long(2), count);
		} else if (key.equals(of("39209050000", XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid3333"))) {
			assertEquals(new Long(1), count);
		} else if (key.equals(of("47412090000", XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid1111"))) {
			assertEquals(new Long(1), count);
		} else if (key.equals(of("EE61201010000", XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid1111"))) {
			assertEquals(new Long(1), count);
		}
	}

}
