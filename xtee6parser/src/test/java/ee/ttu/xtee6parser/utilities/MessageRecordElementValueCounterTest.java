package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.counter.MessageRecordElementValueCounter;
import ee.ttu.xtee6parser.utilities.xpath.XPathUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static ee.ttu.xtee6parser.model.Value.of;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by Marten on 2.05.2016.
 */
public class MessageRecordElementValueCounterTest {

	private static final String separator = "\\.";

	@Test
	public void countByValueTest() throws IOException {
		List<MessageRecord> messageRecords = MessageRecordElementTagCounterTest.getMessageRecords();
		Map<Value, Long> countMap = new MessageRecordElementValueCounter(messageRecords).count();
		countMap.forEach(this::assertValue);
	}

	private void assertValue(Value key, Long count) {
		if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.f") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "ffff"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content2") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "5"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content2") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "99"))) {
			assertEquals(new Long(1), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.c.d.g") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "gggggg"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.test.a.b") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "asd"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse.response.content") + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + "4"))) {
			assertEquals(new Long(4), count);
		} else {
			throw new IllegalArgumentException("unknown tag: " + key);
		}
	}

	@Test
	public void countByValueFilterTest() throws Exception {
		List<MessageRecord> messageRecords = MessageRecordElementTagCounterTest.getMessageRecordsFilter();
		Map<Value, Long> countMap = new MessageRecordElementValueCounter(messageRecords, XPathUtil.PATH_SERVICE_MEMBERCODE).count();
		countMap.forEach(this::assertValueFilter);
	}

	private void assertValueFilter(Value key, Long count) {
		if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse%:5555"),
				XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid1111"))) {
			assertEquals(new Long(3), count);
		} else if (key.equals(of(StringUtils.replaceSeparator(separator, "body.getrandomresponse%:6666"),
				XPathUtil.PATH_SERVICE_MEMBERCODE, StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR, "ttutudengid2222"))) {
			assertEquals(new Long(1), count);
		} else {
			throw new IllegalArgumentException("unknown tag: " + key.getValue() + " / " + key.getElementFilter());
		}
	}

}
