package ee.ttu.xtee6parser.utilities.properties;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

/**
 * Created by margus on 24.04.2016.
 */
public class XroadPropertiesFileParserTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	private File xroadConfFile;

	@Before
	public void setUp() throws Exception {
		xroadConfFile = tempFolder.newFile("proxy.ini");

	}

	@Test
	public void getDBPropertiesFileLocationTest() throws Exception {
		FileUtils.writeStringToFile(xroadConfFile, "database-properties=/etc/xroad/db.properties");
		assertEquals("/etc/xroad/db.properties", XroadPropertiesFileParser.getDBPropertiesFileLocation(xroadConfFile.getAbsolutePath()));
	}

	public void getDBPropertiesFromConfFileTest() throws Exception {
		FileUtils.writeStringToFile(xroadConfFile, "messagelog.hibernate.connection.driver_class = org.postgresql.Driver\n");
		FileUtils.writeStringToFile(xroadConfFile, "messagelog.hibernate.connection.url = jdbc:postgresql://127.0.0.1:5432/messagelog\n");
		FileUtils.writeStringToFile(xroadConfFile, "messagelog.hibernate.connection.username = messagelog\n");
		FileUtils.writeStringToFile(xroadConfFile, "messagelog.hibernate.connection.password = messagelog\n");
		Properties properties = XroadPropertiesFileParser.getDBPropertiesFromConfFile(xroadConfFile.getAbsolutePath());

		assertEquals("org.postgresql.Driver", properties.getProperty(DbPropertiesFields.DB_DRIVER.getValue()));
		assertEquals("jdbc:postgresql://127.0.0.1:5432/messagelog", properties.getProperty(DbPropertiesFields.DB_URL.getValue()));
		assertEquals("messagelog", properties.getProperty(DbPropertiesFields.DB_USERNAME.getValue()));
		assertEquals("messagelog", properties.getProperty(DbPropertiesFields.DB_PASSWORD.getValue()));


	}
}
