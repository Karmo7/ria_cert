package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.MessageCount;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.utilities.counter.MessageCounter;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Marten on 21.04.2016.
 */
public class MessageCounterTest {

	@Test
	public void testCount() throws Exception {

		List<MessageRecord> messageRecords = getMessageRecords();
		MessageCount count = new MessageCounter().count(messageRecords);

		assertEquals(new Long(14), count.getAllMessagesCount());
		assertEquals(new Long(5), count.getFaultyMessagesCount());
		assertEquals(new Long(9), count.getSuccesfulMessagesCount());
	}

	public List<MessageRecord> getMessageRecords() throws IOException {
		List<MessageRecord> messageRecords = new ArrayList<>();
		String xml;

		for (int i = 0; i < 5; i++) {
			xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_FAULT);
			messageRecords.add(new MessageRecord(xml));
		}

		for (int i = 0; i < 9; i++) {
			xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_SSN_2);
			messageRecords.add(new MessageRecord(xml));
		}

		return messageRecords;
	}
}
