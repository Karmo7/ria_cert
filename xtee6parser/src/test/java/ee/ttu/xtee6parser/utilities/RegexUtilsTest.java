package ee.ttu.xtee6parser.utilities;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Marten on 14.04.2016.
 */
public class RegexUtilsTest {

	@Test
	public void isSsnTest() {
		assertTrue(RegexUtils.isSsn("39211101910"));
		assertTrue(RegexUtils.isSsn("EE39211101910"));
		assertTrue(RegexUtils.isSsn(" 39211101910 "));
		assertTrue(RegexUtils.isSsn("\n39211101910\t"));

		assertFalse(RegexUtils.isSsn("0039211101910"));
		assertFalse(RegexUtils.isSsn("9211101910"));
		assertFalse(RegexUtils.isSsn("39211101910EE"));
		assertFalse(RegexUtils.isSsn("EE392111019"));
		assertFalse(RegexUtils.isSsn("3921110191c"));
		assertFalse(RegexUtils.isSsn("392111 01910"));
		assertFalse(RegexUtils.isSsn("aaaaaaaaaaa"));
		assertFalse(RegexUtils.isSsn("aaaaaaaaaaaaa"));
		assertFalse(RegexUtils.isSsn("79211101910"));
		assertFalse(RegexUtils.isSsn("39221101910"));
		assertFalse(RegexUtils.isSsn("39211401910"));
		assertFalse(RegexUtils.isSsn(null));
		assertFalse(RegexUtils.isSsn(" \t\n "));
	}
}
