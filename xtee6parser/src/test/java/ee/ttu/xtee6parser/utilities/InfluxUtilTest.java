package ee.ttu.xtee6parser.utilities;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.Assert.assertEquals;

/**
 * Created by margus on 2.04.2016.
 */
public class InfluxUtilTest {
	@Test
	public void getDateFromInfluxTimeTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11.386Z";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 386000000, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));

	}

	@Test(expected = RuntimeException.class)
	public void getDateFromInfluxTimeIncorrectValueTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11.s386Z";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 386000000, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));

	}

	@Test
	public void getDateFromInfluxTimeSingleMilliSecTest() throws Exception {
		String influxTime = "1970-01-17T22:01:23.8Z";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1461683L, 800000000, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));

	}

	@Test
	public void getDateFromInfluxTimeNoTImeZoneTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11.386";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 386000000, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));

	}

	@Test
	public void getDateFromInfluxTimeWithNanoSecondsTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11.386570484Z";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 386570484, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));
	}

	@Test
	public void getDateFromInfluxTimeWithNanoSecondsWithOutTimeZoneTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11.386570484";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 386570484, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));
	}

	@Test
	public void getDateFromInfluxTimeWithOutSecondsTest() throws Exception {
		String influxTime = "2016-04-02T17:34:11Z";
		LocalDateTime date = LocalDateTime.ofEpochSecond(1459618451L, 0, ZoneOffset.UTC);
		assertEquals(date, InfluxUtil.getDateFromInfluxTime(influxTime));
	}

}
