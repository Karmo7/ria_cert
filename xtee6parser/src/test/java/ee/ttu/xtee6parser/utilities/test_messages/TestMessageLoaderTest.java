package ee.ttu.xtee6parser.utilities.test_messages;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Marten on 2.04.2016.
 */
public class TestMessageLoaderTest {

	@Test
	public void loadTest() throws IOException {
		String message = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES);
		Assert.assertNotNull(message);
	}

	@Test(expected = IOException.class)
	public void loadFileNotFoundTest() throws IOException {
		TestMessageLoader.load("no_such_file_exists_$");
	}

	@Test(expected = IllegalArgumentException.class)
	public void loadIllegalArgumentTest() throws IOException {
		TestMessageLoader.load(null);
	}
}
