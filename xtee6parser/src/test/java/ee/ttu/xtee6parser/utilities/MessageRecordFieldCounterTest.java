package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.utilities.counter.MessageRecordFieldCounter;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ee.ttu.xtee6parser.model.Value.of;

/**
 * Created by Karmo on 019 19 04 2016.
 */
public class MessageRecordFieldCounterTest {

	@Test
	public void mergeMessages() throws Exception {
		List<MessageRecord> messageRecords = new ArrayList<>();
		messageRecords.add(createMessageRecord("1", "1"));
		messageRecords.add(createMessageRecord("1", "2"));
		messageRecords.add(createMessageRecord("1", "2"));

		MessageRecordFieldCounter counter = new MessageRecordFieldCounter(messageRecords, "memberCode");
		Assert.assertEquals(new Long(3), counter.count().get(of("1")));

		counter = new MessageRecordFieldCounter(messageRecords, "subsystemCode");
		Assert.assertEquals(new Long(1), counter.count().get(of("1")));
		Assert.assertEquals(new Long(2), counter.count().get(of("2")));
	}

	private MessageRecord createMessageRecord(String memberCode, String subsystemCode) {
		MessageRecord messageRecord = new MessageRecord();
		messageRecord.setMemberCode(memberCode);
		messageRecord.setSubsystemCode(subsystemCode);
		return messageRecord;
	}

}
