package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.parser.XmlElementParser;
import ee.ttu.xtee6parser.utilities.parser.XmlElementSsnParser;
import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Marten on 14.04.2016.
 */
public class XmlElementSsnParserTest {

	@Test
	public void getAllElementValuesTest1() throws Exception {
		String xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_SSN_1);
		XmlElementParser xmlElementParser = new XmlElementSsnParser(xml);
		List<Value> ssnList = xmlElementParser.getAllElementValues();
		assertEquals(3, ssnList.size());
	}

	@Test
	public void getAllElementValuesTest2() throws Exception {
		String xml = TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_SSN_2);
		XmlElementParser xmlElementParser = new XmlElementSsnParser(xml);
		List<Value> ssnList = xmlElementParser.getAllElementValues();
		assertEquals(1, ssnList.size());
	}
}
