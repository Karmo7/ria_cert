package ee.ttu.xtee6parser.utilities.test_messages;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Marten on 2.04.2016.
 */
public class TestMessageLoader {

	public static final String SOAP_MESSAGE_ELEMENT_VALUES = "soap_message_element_values";
	public static final String SOAP_MESSAGE_ELEMENT_VALUES_2 = "soap_message_element_values_2";
	public static final String SOAP_MESSAGE_NO_BODY = "soap_message_no_body";
	public static final String SOAP_MESSAGE_REQUEST_1 = "soap_message_request_1";
	public static final String SOAP_MESSAGE_RESPONSE_1 = "soap_message_response_1";
	public static final String SOAP_MESSAGE_FIN_REQUEST_1 = "soap_message_fin_request_1";
	public static final String SOAP_MESSAGE_REQUEST_MISSING_SERVICECODE = "soap_message_request_missing_servicecode";
	public static final String SOAP_MESSAGE_REQUEST_EMPTY_SERVICECODE = "soap_message_request_empty_servicecode";
	public static final String SOAP_MESSAGE_SSN_1 = "soap_message_ssn_1";
	public static final String SOAP_MESSAGE_SSN_2 = "soap_message_ssn_2";

	public static final String SOAP_MESSAGE_ELEMENT_VALUES_NO_TAG = "soap_message_element_values_no_tag";
	public static final String SOAP_MESSAGE_ELEMENT_VALUES_EMPTY_TAG = "soap_message_element_values_empty_tag";
	public static final String SOAP_MESSAGE_FILTER_1 = "soap_message_filter_1";
	public static final String SOAP_MESSAGE_FILTER_2 = "soap_message_filter_2";

	public static final String SOAP_MESSAGE_FAULT = "soap_message_fault";
	private static final String PATH = "src/test/java/ee/ttu/xtee6parser/utilities/test_messages/";

	private TestMessageLoader() {
	}

	public static String load(String fileName) throws IOException {
		if (fileName == null) {
			throw new IllegalArgumentException("fileName cannot be null!");
		}
		Path file = Paths.get(PATH + fileName);
		return new String(Files.readAllBytes(file));
	}


}
