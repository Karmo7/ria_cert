package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.utilities.test_messages.TestMessageLoader;
import org.junit.Test;
import org.w3c.dom.Document;

import static org.junit.Assert.assertNotNull;

/**
 * Created by Marten on 21.04.2016.
 */
public class XmlDocumentFactoryTest {

	@Test
	public void testCreateWithXml() throws Exception {
		Document document = XmlDocumentFactory.create(TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_2));
		assertNotNull(document);
	}

	@Test
	public void testCreateWithAllParams() throws Exception {
		Document document = XmlDocumentFactory.create(
				TestMessageLoader.load(TestMessageLoader.SOAP_MESSAGE_ELEMENT_VALUES_2),
				false, false);
		assertNotNull(document);
	}
}
