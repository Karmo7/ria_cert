package ee.ttu.xtee6parser.utilities.grouped_value_counter;

import ee.ttu.xtee6parser.model.Value;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import static ee.ttu.xtee6parser.model.Value.of;
import static org.junit.Assert.*;

/**
 * Created by Marten on 20.04.2016.
 */
public class CountTest {

	@Test
	public void testGetIsEmpty() throws Exception {
		Count count = new Count();
		assertTrue(count.count().isEmpty());
	}

	@Test
	public void testGetConvertedIsEmpty() throws Exception {
		Count count = new Count();
		assertEquals(0, count.getConverted().length);
	}

	@Test
	public void testGetRawIsEmpty() throws Exception {
		Count count = new Count();
		assertEquals(0, count.getRaw().length);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddNull() throws Exception {
		Count count = new Count();
		count.add(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructNull() {
		Count count = new Count(null);
	}

	@Test
	public void testAddNotEmpty() throws Exception {
		Count count = new Count();
		count.add(new Value("str"));
		assertFalse(count.getValueCountMap().isEmpty());
	}

	@Test
	public void testGetSize3() throws Exception {
		Count count = getCount3();
		assertEquals(3, count.count().size());
	}

	@Test
	public void testGetConvertedSize3() throws Exception {
		Count count = getCount3();
		assertEquals(3, count.getConverted().length);
	}

	@Test
	public void testGetRawSize3() throws Exception {
		Count count = getCount3();
		assertEquals(3, count.getRaw().length);
	}

	@Test
	public void testGetConvertedCount() throws Exception {
		Count count = getCount3();
		ValueCount[] values = count.getConverted();

		for (ValueCount valueCount : values) {
			assertGetConvertedCount(valueCount);
		}
	}

	private void assertGetConvertedCount(ValueCount valueCount) {
		if (valueCount.value.equals(new Value("str1"))) {
			assertEquals(2, valueCount.count, 0.01);
		} else {
			assertEquals(1, valueCount.count, 0.01);
		}
	}

	@Test
	public void testGetCount() throws Exception {
		Count count = getCountValue4();
		assertEquals(new Long(4), count.count().getOrDefault(of("str1"), new Long(-1)));
	}

	@Test
	public void testGetRawCount() throws Exception {
		Count count = getCountValue4();
		assertEquals(4, count.getRaw()[0], 0.01);
	}

	@NotNull
	private Count getCount3() {
		Count count = new Count();

		count.add(new Value("str1"));
		count.add(new Value("str1"));
		count.add(new Value("str2"));
		count.add(new Value("str3"));
		return count;
	}

	@NotNull
	private Count getCountValue4() {
		Count count = new Count();

		count.add(new Value("str1"));
		count.add(new Value("str1"));
		count.add(new Value("str1"));
		count.add(new Value("str1"));

		return count;
	}

}
