package ee.ttu.xtee6parser.statistics;

import ee.ttu.xtee6parser.model.Value;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

/**
 * Created by Karmo on 005 5 04 2016.
 */
public class StatTest {

	private HashMap<Value, Long> elementCountMap = new HashMap<>();

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void skewness_whenCountsAreEqual_returnsNan() throws Exception {
		elementCountMap.put(new Value("a"), new Long(1));
		elementCountMap.put(new Value("b"), new Long(1));
		Assert.assertEquals(Double.NaN, new Stat(elementCountMap).skewness(), 0.0);
	}

	@Test
	public void skewness_whenMedianBelowAvg_isPositive() throws Exception {
		elementCountMap.put(new Value("a"), new Long(1));
		elementCountMap.put(new Value("b"), new Long(1));
		elementCountMap.put(new Value("c"), new Long(3));
		Assert.assertTrue(new Stat(elementCountMap).skewness() > 0);
	}

	@Test
	public void skewness_whenMedianAboveAvg_isNegative() throws Exception {
		elementCountMap.put(new Value("a"), new Long(1));
		elementCountMap.put(new Value("b"), new Long(3));
		elementCountMap.put(new Value("c"), new Long(3));
		Assert.assertTrue(new Stat(elementCountMap).skewness() < 0);
	}

	@Test
	public void kurtosis_whenCountsAreEqual_returnsNan() throws Exception {
		elementCountMap.put(new Value("a"), new Long(1));
		elementCountMap.put(new Value("b"), new Long(1));
		elementCountMap.put(new Value("c"), new Long(1));
		elementCountMap.put(new Value("d"), new Long(1));
		Assert.assertEquals(Double.NaN, new Stat(elementCountMap).kurtosis(), 0.0);
	}

	@Test
	public void kurtosis_whenPeakAboveNormal_isPositive() throws Exception {
		elementCountMap.put(new Value("a"), new Long(1));
		elementCountMap.put(new Value("b"), new Long(1));
		elementCountMap.put(new Value("c"), new Long(10));
		elementCountMap.put(new Value("d"), new Long(1));
		System.out.println(new Stat(elementCountMap).kurtosis());
		Assert.assertTrue(new Stat(elementCountMap).kurtosis() > 0);
	}

	@Test
	public void kurtosis_whenPeakBelowNormal_isNegative() throws Exception {
		elementCountMap.put(new Value("a"), new Long(9));
		elementCountMap.put(new Value("b"), new Long(9));
		elementCountMap.put(new Value("c"), new Long(10));
		elementCountMap.put(new Value("d"), new Long(10));
		System.out.println(new Stat(elementCountMap).kurtosis());
		Assert.assertTrue(new Stat(elementCountMap).kurtosis() < 0);
	}
}
