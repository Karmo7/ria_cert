package ee.ttu.xtee6parser.program;

import ee.ttu.xtee6parser.utilities.HostnameResolver;
import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.properties.AbstractProperties;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by margus on 9.04.2016.
 */
public class ParserProperties extends AbstractProperties {
	private static final String PROPERTY_FILE_NAME = "ParserProperties.txt";

	private HostnameResolver hostnameResolver;

	private long pollIntervalSeconds;
	private long pollDepthSeconds;
	private String hostname;
	private boolean loadDBPropertiesFromXroad;
	private String xroadProxyConfFileLocation;

	public ParserProperties(HostnameResolver hostnameResolver) {
		this(PROPERTY_FILE_NAME, hostnameResolver);
	}

	public ParserProperties(String fileName, HostnameResolver hostnameResolver) {
		super(fileName);
		this.hostnameResolver = hostnameResolver;
		loadExtraProperties();
	}

	public ParserProperties(Properties properties, HostnameResolver hostnameResolver) {
		super(properties);
		this.hostnameResolver = hostnameResolver;
		loadExtraProperties();
	}

	public ParserProperties(InputStream propertiesFile, HostnameResolver hostnameResolver) {
		super(propertiesFile);
		this.hostnameResolver = hostnameResolver;
		loadExtraProperties();
	}


	public long getPollIntervalSeconds() {
		return pollIntervalSeconds;
	}

	public long getPollDepthSeconds() {
		return pollDepthSeconds;
	}

	public String getHostname() {
		return hostname;
	}

	public boolean loadDBPropertiesFromXroad() {
		return loadDBPropertiesFromXroad;
	}

	public String getXroadProxyConfFileLocation() {
		return xroadProxyConfFileLocation;
	}

	@Override
	protected void loadProperties(Properties properties) {
		String pollIntervalValue = (String) properties.get("pollinterval_seconds");
		validateProperty("pollinterval_seconds", pollIntervalValue);
		loadPollInterval(pollIntervalValue);

		String pollDepthValue = (String) properties.get("polldepth_seconds");
		if (tryValidateProperty(pollDepthValue) == false)
			pollDepthValue = pollIntervalValue;
		loadPollDepth(pollDepthValue);

		String loadDBFromXroadValue = (String) properties.get("load_db_properties_from_xroad_conf");
		loadDBPropertiesConf(loadDBFromXroadValue);

		loadXroadProxyConfFileLocation((String) properties.get("xroad_proxy_conf_file"));
	}

	private void loadExtraProperties() {
		String hostnameValue = (String) properties.get("hostname");
		loadHostname(hostnameValue);
	}

	private void loadHostname(String hostnameValue) {
		if (StringUtils.isBlank(hostnameValue)) {
			hostname = hostnameResolver.getHostname();
		} else {
			hostname = hostnameValue;
		}
	}

	private void loadPollInterval(String pollIntervalValue) {
		try {
			pollIntervalSeconds = Long.parseLong(pollIntervalValue);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Incorrect pollinterval_seconds format");
		}
	}

	private void loadPollDepth(String pollDepthValue) {
		try {
			pollDepthSeconds = Long.parseLong(pollDepthValue);
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Incorrect pollinterval_seconds format");
		}
	}

	private void loadDBPropertiesConf(String loadDBFromXroadValue) {
		loadDBPropertiesFromXroad = Boolean.valueOf(loadDBFromXroadValue);
	}

	private void loadXroadProxyConfFileLocation(String xroadProxyConfFileLocationValue) {
		if (loadDBPropertiesFromXroad && (StringUtils.isBlank(xroadProxyConfFileLocationValue))) {
			throw new IllegalArgumentException("xroad_proxy_conf_file is required when load_db_properties_from_xroad_conf is true");
		}

		xroadProxyConfFileLocation = xroadProxyConfFileLocationValue;
	}
}
