package ee.ttu.xtee6parser.program;

import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.InfluxProperties;
import ee.ttu.xtee6parser.influx.MeasurementFactory;
import ee.ttu.xtee6parser.influx.impl.InfluxDaoImpl;
import ee.ttu.xtee6parser.jdbc.DbProperties;
import ee.ttu.xtee6parser.jdbc.XteeDao;
import ee.ttu.xtee6parser.jdbc.impl.XteeDaoImpl;
import ee.ttu.xtee6parser.parser.Parser;
import ee.ttu.xtee6parser.parser.ParsersRunner;
import ee.ttu.xtee6parser.parser.impl.MessageRecordParser;
import ee.ttu.xtee6parser.parser.impl.MessageRecordStandardWorker;
import ee.ttu.xtee6parser.parser.impl.MessageRecordStatisticsWorker;
import ee.ttu.xtee6parser.parser.impl.MessageRecordWorker;
import ee.ttu.xtee6parser.utilities.HostnameResolver;
import ee.ttu.xtee6parser.utilities.properties.XroadPropertiesFileParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Marten on 12.03.2016.
 */
public class Xtee6Parser {

	private static ParserProperties properties;
	private static DbProperties dbProperties;
	private static InfluxProperties influxProperties;
	private static InfluxDao influxDao;
	private static XteeDao xteeDao;
	private static MeasurementFactory measurementFactory;
	private static final Logger logger = LogManager.getLogger(Xtee6Parser.class);


	public static void main(String[] args) {
		try {
			loadProperties();
			createDependencies();
			runParsers();
		} catch (XteeParserException e) {
			e.logError();
		} catch (Exception e) {
			logger.error("Technical error:", e);
		}
	}

	private static void runParsers() throws InterruptedException {
		List<Parser> parsers = getParsers();
		ParsersRunner parsersRunner = new ParsersRunner(parsers, properties.getPollIntervalSeconds());
		parsersRunner.run();
	}

	private static List<Parser> getParsers() {
		List<Parser> parsers = new ArrayList<>();
		parsers.add(new MessageRecordParser(influxDao, xteeDao,
				getMessageRecordWorkers(), properties.getPollDepthSeconds()));
		return parsers;
	}

	@NotNull
	private static List<MessageRecordWorker> getMessageRecordWorkers() {
		List<MessageRecordWorker> workers = new ArrayList<>();
		workers.add(new MessageRecordStandardWorker(measurementFactory, influxDao));
		workers.add(new MessageRecordStatisticsWorker(influxDao));
		return workers;
	}

	private static void createDependencies() throws FileNotFoundException {
		xteeDao = new XteeDaoImpl(dbProperties);
		influxDao = new InfluxDaoImpl(influxProperties);
		measurementFactory = new MeasurementFactory(influxDao, properties);
	}

	private static void loadProperties() {
		loadParserProperties();
		if (properties.loadDBPropertiesFromXroad()) {
			loadDatabasePropertiesFromXroad();
		} else {
			loadDatabaseProperties();
		}
		loadInfluxProperties();
	}

	private static void loadParserProperties() {
		logger.info("Loading parser properties");
		properties = new ParserProperties(new HostnameResolver());
	}

	private static void loadDatabaseProperties() {
		logger.info("Loading database properties");
		dbProperties = new DbProperties("DBConnection.txt");
	}

	private static void loadDatabasePropertiesFromXroad() {
		String dataBaseConfFile = XroadPropertiesFileParser
				.getDBPropertiesFileLocation(properties.getXroadProxyConfFileLocation());
		Properties properties = XroadPropertiesFileParser.getDBPropertiesFromConfFile(dataBaseConfFile);
		dbProperties = new DbProperties(properties);
	}

	private static void loadInfluxProperties() {
		logger.info("Loading influx properties");
		influxProperties = new InfluxProperties("InfluxDBProperties.txt");
	}

}
