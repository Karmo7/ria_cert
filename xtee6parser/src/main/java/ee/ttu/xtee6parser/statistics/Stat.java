package ee.ttu.xtee6parser.statistics;

import ee.ttu.xtee6parser.model.Value;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import java.util.Map;

/**
 * Created by Karmo on 005 5 04 2016.
 */
public class Stat {

	private DescriptiveStatistics statistics;

	public Stat(Map<Value, Long> elementValueCountMap) {
		double[] elementValueCounts = elementValueCountMap.values().stream()
				.mapToDouble(d -> d).toArray();
		statistics = new DescriptiveStatistics(elementValueCounts);
	}

	public double skewness() {
		return statistics.getSkewness();
	}

	/**
	 * @return NaN if less than 4 values.
	 */
	public double kurtosis() {
		return statistics.getKurtosis();
	}
}
