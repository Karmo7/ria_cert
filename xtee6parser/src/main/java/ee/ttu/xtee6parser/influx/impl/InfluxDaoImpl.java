package ee.ttu.xtee6parser.influx.impl;

import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.InfluxProperties;
import ee.ttu.xtee6parser.utilities.InfluxUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDB.ConsistencyLevel;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import retrofit.RetrofitError;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class InfluxDaoImpl implements InfluxDao {

	private static final Logger logger = LogManager.getLogger(InfluxDaoImpl.class);
	private static final int MAX_BATCH_SIZE = 2000;
	private static final int MAX_BATCH_LENGTH_MS = 100;

	private InfluxProperties influxProperties;
	private InfluxDB influxDB;

	public InfluxDaoImpl(InfluxProperties influxProperties) {
		this.influxProperties = influxProperties;
		influxDB = getConnection();
		influxDB.enableBatch(MAX_BATCH_SIZE, MAX_BATCH_LENGTH_MS, TimeUnit.MILLISECONDS);
	}

	@Override
	public void saveMeasurementPoints(List<Point> measurmentPoints) {
		try {
			BatchPoints batchPoints = BatchPoints.database(influxProperties.getDbName())
					.retentionPolicy(influxProperties.getRetensionPolicy())
					.consistency(ConsistencyLevel.ALL).build();
			measurmentPoints.stream().forEach(x -> batchPoints.point(x));

			influxDB.write(batchPoints);
		} catch (RetrofitError e) {
			throw new XteeParserException("Could not save measurement points to Influx!", e);
		}
	}

	@Override
	public LocalDateTime getLastSavedMeasurementPointTime() {
		try {
			System.out.println(influxDB.ping());
			Query query = new Query("select time, count  from \"" + influxProperties.getRetensionPolicy() + "\".messagelog order by time desc  limit 1",
					influxProperties.getDbName());
			QueryResult result = influxDB.query(query);

			String pointTimeValue = InfluxUtil.getFirstValueFromQueryResult(result);
			if (pointTimeValue != null)
				return InfluxUtil.getDateFromInfluxTime(pointTimeValue);
		} catch (RetrofitError e) {
			throw new XteeParserException("Could not get last saved measurement point time from influx!", e);
		}

		return null;
	}

	@Override
	public LocalDateTime getRequestMessageTime(String id) {
		try {
			Query query = new Query("select time, count from \"" + influxProperties.getRetensionPolicy() + "\".messagelog where queryid = '" + id + "' and response = '0' order by time desc limit 1",
					influxProperties.getDbName());
			QueryResult result = influxDB.query(query);
			String messageTimeValue = InfluxUtil.getFirstValueFromQueryResult(result);
			if (messageTimeValue != null) {
				return InfluxUtil.getDateFromInfluxTime(messageTimeValue);
			}
		} catch (RetrofitError e) {
			throw new XteeParserException("Could not get request message time from influx, request id=" + id, e);
		}
		return null;
	}

	@Override
	public InfluxDB getConnection() {
		return InfluxDBFactory.connect(influxProperties.getUrl(),
				influxProperties.getUser(), influxProperties.getPassword());
	}


}
