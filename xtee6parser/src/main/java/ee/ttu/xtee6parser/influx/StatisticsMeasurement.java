package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.model.MessageStatistics;
import ee.ttu.xtee6parser.utilities.DateUtils;
import org.influxdb.dto.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Karmo on 006 6 04 2016.
 */
public class StatisticsMeasurement {

	public List<Point> getMeasurementPoints(String measurement, MessageStatistics messageStatistics) {
		List<Point> list = new ArrayList<>();
		if (messageStatistics.isValid())
			list.add(getMeasurementPoint(measurement, messageStatistics));
		return list;
	}

	private Point getMeasurementPoint(String measurement, MessageStatistics messageStatistics) {
		return Point.measurement(measurement).time(DateUtils.time(), TimeUnit.MILLISECONDS)
				.field("bodyskewness", messageStatistics.getBodySkewness())
				.field("bodykurtosis", messageStatistics.getBodyKurtosis())
				.build();
	}
}
