package ee.ttu.xtee6parser.influx;

import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;

import java.time.LocalDateTime;
import java.util.List;

public interface InfluxDao {
	InfluxDB getConnection();

	void saveMeasurementPoints(List<Point> measurementPoints);

	LocalDateTime getLastSavedMeasurementPointTime();

	LocalDateTime getRequestMessageTime(String id);
}
