package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.program.ParserProperties;
import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.parser.XRoadSoapMessageParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.influxdb.dto.Point;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by margus on 19.03.2016.
 */
public class MeasurementFactory {

	private static final Logger logger = LogManager.getLogger(MeasurementFactory.class);

	private InfluxDao influxDao;
	private ParserProperties parserProperties;

	public MeasurementFactory(InfluxDao influxDao, ParserProperties parserProperties) {
		this.influxDao = influxDao;
		this.parserProperties = parserProperties;
	}

	public List<Point> getMeasurmentPoints(List<MessageRecord> messageRecords) {

		return messageRecords.stream()
				.map(x -> getMeasurementPoint(x, messageRecords)).collect(Collectors.toList());
	}

	private Point getMeasurementPoint(MessageRecord messageRecord, List<MessageRecord> messageRecords) {
		XRoadSoapMessageParser xRoadSoapMessageParser;
		try {
			xRoadSoapMessageParser = new XRoadSoapMessageParser(messageRecord.getMessage());
		} catch (IOException | SAXException | ParserConfigurationException | IllegalArgumentException e) {
			logger.warn("Could not parse XML", e);
			xRoadSoapMessageParser = null;
		}


		return Point.measurement("messagelog").time(messageRecord.getTime(), TimeUnit.MILLISECONDS)
				.tag("membercode", StringUtils.getDefaultIfNullOrEmpty(messageRecord.getMemberCode(), "MEMCODEMISSING"))
				.tag("subsystemcode", StringUtils.getDefaultIfNullOrEmpty(messageRecord.getSubsystemCode(), "SYSCODEMISSING"))
				.tag("response", messageRecord.isResponse() ? "1" : "0")
				.field("count", 1)
				.tag("servicename",
						Optional.ofNullable(xRoadSoapMessageParser == null ? null :
								StringUtils.getDefaultIfNullOrEmpty(xRoadSoapMessageParser.getServiceCode(), "SERVICECODEMISSING"))
								.orElse("SERVICECODEMISSING"))
				.tag("serviceversion",
						Optional.ofNullable(
								xRoadSoapMessageParser == null ? null :
										StringUtils.getDefaultIfNullOrEmpty(xRoadSoapMessageParser.getServiceVersion(), "SERVICEVERSIONMISSING"))
								.orElse("SERVICEVERSIONMISSING"))
				.field("responsetime", messageRecord.isResponse() ? getResponseAndItsRequestTimeDiff(messageRecord, messageRecords) : 0)
				.tag("messagehash",
						StringUtils.getDefaultIfNullOrEmpty(
								StringUtils.getMD5HashOfString(messageRecord.getMessage()), "MESSAGEHASHMISSING"))
				.field("messagesize", messageRecord.getMessage() != null ? messageRecord.getMessage().getBytes().length : 0)
				.tag("queryid", messageRecord.getQueryId() + "")
				.tag("host", StringUtils.getDefaultIfNullOrEmpty(parserProperties.getHostname(), "HOSTNAMEMISSING"))
				.build();

	}

	private long getResponseAndItsRequestTimeDiff(MessageRecord response, List<MessageRecord> messageRecords) {
		long timeDiff;

		timeDiff = messageRecords.stream().filter(x -> x.getQueryId().equals(response.getQueryId()) && !x.isResponse())
				.findFirst().map(x -> response.getTime() - x.getTime()).orElse(0L);
		if (timeDiff == 0) {
			try{
				LocalDateTime requestTime = influxDao.getRequestMessageTime(response.getQueryId());
				if (requestTime != null) {
					timeDiff = response.getTime() - requestTime.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
				}
			}catch (XteeParserException e){
				logger.warn("Could not get request time from influx", e);
			}

		}

		return timeDiff;
	}

}
