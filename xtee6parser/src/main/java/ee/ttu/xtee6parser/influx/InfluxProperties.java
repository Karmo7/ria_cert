package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.properties.AbstractProperties;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Karmo on 022 22 03 2016.
 */
public class InfluxProperties extends AbstractProperties {

	public static final String PROPERTY_FILE_NAME = "InfluxDBProperties.txt";

	private static final String DEFAULT_RETENSION_POLICY = "default";

	private String url;
	private String user;
	private String password;
	private String dbName;
	private String retensionPolicy;

	public InfluxProperties() {
		this(PROPERTY_FILE_NAME);
	}

	public InfluxProperties(String fileName) {
		super(fileName);
	}

	public InfluxProperties(Properties properties) {
		super(properties);
	}

	public InfluxProperties(InputStream propertiesFile) {
		super(propertiesFile);
	}

	@Override
	protected void loadProperties(Properties properties) {
		validateProperty("url", properties.getProperty("url"));
		url = properties.getProperty("url");
		validateProperty("user", properties.getProperty("user"));
		user = properties.getProperty("user");
		validateProperty("password", properties.getProperty("password"));
		password = properties.getProperty("password");
		validateProperty("dbName", properties.getProperty("dbName"));
		dbName = properties.getProperty("dbName");
		if (!StringUtils.isBlank(properties.getProperty("retension-policy"))) {
			retensionPolicy = properties.getProperty("retension-policy");
		} else {
			retensionPolicy = DEFAULT_RETENSION_POLICY;
		}

	}

	public String getUrl() {
		return url;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public String getDbName() {
		return dbName;
	}

	public String getRetensionPolicy() {
		return retensionPolicy;
	}
}
