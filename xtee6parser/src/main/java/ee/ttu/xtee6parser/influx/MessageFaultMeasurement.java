package ee.ttu.xtee6parser.influx;

import ee.ttu.xtee6parser.model.MessageCount;
import ee.ttu.xtee6parser.utilities.DateUtils;
import org.influxdb.dto.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Karmo on 007 7 05 2016.
 */
public class MessageFaultMeasurement {
	private String measurement;

	public MessageFaultMeasurement(String measurement) {
		this.measurement = measurement;
	}

	public List<Point> getMeasurementPoints(MessageCount messageCount) {
		List<Point> list = new ArrayList<>();
		list.add(getMeasurementPoint(messageCount));
		return list;
	}

	private Point getMeasurementPoint(MessageCount messageCount) {
		return Point.measurement(measurement).time(DateUtils.time(), TimeUnit.MILLISECONDS)
				.field("messagecount", messageCount.getAllMessagesCount())
				.field("successfulmessagecount", messageCount.getSuccesfulMessagesCount())
				.field("faultymessagecount", messageCount.getFaultyMessagesCount())
				.build();
	}
}
