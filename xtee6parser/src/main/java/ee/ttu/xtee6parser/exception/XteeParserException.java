package ee.ttu.xtee6parser.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Marten on 28.04.2016.
 */
public class XteeParserException extends RuntimeException {

	private String message;
	private Throwable cause;
	private Class errorSource;

	public XteeParserException(String message, Throwable cause) {
		this(message, cause, null);
	}

	public XteeParserException(String message, Throwable cause, Class errorSource) {
		super(message, cause);
		this.message = message;
		this.cause = cause;
		this.errorSource = errorSource;
		validate();
	}

	public void logError() {
		getLogger().error(message, cause);
	}

	private void validate() {
		if (message == null || cause == null) {
			throw new IllegalArgumentException("message and cause cannot be null");
		}
	}

	private Logger getLogger() {
		if (errorSource != null) {
			return LogManager.getLogger(errorSource);
		} else {
			return LogManager.getLogger();
		}
	}

}
