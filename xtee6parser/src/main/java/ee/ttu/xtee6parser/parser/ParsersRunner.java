package ee.ttu.xtee6parser.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Created by margus on 9.04.2016.
 */
public class ParsersRunner {

	private static final Logger logger = LogManager.getLogger(ParsersRunner.class);

	private List<Parser> parsers;
	private long pollIntervalSeconds;

	public ParsersRunner(List<Parser> parsers, long pollIntervalSeconds) {
		this.parsers = parsers;
		this.pollIntervalSeconds = pollIntervalSeconds;
	}


	public void run() throws InterruptedException {
		logger.info("Starting parsers runner, running " + parsers.size()
				+ " parsers with interval " + pollIntervalSeconds + " seconds");
		while (true) {
			runParsers();
			sleep();
		}
	}

	public void runParsers() {
		parsers.stream().forEach(x -> runParser(x));//maybe parallelStream
	}

	private void runParser(Parser parser) {
		try {
			parser.parse();
		} catch (Exception e) {
			logger.error("Error when running parser " + parser.getClass(), e);
		}

	}

	private void sleep() throws InterruptedException {
		Thread.sleep(pollIntervalSeconds * 1000);
	}
}
