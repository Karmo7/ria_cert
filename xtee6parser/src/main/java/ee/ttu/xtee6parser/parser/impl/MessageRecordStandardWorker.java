package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.MeasurementFactory;
import ee.ttu.xtee6parser.model.MessageRecord;

import java.util.List;

/**
 * Created by Karmo on 012 12 04 2016.
 */
public class MessageRecordStandardWorker implements MessageRecordWorker {
	private MeasurementFactory measurementFactory;
	private InfluxDao influxDao;

	public MessageRecordStandardWorker(
			MeasurementFactory measurementFactory, InfluxDao influxDao) {
		this.measurementFactory = measurementFactory;
		this.influxDao = influxDao;
	}

	@Override
	public void run(List<MessageRecord> messageRecordList) {
		influxDao.saveMeasurementPoints(
				measurementFactory.getMeasurmentPoints(messageRecordList));
	}
}
