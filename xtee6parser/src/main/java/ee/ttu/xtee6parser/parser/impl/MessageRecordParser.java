package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.jdbc.XteeDao;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 * Created by margus on 9.04.2016.
 */
public class MessageRecordParser implements Parser {
	private InfluxDao influxDao;
	private XteeDao xteeDao;
	private List<MessageRecordWorker> messageRecordWorkerList;
	private long pollDepthSeconds;

	public MessageRecordParser(InfluxDao influxDao, XteeDao xteeDao,
							   List<MessageRecordWorker> messageRecordWorkerList, long pollDepthSeconds) {
		this.influxDao = influxDao;
		this.xteeDao = xteeDao;
		this.messageRecordWorkerList = messageRecordWorkerList;
		this.pollDepthSeconds = pollDepthSeconds;
	}

	public void parse() {
		LocalDateTime startDate = getStartDate();
		List<MessageRecord> messageRecords;
		if (startDate != null) {
			messageRecords = xteeDao.getAllMessageRecordsSinceDate(startDate);
		} else {
			messageRecords = xteeDao.getAllMessageRecords();
		}
		System.out.println(messageRecords.size());
		for (MessageRecordWorker worker : messageRecordWorkerList)
			worker.run(messageRecords);
	}

	private LocalDateTime getStartDate() {
		LocalDateTime startDate;
		if (pollDepthSeconds != 0) {
			startDate = getCurrentTime().minusSeconds(pollDepthSeconds);
		} else {
			startDate = influxDao.getLastSavedMeasurementPointTime();
		}
		return startDate;
	}

	@NotNull
	LocalDateTime getCurrentTime() {
		return LocalDateTime.now(ZoneOffset.UTC);
	}


}
