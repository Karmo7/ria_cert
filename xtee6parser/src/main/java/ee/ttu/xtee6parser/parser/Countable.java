package ee.ttu.xtee6parser.parser;

import ee.ttu.xtee6parser.model.Value;

import java.util.Map;

/**
 * Created by Karmo on 024 24 04 2016.
 */
public interface Countable {
	Map<Value, Long> count();
}
