package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.model.MessageRecord;

import java.util.List;

/**
 * Created by Karmo on 012 12 04 2016.
 */
public interface MessageRecordWorker {
	void run(List<MessageRecord> messageRecordList);
}
