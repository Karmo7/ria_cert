package ee.ttu.xtee6parser.parser.impl;

import ee.ttu.xtee6parser.influx.InfluxDao;
import ee.ttu.xtee6parser.influx.MessageFaultMeasurement;
import ee.ttu.xtee6parser.influx.StatisticsMeasurement;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.MessageStatistics;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.parser.Countable;
import ee.ttu.xtee6parser.statistics.Stat;
import ee.ttu.xtee6parser.utilities.counter.*;
import ee.ttu.xtee6parser.utilities.grouped_value_counter.Count;
import ee.ttu.xtee6parser.utilities.grouped_value_counter.MessageRecordGroupedByElementValueCounter;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Karmo on 012 12 04 2016.
 */
public class MessageRecordStatisticsWorker implements MessageRecordWorker {

	private final static String MEASUREMENT_PREFIX = "stat_";
	private StatisticsMeasurement statisticsMeasurement;
	private MessageFaultMeasurement messageFaultMeasurement;
	private InfluxDao influxDao;

	public MessageRecordStatisticsWorker(InfluxDao influxDao) {
		this.influxDao = influxDao;
		this.statisticsMeasurement = new StatisticsMeasurement();
		this.messageFaultMeasurement = new MessageFaultMeasurement(
				MEASUREMENT_PREFIX + "messagefaults");
	}

	public MessageRecordStatisticsWorker(
			InfluxDao influxDao, StatisticsMeasurement statisticsMeasurement,
			MessageFaultMeasurement messageFaultMeasurement) {
		this.influxDao = influxDao;
		this.statisticsMeasurement = statisticsMeasurement;
		this.messageFaultMeasurement = messageFaultMeasurement;
	}

	@Override
	public void run(List<MessageRecord> messageRecordList) {
		Map<String, Countable> counters = initCounters(messageRecordList);

		counters.forEach((measurementName, counter) -> {
			Map<Value, Long> elementCounts = counter.count();
			Stat stat = new Stat(elementCounts);
			influxDao.saveMeasurementPoints(
					statisticsMeasurement.getMeasurementPoints(measurementName,
							new MessageStatistics(stat.skewness(), stat.kurtosis())));
		});
		influxDao.saveMeasurementPoints(messageFaultMeasurement.getMeasurementPoints(
				new MessageCounter().count(messageRecordList)));
	}

	@NotNull
	private Map<String, Countable> initCounters(List<MessageRecord> messageRecordList) {
		Map<String, Countable> counters = new HashMap<>();
		counters.put(MEASUREMENT_PREFIX + "messageElement",
				new MessageRecordElementCounter(messageRecordList));
		counters.put(MEASUREMENT_PREFIX + "messageElementSsn",
				new MessageRecordElementSsnCounter(messageRecordList));
		counters.put(MEASUREMENT_PREFIX + "messageElementValue",
				new MessageRecordElementValueCounter(messageRecordList));
		counters.put(MEASUREMENT_PREFIX + "memberCode",
				new MessageRecordFieldCounter(messageRecordList, "memberCode"));
		counters.put(MEASUREMENT_PREFIX + "subsystemCode",
				new MessageRecordFieldCounter(messageRecordList, "subsystemCode"));

		Map<String, Count> map = new MessageRecordGroupedByElementValueCounter(messageRecordList).count();
		map.forEach((element, count) -> counters.put(MEASUREMENT_PREFIX + "element_" + element, count));
		return counters;
	}
}
