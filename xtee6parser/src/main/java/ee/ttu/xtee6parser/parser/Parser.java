package ee.ttu.xtee6parser.parser;

/**
 * Created by margus on 9.04.2016.
 */
public interface Parser {
	void parse();
}
