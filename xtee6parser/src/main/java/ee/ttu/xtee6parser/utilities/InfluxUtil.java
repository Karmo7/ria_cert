package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.exception.XteeParserException;
import org.influxdb.dto.QueryResult;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 * Created by margus on 27.03.2016.
 */
public class InfluxUtil {


	public static String getFirstValueFromQueryResult(QueryResult result) {
		List<QueryResult.Result> results = result.getResults();
		//TODO seda peab paremini saama
		if (result != null && results.size() > 0 && results.get(0).getSeries() != null
				&& results.get(0).getSeries().size() > 0 && results.get(0).getSeries().get(0).getValues() != null
				&& results.get(0).getSeries().get(0).getValues().size() > 0) {
			return (String) results.get(0).getSeries().get(0).getValues().get(0).get(0);
		}
		return null;
	}

	public static LocalDateTime getDateFromInfluxTime(String timeValue) {
		try {
			return LocalDateTime.parse(timeValue, DateTimeFormatter.ISO_DATE_TIME);
		} catch (DateTimeParseException e) {
			throw new XteeParserException("Cannot parse influx date, timevalue=" + timeValue, e);
		}


	}
}
