package ee.ttu.xtee6parser.utilities.counter;

import ee.ttu.xtee6parser.model.MessageCount;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.utilities.parser.FaultXmlElementParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Marten on 21.04.2016.
 */
public class MessageCounter {

	private static final Logger logger = LogManager.getLogger(MessageCounter.class);

	public MessageCount count(List<MessageRecord> messageRecords) {
		validate(messageRecords);
		Long allMessages = new Long(messageRecords.size());
		Long faultMessages = countFaultMessages(messageRecords);
		return new MessageCount(allMessages, faultMessages);
	}

	private Long countFaultMessages(List<MessageRecord> messageRecords) {
		Long faultMessages = new Long(0);
		for (MessageRecord messageRecord : messageRecords) {
			faultMessages += parseMessageAndGetFaultMessageCount(messageRecord);
		}
		return faultMessages;
	}

	private Long parseMessageAndGetFaultMessageCount(MessageRecord messageRecord) {
		try {
			return new Long(new FaultXmlElementParser(messageRecord).getAllElementValues().size());
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
			logger.error("Error parsing fault messages! Cause: " + e.getMessage(), e);
		}
		return new Long(0);
	}

	private void validate(List<MessageRecord> messageRecords) {
		if (messageRecords == null) {
			logger.error("validation error!");
			throw new IllegalArgumentException("messageRecords cannot be null!");
		}
	}

}
