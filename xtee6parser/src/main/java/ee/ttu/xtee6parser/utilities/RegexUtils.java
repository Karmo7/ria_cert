package ee.ttu.xtee6parser.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Marten on 14.04.2016.
 */
public class RegexUtils {
	private static final String EE = "EE";

	public static Boolean isSsn(String string) {
		if (StringUtils.isBlank(string)) {
			return Boolean.FALSE;
		}
		return SsnValidator.validate(StringUtils.removeWhiteSpace(string));
	}

	protected static class SsnValidator {
		public static Boolean validate(String trimmedString) {
			if (trimmedString.length() == 11) {
				return validateSsnWithoutPrefic(trimmedString);
			} else if (trimmedString.length() == 13) {
				return validatePrefixAndSsn(trimmedString);
			} else {
				return Boolean.FALSE;
			}
		}

		private static Boolean validatePrefixAndSsn(String ssnWithPrefix) {
			String prefix = ssnWithPrefix.substring(0, 2);
			String ssn = ssnWithPrefix.substring(2);
			return validateSsnPrefix(prefix) && validateSsnWithoutPrefic(ssn);
		}

		private static Boolean validateSsnPrefix(String prefix) {
			return EE.equals(prefix);
		}

		private static Boolean validateSsnWithoutPrefic(String ssn) {
			Pattern pattern = Pattern.compile("[1-6][\\d]{2}[0-1][\\d][0-3][\\d]{5}");
			Matcher matcher = pattern.matcher(ssn);
			return matcher.matches();
		}
	}
}
