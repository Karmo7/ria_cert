package ee.ttu.xtee6parser.utilities.properties;

import ee.ttu.xtee6parser.exception.XteeParserException;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by margus on 24.04.2016.
 */
public class XroadPropertiesFileParser {
	public static String getDBPropertiesFileLocation(String xroadConfFile) {
		Properties properties = getPropertiesFromConfFile(xroadConfFile);
		return (String) properties.get("database-properties");
	}

	public static Properties getDBPropertiesFromConfFile(String confFile) {
		Properties properties = getPropertiesFromConfFile(confFile);
		Properties dbProperties = new Properties();
		dbProperties.setProperty(
				DbPropertiesFields.DB_DRIVER.getValue(),
				properties.getProperty("messagelog.hibernate.connection.driver_class"));
		dbProperties.setProperty(
				DbPropertiesFields.DB_URL.getValue(),
				properties.getProperty("messagelog.hibernate.connection.url"));
		dbProperties.setProperty(DbPropertiesFields.DB_USERNAME.getValue(),
				properties.getProperty("messagelog.hibernate.connection.username"));
		dbProperties.setProperty(DbPropertiesFields.DB_PASSWORD.getValue(),
				properties.getProperty("messagelog.hibernate.connection.password"));
		return dbProperties;
	}

	private static Properties getPropertiesFromConfFile(String confFile) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(confFile));
		} catch (IOException e) {
			throw new XteeParserException("Error loading " + confFile, e);
		}
		return properties;
	}
}
