package ee.ttu.xtee6parser.utilities.parser;

import ee.ttu.xtee6parser.utilities.SoapXmlParserUtil;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by margus on 27.03.2016.
 */
public class XRoadSoapMessageParser {
	private SoapXmlParserUtil soapXmlParserUtil;

	public XRoadSoapMessageParser(String xml) throws IOException, SAXException, ParserConfigurationException {
		soapXmlParserUtil = new SoapXmlParserUtil(xml);
	}

	public String getServiceCode() {
		return soapXmlParserUtil.getFirstElementContentByTagNameNS("http://x-road.eu/xsd/identifiers", "serviceCode");
	}

	public String getServiceVersion() {
		return soapXmlParserUtil.getFirstElementContentByTagNameNS("http://x-road.eu/xsd/identifiers", "serviceVersion");
	}

}
