package ee.ttu.xtee6parser.utilities.parser;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

public class FaultXmlElementParser extends XmlElementParser {

	public FaultXmlElementParser(MessageRecord messageRecord) throws ParserConfigurationException, IOException, SAXException {
		super(messageRecord.getMessage(), false, false);
	}

	@Override
	protected void appendElementNode(String prevPath, List<Value> elementValues, Node childNode) {
		if (childNode.getLocalName().equalsIgnoreCase("Fault")) {
			elementValues.add(new Value("fault"));
		} else {
			walkNodeChildren(childNode, prevPath, elementValues);
		}
	}

	@Override
	protected void appendTextValueNode(String prevPath, List<Value> elementValues, Node childNode) {
	}

}
