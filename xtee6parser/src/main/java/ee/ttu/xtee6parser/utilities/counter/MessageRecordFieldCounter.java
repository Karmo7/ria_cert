package ee.ttu.xtee6parser.utilities.counter;

import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by Karmo on 019 19 04 2016.
 */
public class MessageRecordFieldCounter extends MessageRecordElementCounter {

	private String fieldToCount;

	public MessageRecordFieldCounter(
			@NotNull List<MessageRecord> messageRecords, String fieldToCount) {
		super(messageRecords);
		this.fieldToCount = fieldToCount;
	}

	@Override
	protected void mergeMessages(Map<Value, Long> fieldCountMap, MessageRecord messageRecord)
			throws IOException, SAXException, ParserConfigurationException {
		String value = getCountedFieldValue(messageRecord);
		fieldCountMap.merge(new Value(value), STEP, (prevValue, step) -> prevValue += step);
	}

	private String getCountedFieldValue(MessageRecord messageRecord) {
		try {
			Field field = messageRecord.getClass().getDeclaredField(fieldToCount);
			field.setAccessible(true);
			return field.get(messageRecord).toString();
		} catch (NoSuchFieldException | IllegalAccessException e) {
			throw new XteeParserException("Error: ", e);
		}
	}
}
