package ee.ttu.xtee6parser.utilities.parser;

import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.StringUtils;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Marten on 13.04.2016.
 */
public class XmlElementValueParser extends XmlElementParser {

	public XmlElementValueParser(String xml, boolean ignoreComments, boolean ignoreWhitespace) throws ParserConfigurationException, IOException, SAXException {
		super(xml, ignoreComments, ignoreWhitespace);
	}

	public XmlElementValueParser(String xml, boolean ignoreComments, boolean ignoreWhitespace, String elementPath) throws ParserConfigurationException, IOException, SAXException {
		super(xml, ignoreComments, ignoreWhitespace, elementPath);
	}

	public XmlElementValueParser(String xml) throws ParserConfigurationException, IOException, SAXException {
		super(xml);
	}

	public XmlElementValueParser(String xml, String elementPath) throws ParserConfigurationException, IOException, SAXException {
		super(xml, elementPath);
	}

	@Override
	protected void appendTextValueNode(String prevPath, List<Value> elementValues, Node childNode) {
		String nodeOriginalTextValue = childNode.getNodeValue();

		String nodeStrippedValue = StringUtils.removeWhiteSpace(nodeOriginalTextValue);
		if (!StringUtils.isBlank(nodeStrippedValue)) {
			String newPath = prevPath + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + nodeOriginalTextValue;
			elementValues.add(new Value(newPath, getElement().getElementFilter()));
		}
	}
}
