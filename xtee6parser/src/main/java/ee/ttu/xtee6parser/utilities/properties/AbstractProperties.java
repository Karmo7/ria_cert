package ee.ttu.xtee6parser.utilities.properties;

import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.utilities.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by margus on 9.04.2016.
 */
public abstract class AbstractProperties {


	protected Properties properties;

	public AbstractProperties(String fileName) {
		validateFileName(fileName);
		this.properties = loadPropertiesFile(fileName);
		loadProperties(this.properties);

	}

	public AbstractProperties(Properties properties) {
		validateProperties(properties);
		loadProperties(properties);
		this.properties = properties;
	}

	public AbstractProperties(InputStream propertiesFile) {
		validatePropertiesFile(propertiesFile);
		Properties properties = loadPropertiesFromFile(propertiesFile);
		loadProperties(properties);
		this.properties = properties;
	}

	public Properties getProperties() {
		return properties;
	}

	private Properties loadPropertiesFile(String fileName) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(fileName));
		} catch (IOException e) {
			throw new XteeParserException("Error loading " + fileName, e);
		}
		return properties;
	}

	protected void validateProperty(String property, String propertyValue) {
		if (!tryValidateProperty(propertyValue)) {
			throw new IllegalArgumentException(property + " is required!");
		}
	}

	protected boolean tryValidateProperty(String propertyValue) {
		return !StringUtils.isBlank(propertyValue);
	}

	protected void validateProperties(Properties properties) {
		if (properties == null) {
			throw new IllegalArgumentException("properites file cannot be null!");
		}
	}

	protected void validatePropertiesFile(InputStream propertiesFile) {
		if (propertiesFile == null) {
			throw new IllegalArgumentException("propertiesFile cannot be null!");
		}
	}

	protected void validateFileName(String fileName) {
		if (fileName == null || fileName.isEmpty()) {
			throw new IllegalArgumentException("filename cannot be null or empty!");
		}
	}

	private Properties loadPropertiesFromFile(InputStream propertiesFile) {
		Properties properties = new Properties();
		try {
			properties.load(propertiesFile);
		} catch (IOException e) {
			throw new XteeParserException("Error loading properties file!", e);
		}
		return properties;
	}


	protected abstract void loadProperties(Properties properties);
}
