package ee.ttu.xtee6parser.utilities.grouped_value_counter;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.parser.XmlElementValueParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marten on 14.04.2016.
 */
public class MessageRecordGroupedByElementValueCounter {
	private static final Logger logger = LogManager.getLogger(MessageRecordGroupedByElementValueCounter.class);

	private List<MessageRecord> messageRecords;

	public MessageRecordGroupedByElementValueCounter(@NotNull List<MessageRecord> messageRecords) {
		this.messageRecords = messageRecords;
	}

	public Map<String, Count> count() {
		Map<String, Count> countMap = new HashMap<>();
		for (MessageRecord messageRecord : messageRecords) {
			tryCount(messageRecord, countMap);
		}
		return countMap;
	}

	private void tryCount(MessageRecord messageRecord, Map<String, Count> countMap) {
		try {
			List<Value> elementValues = new XmlElementValueParser(messageRecord.getMessage()).getAllElementValues();
			for (Value elementValue : elementValues) {
				final StringUtils.SeparatedElement element = StringUtils.separateElement(elementValue.getValue().toLowerCase());
				countMap.put(element.prefix, addValueToMap(new Value(element.value), countMap.get(element.prefix)));
			}
		} catch (ParserConfigurationException | IOException | SAXException e) {
			logger.error("Error counting element values", e);
			e.printStackTrace();
		}
	}

	protected
	@NotNull
	Count addValueToMap(Value value, Count prevHolder) {
		if (prevHolder != null) {
			prevHolder.add(value);
			return prevHolder;
		} else {
			return new Count(value);
		}
	}

}
