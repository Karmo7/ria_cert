package ee.ttu.xtee6parser.utilities.parser;

import com.google.common.collect.Lists;
import ee.ttu.xtee6parser.model.Element;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.XmlDocumentFactory;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marten on 13.04.2016.
 */
public class XmlElementParser {

	private Document document;
	private Element element;
	private String elementPath;

	public XmlElementParser(String xml) throws ParserConfigurationException, IOException, SAXException {
		this(xml, null);
	}

	public XmlElementParser(String xml, String elementPath) throws ParserConfigurationException, IOException, SAXException {
		document = XmlDocumentFactory.create(xml);
		this.elementPath = elementPath;
		this.element = new Element(elementPath, document, xml);
	}

	public XmlElementParser(String xml, boolean ignoreComments, boolean ignoreWhitespace) throws ParserConfigurationException, IOException, SAXException {
		document = XmlDocumentFactory.create(xml, ignoreComments, ignoreWhitespace);
		this.element = new Element();
	}

	public XmlElementParser(String xml, boolean ignoreComments, boolean ignoreWhitespace, String elementPath) throws ParserConfigurationException, IOException, SAXException {
		document = XmlDocumentFactory.create(xml, ignoreComments, ignoreWhitespace);
		this.elementPath = elementPath;
		this.element = new Element(elementPath, document, xml);
	}

	public List<Value> getAllElementValues() {
		return getAllElementValues(null);
	}

	public List<Value> getAllElementValues(@Nullable String namespace) {
		if (namespace == null) {
			namespace = "http://schemas.xmlsoap.org/soap/envelope/";
		}

		for (String bodyTag : getBodyTags()) {
			NodeList nodeList = document.getElementsByTagNameNS(namespace, bodyTag);

			if (isBodyTagNotFound(nodeList)) {
				continue;
			} else if (isBodyTagMatchFound(nodeList)) {
				Node bodyNode = nodeList.item(0);
				return walkNodeChildren(bodyNode, "Body", new ArrayList<>());
			} else {
				throw new IllegalArgumentException("Unhandled number of nodes in the soap message: " + nodeList.getLength());
			}
		}

		throw new RuntimeException("Can't find body element of XML document");
	}

	protected List<Value> walkNodeChildren(Node parentNode, String prevPath, List<Value> elementValues) {
		int childNodesCount = parentNode.getChildNodes().getLength();

		for (int i = 0; i < childNodesCount; i++) {
			Node childNode = parentNode.getChildNodes().item(i);
			appendChildNode(prevPath, elementValues, childNode);
		}

		return elementValues;
	}

	protected void appendChildNode(String prevPath, List<Value> elementValues, Node childNode) {
		if (childNode.getNodeType() == Node.ELEMENT_NODE) {
			appendElementNode(prevPath, elementValues, childNode);
		} else if (childNode.getNodeType() == Node.TEXT_NODE) {
			appendTextValueNode(prevPath, elementValues, childNode);
		}
	}

	protected void appendElementNode(String prevPath, List<Value> elementValues, Node childNode) {
		String newPath = prevPath + StringUtils.ELEMENT_VALUE_SEPATOR + childNode.getLocalName();
		walkNodeChildren(childNode, newPath, elementValues);
	}

	protected void appendTextValueNode(String prevPath, List<Value> elementValues, Node childNode) {
		String nodeOriginalTextValue = childNode.getNodeValue();

		String nodeStrippedValue = StringUtils.removeWhiteSpace(nodeOriginalTextValue);
		if (!StringUtils.isBlank(nodeStrippedValue)) {
			elementValues.add(new Value(prevPath, element.getElementFilter()));
		}
	}

	private boolean isBodyTagMatchFound(NodeList nodeList) {
		return nodeList.getLength() == 1;
	}

	private boolean isBodyTagNotFound(NodeList nodeList) {
		return nodeList.getLength() == 0;
	}

	private List<String> getBodyTags() {
		return Lists.newArrayList("Body", "body", "BODY");
	}

	protected Document getDocument() {
		return document;
	}

	protected Element getElement() {
		return element;
	}

}
