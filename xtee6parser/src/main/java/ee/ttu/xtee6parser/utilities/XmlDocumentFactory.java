package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.influx.impl.InfluxDaoImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.Nullable;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Created by Marten on 21.04.2016.
 */
public class XmlDocumentFactory {

	private static final Logger logger = LogManager.getLogger(InfluxDaoImpl.class);

	private static final Boolean IGNORE_COMMENTS = Boolean.TRUE;
	private static final Boolean IGNORE_WHITESPACE = Boolean.TRUE;

	private XmlDocumentFactory() {
	}

	public static Document create(String xml) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		setBuilderParams(IGNORE_COMMENTS, IGNORE_WHITESPACE, dbf);
		return tryBuildDocument(xml, dbf);
	}

	public static Document create(String xml, boolean ignoreComments, boolean ignoreWhitespace) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		setBuilderParams(ignoreComments, ignoreWhitespace, dbf);
		return tryBuildDocument(xml, dbf);
	}

	@Nullable
	private static Document tryBuildDocument(String xml, DocumentBuilderFactory dbf) {
		try {
			return buildDocument(xml, dbf);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			return handleError(e);
		}
	}

	private static Document handleError(Exception e) {
		e.printStackTrace();
		logger.error("cannot handle error", e);
		return null;
	}

	private static Document buildDocument(String xml, DocumentBuilderFactory dbf) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
	}

	private static void setBuilderParams(boolean ignoreComments, boolean ignoreWhitespace, DocumentBuilderFactory dbf) {
		dbf.setIgnoringComments(ignoreComments);
		dbf.setIgnoringElementContentWhitespace(ignoreWhitespace);
		dbf.setNamespaceAware(true);
	}
}
