package ee.ttu.xtee6parser.utilities;

import ee.ttu.xtee6parser.exception.XteeParserException;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

/**
 * Created by margus on 23.04.2016.
 */
public class HostnameResolver {

	public String getHostname() {
		try {
			return IOUtils.toString(Runtime.getRuntime().exec("hostname").getInputStream(), "UTF-8").trim();
		} catch (IOException e) {
			throw new XteeParserException("Could not resolve hostname", e);
		}

	}
}
