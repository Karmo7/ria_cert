package ee.ttu.xtee6parser.utilities.grouped_value_counter;

import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.parser.Countable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Count implements Countable {

	private Map<Value, Long> valueCountMap;

	private static final Long STEP = new Long(1);

	public Count() {
		valueCountMap = new HashMap<>();
	}

	public Count(Value value) {
		this();
		validate(value);
		addToMap(value);
	}

	public void add(Value value) {
		validate(value);
		addToMap(value);
	}

	public Map<Value, Long> count() {
		return valueCountMap;
	}

	public double[] getRaw() {
		int mapSize = valueCountMap.keySet().size();
		double[] countArr = new double[mapSize];

		Iterator<Map.Entry<Value, Long>> iterator = valueCountMap.entrySet().iterator();
		int index = 0;
		while (iterator.hasNext()) {
			addCountToArray(countArr, iterator, index);
			index++;
		}
		return countArr;
	}

	public ValueCount[] getConverted() {
		int mapSize = valueCountMap.keySet().size();
		ValueCount[] valueCounts = new ValueCount[mapSize];

		Iterator<Value> iterator = valueCountMap.keySet().iterator();
		int index = 0;
		while (iterator.hasNext()) {
			addCountToArray(valueCounts, iterator, index);
			index++;
		}
		return valueCounts;
	}

	private void addToMap(Value value) {
		valueCountMap.merge(value, STEP, (prevValue, step) -> prevValue += step);
	}

	private void validate(Value value) {
		if (value == null) {
			throw new IllegalArgumentException("value cannot be null!");
		}
	}

	private void addCountToArray(double[] countArr, Iterator<Map.Entry<Value, Long>> iterator, int index) {
		Long count = iterator.next().getValue();
		countArr[index] = count.doubleValue();
	}

	private void addCountToArray(ValueCount[] valueCounts, Iterator<Value> iterator, int index) {
		Value value = iterator.next();
		Long count = valueCountMap.get(value);
		valueCounts[index] = new ValueCount(value, count.doubleValue());
	}

	public Map<Value, Long> getValueCountMap() {
		return valueCountMap;
	}
}
