package ee.ttu.xtee6parser.utilities;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Marten on 12.03.2016.
 */
public class StringUtils {

	public static final String ELEMENT_VALUE_SEPATOR = "_";
	public static final String ELEMENT_VALUE_PREFIX_SEPATOR = "%:";

	public static String getDefaultIfNullOrEmpty(String value, String defaultValue) {
		if (value == null || value.isEmpty()) {
			return defaultValue;
		} else {
			return value;
		}
	}

	public static Boolean isBlank(String string) {
		if (string == null || string.trim().equals("")) {
			return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	public static String getMD5HashOfString(String string) {
		MessageDigest messageDigest;
		if (string != null) {
			try {
				messageDigest = MessageDigest.getInstance("MD5");
				messageDigest.update(string.getBytes());
				String hash = new BigInteger(1, messageDigest.digest()).toString(16);

				return hash == null ? "" : hash;
			} catch (NoSuchAlgorithmException e) {
				return null;
			}
		} else {
			return null;
		}
	}

	public static Boolean notNull(String string) {
		return string != null;
	}

	public static String removeWhiteSpace(String string) {
		if (string == null) {
			return null;
		}
		return string.replace("\\n", "").trim();
	}

	public static SeparatedElement separateElement(String element) {
		if (element == null) {
			return null;
		}
		return getPrefix(element);
	}

	public static class SeparatedElement {
		public String prefix;
		public String value;

		public SeparatedElement(String prefix, String value) {
			this.prefix = prefix;
			this.value = value;
		}
	}

	public static String replaceSeparator(String separator, String string) {
		if (separator == null || string == null) {
			return null;
		}
		return string.replaceAll(separator, StringUtils.ELEMENT_VALUE_SEPATOR);
	}

	private static SeparatedElement getPrefix(String element) {
		String[] splittedElement = element.split(ELEMENT_VALUE_PREFIX_SEPATOR);
		validateSplittedArray(element, splittedElement);
		return new SeparatedElement(splittedElement[0], splittedElement[1]);
	}

	private static void validateSplittedArray(String element, String[] splittedElement) {
		if (splittedElement.length != 2) {
			throw new IllegalArgumentException("Error handling input string: " + element + " .");
		}
	}
}
