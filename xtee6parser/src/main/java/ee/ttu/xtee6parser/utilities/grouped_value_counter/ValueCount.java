package ee.ttu.xtee6parser.utilities.grouped_value_counter;

import ee.ttu.xtee6parser.model.Value;

public class ValueCount {
	public Value value;
	public double count;

	public ValueCount(Value value, double count) {
		this.value = value;
		this.count = count;
	}
}
