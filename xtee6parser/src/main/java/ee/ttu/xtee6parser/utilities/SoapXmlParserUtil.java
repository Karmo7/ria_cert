package ee.ttu.xtee6parser.utilities;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by margus on 19.03.2016.
 */
public class SoapXmlParserUtil {

	private Document document;

	public SoapXmlParserUtil(String xml) throws ParserConfigurationException, IOException, SAXException {
		validateInputXml(xml);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		document = db.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
	}

	public SoapXmlParserUtil(String xml, boolean ignoreComments, boolean ignoreWhitespace) throws ParserConfigurationException, IOException, SAXException {
		validateInputXml(xml);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setIgnoringComments(ignoreComments);
		dbf.setIgnoringElementContentWhitespace(ignoreWhitespace);
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		document = db.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8)));
	}

	public String getFirstElementContentByTagNameNS(String namespace, String tagName) {
		String value = null;
		NodeList nodeList = document.getElementsByTagNameNS(namespace, tagName);
		if (nodeList.getLength() > 0 && !nodeList.item(0).getTextContent().isEmpty()) {
			value = nodeList.item(0).getTextContent();
		}
		return value;
	}


	public String getNamespaceFirstNodeName(String namespace) {
		String nodeName = null;
		NodeList nodeList = document.getElementsByTagNameNS(namespace, "*");
		if (nodeList.getLength() > 0) {
			nodeName = nodeList.item(0).getNodeName();
			String prefix = nodeList.item(0).getPrefix();
			nodeName = nodeName.replace(prefix + ":", "");
		}
		return nodeName;
	}


	public List<String> getAllNamespaces() {
		List<String> namespaces = new ArrayList<>();
		XPathFactory xPathFactory = XPathFactory.newInstance();
		XPath xPath = xPathFactory.newXPath();
		XPathExpression xPathExpression = null;
		try {
			xPathExpression = xPath.compile("//namespace::*");
			NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				namespaces.add(nodeList.item(i).getNodeValue());
			}
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return namespaces;
	}

	private void validateInputXml(String xml) {
		if (xml == null || xml.isEmpty()) {
			throw new IllegalArgumentException("Input xml is empty or null");
		}
	}


}
