package ee.ttu.xtee6parser.utilities.properties;

/**
 * Created by margus on 24.04.2016.
 */
public enum DbPropertiesFields {
	DB_DRIVER("driver"),
	DB_URL("url"),
	DB_USERNAME("user"),
	DB_PASSWORD("password");
	private final String value;

	DbPropertiesFields(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
