package ee.ttu.xtee6parser.utilities.counter;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.parser.Countable;
import ee.ttu.xtee6parser.utilities.parser.XmlElementParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Marten on 2.04.2016.
 */
public class MessageRecordElementCounter implements Countable {

	private static final Logger logger = LogManager.getLogger(MessageRecordElementCounter.class);

	private List<MessageRecord> messageRecords;
	private String elementPath;

	protected static final Long STEP = new Long(1);

	public MessageRecordElementCounter(@NotNull List<MessageRecord> messageRecords) {
		validateMessageRecords(messageRecords);
		this.messageRecords = messageRecords;
	}

	public MessageRecordElementCounter(@NotNull List<MessageRecord> messageRecords, String elementPath) {
		validateMessageRecords(messageRecords);
		this.messageRecords = messageRecords;
		this.elementPath = elementPath;
	}

	private void validateMessageRecords(@NotNull List<MessageRecord> messageRecords) {
		if (messageRecords == null) {
			throw new IllegalArgumentException("messageRecords cannot be null!");
		}
	}

	public Map<Value, Long> count() {
		Map<Value, Long> elementCountMap = new HashMap<>();
		for (MessageRecord messageRecord : messageRecords) {
			tryMergeMessages(elementCountMap, messageRecord);
		}

		return elementCountMap;
	}

	private void tryMergeMessages(Map<Value, Long> elementValueCountMap, MessageRecord messageRecord) {
		try {
			mergeMessages(elementValueCountMap, messageRecord);
		} catch (Exception e) {
			logger.error("Error parsing xml body, silently ignore message", e);
		}
	}

	protected void mergeMessages(Map<Value, Long> elementValueCountMap, MessageRecord messageRecord) throws IOException, SAXException, ParserConfigurationException {
		List<Value> elementValues = new XmlElementParser(messageRecord.getMessage(), elementPath).getAllElementValues();
		for (Value elementValue : elementValues) {
			elementValueCountMap.merge(elementValue, STEP, (prevValue, step) -> prevValue += step);
		}
	}

	protected String getElementPath() {
		return elementPath;
	}
}
