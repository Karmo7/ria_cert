package ee.ttu.xtee6parser.utilities.parser;

import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.RegexUtils;
import ee.ttu.xtee6parser.utilities.StringUtils;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * Created by Marten on 14.04.2016.
 */
public class XmlElementSsnParser extends XmlElementParser {

	public XmlElementSsnParser(String xml, boolean ignoreComments, boolean ignoreWhitespace) throws ParserConfigurationException, IOException, SAXException {
		super(xml, ignoreComments, ignoreWhitespace);
	}

	public XmlElementSsnParser(String xml, boolean ignoreComments, boolean ignoreWhitespace, String elementPath) throws ParserConfigurationException, IOException, SAXException {
		super(xml, ignoreComments, ignoreWhitespace, elementPath);
	}

	public XmlElementSsnParser(String xml) throws IOException, SAXException, ParserConfigurationException {
		super(xml);
	}

	public XmlElementSsnParser(String xml, String elementPath) throws IOException, SAXException, ParserConfigurationException {
		super(xml, elementPath);
	}

	@Override
	protected void appendElementNode(String prevPath, List<Value> elementValues, Node childNode) {
		walkNodeChildren(childNode, prevPath, elementValues);
	}

	@Override
	protected void appendTextValueNode(String prevPath, List<Value> elementValues, Node childNode) {
		String nodeOriginalTextValue = childNode.getNodeValue();

		String nodeStrippedValue = StringUtils.removeWhiteSpace(nodeOriginalTextValue);
		if (!StringUtils.isBlank(nodeStrippedValue) && RegexUtils.isSsn(nodeStrippedValue)) {
			elementValues.add(new Value(prevPath, getElement().getElementFilter()));
		}
	}
}
