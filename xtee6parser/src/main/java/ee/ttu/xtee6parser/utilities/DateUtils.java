package ee.ttu.xtee6parser.utilities;

import java.util.Date;

/**
 * Created by Marten on 7.05.2016.
 */
public class DateUtils {

    public static Long time() {
        return new Date().getTime();
    }

}
