package ee.ttu.xtee6parser.utilities.xpath;

import ee.ttu.xtee6parser.model.MessageRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.StringReader;
import java.util.List;

/**
 * Created by Marten on 1.05.2016.
 */
public class XPathUtil {
	private static final Logger logger = LogManager.getLogger(XPathUtil.class);

	public static final String PATH_SERVICE_MEMBERCODE = "//xro:service/iden:memberCode/text()";
	public static final String PATH_CLIENT_MEMBERCODE = "//xro:client/iden:memberCode/text()";

	public static NamespaceContext getNamespaceContext(String xml) {
		NamespaceContext nsContext = null;
		try {
			XMLEventReader evtReader = XMLInputFactory.newInstance()
					.createXMLEventReader(new StringReader(xml));

			while (evtReader.hasNext()) {
				XMLEvent event = evtReader.nextEvent();
				if (event.isStartElement()) {
					nsContext = ((StartElement) event)
							.getNamespaceContext();
					break;
				}
			}
		} catch (XMLStreamException e) {
			logger.error("finding namespace failed silently!", e);
		}

		return nsContext;
	}

	public static NamespaceContext getNamespaceContext(List<MessageRecord> messageRecords) throws XMLStreamException {
		if (messageRecords == null || messageRecords.isEmpty()) {
			return null;
		}
		return getNamespaceContext(messageRecords.get(0).getMessage());
	}

}
