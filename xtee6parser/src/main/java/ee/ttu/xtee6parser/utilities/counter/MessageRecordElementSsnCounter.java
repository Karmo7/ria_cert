package ee.ttu.xtee6parser.utilities.counter;

import ee.ttu.xtee6parser.model.MessageRecord;
import ee.ttu.xtee6parser.model.Value;
import ee.ttu.xtee6parser.utilities.parser.XmlElementSsnParser;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by Marten on 14.04.2016.
 */
public class MessageRecordElementSsnCounter extends MessageRecordElementCounter {

	public MessageRecordElementSsnCounter(@NotNull List<MessageRecord> messageRecords) {
		super(messageRecords);
	}

	public MessageRecordElementSsnCounter(@NotNull List<MessageRecord> messageRecords, String elementPath) {
		super(messageRecords, elementPath);
	}

	protected void mergeMessages(Map<Value, Long> elementValueCountMap, MessageRecord messageRecord) throws IOException, SAXException, ParserConfigurationException {
		List<Value> elementValues = new XmlElementSsnParser(messageRecord.getMessage(), getElementPath()).getAllElementValues();
		for (Value elementValue : elementValues) {
			elementValueCountMap.merge(elementValue, STEP, (prevValue, step) -> prevValue += step);
		}
	}

}
