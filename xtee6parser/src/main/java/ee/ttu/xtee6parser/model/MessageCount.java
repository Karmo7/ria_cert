package ee.ttu.xtee6parser.model;

/**
 * Created by Marten on 21.04.2016.
 */
public class MessageCount {

	private Long allMessagesCount;
	private Long faultyMessagesCount;

	public MessageCount(Long allMessagesCount, Long faultyMessagesCount) {
		this.allMessagesCount = allMessagesCount;
		this.faultyMessagesCount = faultyMessagesCount;
		validate();
	}

	private void validate() {
		if (faultyMessagesCount > allMessagesCount) {
			throw new IllegalStateException("faulty messages exceed all messages!");
		}
	}

	public MessageCount(Long allMessagesCount) {
		this.allMessagesCount = allMessagesCount;
		this.faultyMessagesCount = new Long(0);
	}

	public void incrementFaulty() {
		faultyMessagesCount++;
		validate();
	}

	public Long getAllMessagesCount() {
		return allMessagesCount;
	}

	public Long getFaultyMessagesCount() {
		return faultyMessagesCount;
	}

	public Long getSuccesfulMessagesCount() {
		return allMessagesCount - faultyMessagesCount;
	}
}
