package ee.ttu.xtee6parser.model;

/**
 * Created by Karmo on 006 6 04 2016.
 */
public class MessageStatistics {

	private double bodySkewness, bodyKurtosis;

	public MessageStatistics(double bodySkewness, double bodyKurtosis) {
		this.bodySkewness = bodySkewness;
		this.bodyKurtosis = bodyKurtosis;
	}

	public double getBodySkewness() {
		return Double.isNaN(bodySkewness) ? 0 : bodySkewness;
	}

	public double getBodyKurtosis() {
		return Double.isNaN(bodyKurtosis) ? 0 : bodyKurtosis;
	}

	public boolean isValid() {
		return !Double.isNaN(bodySkewness) && !Double.isNaN(bodyKurtosis);
	}
}
