package ee.ttu.xtee6parser.model;

import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.xpath.XPathUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Created by Marten on 1.05.2016.
 */
public class Element {

	private static final Logger logger = LogManager.getLogger(Element.class);

	private Document document;
	private String elementPath;
	private String elementValue;

	public Element(String elementPath, Document document, String xml) {
		this.elementPath = elementPath;
		this.document = document;
		if (!StringUtils.isBlank(elementPath)) {
			compile(XPathUtil.getNamespaceContext(xml));
		}
	}

	public Element() {
	}

	private void compile(NamespaceContext nsContext) {
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			xPath.setNamespaceContext(nsContext);
			elementValue = (String) xPath.compile(elementPath).evaluate(document, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			throw new IllegalArgumentException("elementPath doesn't compile as XPath!", e);
		}
	}

	public String getElementFilter() {
		if (StringUtils.isBlank(elementPath) || StringUtils.isBlank(elementValue)) {
			return null;
		}
		return elementPath + StringUtils.ELEMENT_VALUE_PREFIX_SEPATOR + elementValue;
	}

}
