package ee.ttu.xtee6parser.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Marten on 1.05.2016.
 */
public class Value {
	private String value;
	private String elementFilter;

	public Value(@NotNull String value) {
		this.value = validateAndLower(value);
	}

	public Value(@NotNull String value, String elementFilter) {
		this.value = validateAndLower(value);
		this.elementFilter = lower(elementFilter);
	}

	public static Value of(String value) {
		return new Value(value);
	}

	public static Value of(String value, String path, String separator, String element) {
		return new Value(value, path + separator + element);
	}

	public String getValue() {
		return value;
	}

	public String getElementFilter() {
		return elementFilter;
	}

	private String validateAndLower(String value) {
		if (value == null) {
			throw new IllegalArgumentException("value cannot be null!");
		}
		return lower(value);
	}

	private String lower(String value) {
		if (value == null) {
			return null;
		}
		return value.toLowerCase();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Value value1 = (Value) o;

		if (!value.equals(value1.value)) return false;
		return elementFilter != null ? elementFilter.equals(value1.elementFilter) : value1.elementFilter == null;

	}

	@Override
	public int hashCode() {
		int result = value.hashCode();
		result = 31 * result + (elementFilter != null ? elementFilter.hashCode() : 0);
		return result;
	}
}
