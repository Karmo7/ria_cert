package ee.ttu.xtee6parser.jdbc;

import ee.ttu.xtee6parser.utilities.StringUtils;
import ee.ttu.xtee6parser.utilities.properties.AbstractProperties;
import ee.ttu.xtee6parser.utilities.properties.DbPropertiesFields;

import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Marten on 12.03.2016.
 */
public class DbProperties extends AbstractProperties {

	public static final String PROPERTY_FILE_NAME = "DBConnection.txt";

	private static final String DB_DRIVER = DbPropertiesFields.DB_DRIVER.getValue();
	private static final String DB_URL = DbPropertiesFields.DB_URL.getValue();
	private static final String DB_USERNAME = DbPropertiesFields.DB_USERNAME.getValue();
	private static final String DB_PASSWORD = DbPropertiesFields.DB_PASSWORD.getValue();

	private static final String DEFAULT_DB_DRIVER = "postgresql";
	private static final String DEFAULT_DB_URL = "localhost:5432/messagelog";

	private String driver;
	private String url;
	private String username;
	private String password;

	public DbProperties() {
		this(PROPERTY_FILE_NAME);
	}

	public DbProperties(String fileName) {
		super(fileName);
	}

	public DbProperties(Properties properties) {
		super(properties);
	}

	public DbProperties(InputStream propertiesFile) {
		super(propertiesFile);
	}

	@Override
	protected void loadProperties(Properties properties) {
		driver = (String) properties.get(DB_DRIVER);
		if (StringUtils.isBlank(driver)) {
			driver = DEFAULT_DB_DRIVER;
		}

		url = (String) properties.get(DB_URL);
		if (StringUtils.isBlank(url)) {
			url = DEFAULT_DB_URL;
		}

		username = (String) properties.get(DB_USERNAME);
		validateProperty(DB_USERNAME, username);

		password = (String) properties.get(DB_PASSWORD);
		validateProperty(DB_PASSWORD, password);
	}


	public String getFullUrl() {
		return getUrl();
	}

	public String getDriver() {
		return driver;
	}

	public String getUrl() {
		return url;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}


}
