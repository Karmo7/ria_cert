package ee.ttu.xtee6parser.jdbc;

import ee.ttu.xtee6parser.model.MessageRecord;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Marten on 12.03.2016.
 */
public interface XteeDao extends JdbcDao {

	List<MessageRecord> getAllMessageRecordsSinceDate(LocalDateTime date);

	List<MessageRecord> getAllMessageRecords();
}
