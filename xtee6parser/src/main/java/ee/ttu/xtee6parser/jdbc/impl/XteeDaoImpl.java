package ee.ttu.xtee6parser.jdbc.impl;

import com.google.common.collect.Lists;
import ee.ttu.xtee6parser.exception.XteeParserException;
import ee.ttu.xtee6parser.jdbc.DbProperties;
import ee.ttu.xtee6parser.jdbc.XteeDao;
import ee.ttu.xtee6parser.model.MessageRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Properties;

/**
 * Created by Marten on 12.03.2016.
 */
public class XteeDaoImpl implements XteeDao {

	private static final Logger logger = LogManager.getLogger(XteeDaoImpl.class);

	private DbProperties dbProperties;

	public XteeDaoImpl(DbProperties dbProperties) {
		this.dbProperties = dbProperties;
	}

	public XteeDaoImpl(Properties properties) {
		this.dbProperties = new DbProperties(properties);
	}

	public XteeDaoImpl(InputStream propertiesFile) {
		this.dbProperties = new DbProperties(propertiesFile);
	}


	public Connection getConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			return DriverManager.getConnection(dbProperties.getFullUrl(), dbProperties.getProperties());
		} catch (SQLException | ClassNotFoundException e) {
			throw new XteeParserException("Could not create connection!", e);
		}
	}

	@Override
	public List<MessageRecord> getAllMessageRecordsSinceDate(LocalDateTime date) {
		String selectQuery = "SELECT * FROM logrecord WHERE discriminator = 'm' AND time> ?";
		try (Connection connection = getConnection();
			 PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
			selectStatement.setLong(1, date.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli());
			try (ResultSet rs = selectStatement.executeQuery()) {
				return new MessageRecordMapper(rs).map();
			}
		} catch (SQLException e) {
			throw new XteeParserException("Could not get all messages from database since date", e);
		}

	}

	@Override
	public List<MessageRecord> getAllMessageRecords() {
		String selectQuery = "SELECT * FROM logrecord WHERE discriminator = 'm' ";
		try (Connection connection = getConnection();
			 PreparedStatement selectStatement = connection.prepareStatement(selectQuery)) {
			try (ResultSet rs = selectStatement.executeQuery()) {
				return new MessageRecordMapper(rs).map();
			}
		} catch (SQLException e) {
			throw new XteeParserException("Could not get all messages from database", e);
		}

	}

	private class MessageRecordMapper {
		private ResultSet rs;

		public MessageRecordMapper(ResultSet rs) {
			this.rs = rs;
		}

		public List<MessageRecord> map() throws SQLException {
			List<MessageRecord> messageRecords = Lists.newArrayList();
			while (rs.next()) {
				messageRecords.add(new MessageRecord(
						rs.getLong("time"),
						rs.getString("queryid"),
						rs.getString("message"),
						rs.getString("signature"),
						rs.getString("hashchain"),
						rs.getString("hashchainresult"),
						rs.getString("signaturehash"),
						rs.getString("timestamp"),
						rs.getString("timestamphashchain"),
						rs.getBoolean("response"),
						rs.getString("memberclass"),
						rs.getString("membercode"),
						rs.getString("subsystemcode")));
			}
			return messageRecords;
		}
	}
}
