package ee.ttu.xtee6parser.jdbc;

import java.sql.Connection;

/**
 * Created by Marten on 12.03.2016.
 */
public interface JdbcDao {

	Connection getConnection();
}
