# Getting started
1. Download Vagrant from https://www.vagrantup.com/downloads.html
2. (Optional, mostly for Windows users) Add Vagrant to your PATH
3. Execute "**vagrant up**"
4. Verify it is working by pointing host machine browser to https://127.0.0.1:4000 (user: "xtee", password "xtee")
5. Virtual machine can be accessed by typing "**vagrant ssh**"
