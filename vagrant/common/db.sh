#!/bin/bash

IP=$(ifconfig eth0 2>/dev/null|grep 'inet addr'|cut -f2 -d':'|cut -f1 -d' ')
apt-get -y install sqlite > /dev/null 2>&1
rm /var/lib/grafana/grafana.db
cp /vagrant/grafana.db /var/lib/grafana/grafana.db
chown grafana:grafana /var/lib/grafana/grafana.db
sqlite3 /var/lib/grafana/grafana.db "DELETE FROM \"data_source\";"
sqlite3 /var/lib/grafana/grafana.db "INSERT INTO \"data_source\" VALUES(1,1,0,\"influxdb\",\"telegraf\",\"proxy\",\"http://$IP:8086\",\"xroad\",\"xroad\",\"telegraf\",0,\"\",\"\",1,\"{}\",\"2016-03-07 18:21:44\",\"2016-03-07 18:50:56\",0);"
sqlite3 /var/lib/grafana/grafana.db "INSERT INTO \"data_source\" VALUES(2,1,0,\"influxdb\",\"Influx\",\"proxy\",\"http://$IP:8086\",\"xroad\",\"xroad\",\"xroad\",0,\"\",\"\",0,\"{}\",\"2016-05-10 13:40:18\",\"2016-05-10 13:40:38\",0);"

service grafana-server restart
