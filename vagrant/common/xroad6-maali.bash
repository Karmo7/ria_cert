# see http://x-road.eu/docs/x-road_v6_security_server_installation_guide.pdf
USER="xtee"
PASSWORD="xtee"
NAME=$(hostname -f)
IP=$(ifconfig eth1 2>/dev/null|grep 'inet addr'|cut -f2 -d':'|cut -f1 -d' ')
#NAME="$(hostname).cloudapp.net"
#IP=$(host $NAME|cut -f4 -d" ")

echo "$PASSWORD
$PASSWORD
"|adduser $USER


apt-get install language-pack-en
locale-gen en_US.UTF-8
update-locale en_US.UTF-8
echo "LC_ALL=en_US.UTF-8" >> /etc/environment
echo "deb http://x-road.eu/packages trusty main" >> /etc/apt/sources.list.d/xroad.list
echo "deb http://ppa.launchpad.net/nginx/stable/ubuntu trusty main" >> /etc/apt/sources.list.d/xroad.list
echo "deb http://ppa.launchpad.net/openjdk-r/ppa/ubuntu trusty main" >> /etc/apt/sources.list.d/xroad.list
curl -s http://x-road.eu/packages/xroad_repo.gpg| sudo apt-key add -
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 00A6F0A3C300EE8C
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB9B1D8886F44E2A
apt-get update

echo "setting up security server ip: $IP hostname: $NAME username: $USER"
echo "xroad-proxy xroad-common/username string $USER" | debconf-set-selections
echo "xroad-proxy xroad-common/admin-subject string /CN=$NAME" | debconf-set-selections
echo "xroad-proxy xroad-common/admin-altsubject string IP:$IP,DNS:$NAME" | debconf-set-selections
echo "xroad-proxy xroad-common/service-subject string /CN=$NAME" | debconf-set-selections
echo "xroad-proxy xroad-common/service-altsubject string IP:$IP,DNS:$NAME" | debconf-set-selections
apt-get -y -qq install xroad-securityserver

curl -s http://x-road.eu/packages/ee-dev_public_anchor.xml > /etc/xroad/configuration-anchor.xml

#apt-get install xroad-addon-hwtokens
# do something with /etc/xroad/devices.ini
#service xroad-signer restart

initctl list | grep "^xroad-" | cut -f1 -d" "| while read p; do service $p restart;sleep 1; done
