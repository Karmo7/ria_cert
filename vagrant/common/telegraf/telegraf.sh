#!/bin/bash
wget "http://get.influxdb.org/telegraf/telegraf_0.10.4.1-1_amd64.deb"
sudo dpkg -i telegraf_0.10.4.1-1_amd64.deb
rm telegraf_0.10.4.1-1_amd64.deb

cp /vagrant_common/telegraf/telegraf.conf /etc/telegraf/telegraf.conf

service telegraf start