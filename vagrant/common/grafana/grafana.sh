#!/bin/bash
curl https://packagecloud.io/gpg.key | sudo apt-key add -
echo "deb https://packagecloud.io/grafana/stable/debian/ wheezy main" | sudo tee -a /etc/apt/sources.list

sudo apt-get update && sudo apt-get install grafana=2.6.0
sudo service grafana-server start

sudo update-rc.d grafana-server defaults 95 10
