# Getting started
1. Download Vagrant from https://www.vagrantup.com/downloads.html
2. (Optional, mostly for Windows users) Add Vagrant to your PATH
3. Execute `vagrant up`
4. Point your browser to http://192.168.33.13:3000/dashboard/db/grafana-dashboard (username: `admin`, password: `admin`)
5. Virtual machine can be accessed by typing `vagrant ssh`

#Summary
This demo application installs Grafana, InfluxDB, Telegraf, our X-Road monitor and test data generator (RecordLogCreator). RecordLogCreator inserts random data to PostgreSQL database, X-Road monitor will parse it and save results to InfluxDB where grafana will read and display it.

#Configuration
By default in this demo the monitor program will poll X-Road security server database every 10 seconds. This can be configured in `demo/ParserProperties.txt`. 10 seconds is fine for our test data generator, but for a real database you may want to increase this to minutes (for statistics generation there should be few dozen new rows every poll).

If you wish to test this demo with a real world database, you may edit `demo/DBConnection.txt`.

#Notes
Updates can be installed by running `/vagrant/update_applications.sh`. This will do `git pull` on our X-Road monitor and compile a new version.
