#!/bin/bash

cd /home/vagrant/ria_cert/RecordlogCreator/
nohup java -jar target/RecordlogCreator-1.0-SNAPSHOT-jar-with-dependencies.jar -u vagrant -p vagrant -f random &

cd /home/vagrant
cp -a /vagrant/demo/ demo
cd demo
nohup java -jar /home/vagrant/ria_cert/xtee6parser/target/xtee6parser-1.0-SNAPSHOT-jar-with-dependencies.jar &

ps -e | grep java
