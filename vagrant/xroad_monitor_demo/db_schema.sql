--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE messagelog;
ALTER ROLE messagelog WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md59eff18435ac2ca542458f9ce8a84fad7';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION;
CREATE ROLE serverconf;
ALTER ROLE serverconf WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md518af3e52437612995782277e24a07270';
CREATE ROLE vagrant;
ALTER ROLE vagrant WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md5ce5f2d27bc6276a03b0328878c1dc0e2';






--
-- Database creation
--

CREATE DATABASE messagelog WITH TEMPLATE = template0 OWNER = messagelog;
CREATE DATABASE serverconf WITH TEMPLATE = template0 OWNER = serverconf;
REVOKE ALL ON DATABASE template1 FROM PUBLIC;
REVOKE ALL ON DATABASE template1 FROM postgres;
GRANT ALL ON DATABASE template1 TO postgres;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect messagelog

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: messagelog; Tablespace: 
--

CREATE TABLE databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20)
);


ALTER TABLE public.databasechangelog OWNER TO messagelog;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: messagelog; Tablespace: 
--

CREATE TABLE databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO messagelog;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: messagelog
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO messagelog;

--
-- Name: last_archive_digest; Type: TABLE; Schema: public; Owner: messagelog; Tablespace: 
--

CREATE TABLE last_archive_digest (
    id bigint NOT NULL,
    digest text,
    filename character varying(255)
);


ALTER TABLE public.last_archive_digest OWNER TO messagelog;

--
-- Name: logrecord; Type: TABLE; Schema: public; Owner: messagelog; Tablespace: 
--

CREATE TABLE logrecord (
    id SERIAL NOT NULL,
    discriminator character varying(255) NOT NULL,
    "time" bigint,
    archived boolean,
    queryid character varying(255),
    message text,
    signature text,
    hashchain text,
    hashchainresult text,
    signaturehash text,
    timestamprecord bigint,
    timestamphashchain text,
    response boolean,
    "timestamp" text,
    memberclass character varying(255),
    membercode character varying(255),
    subsystemcode character varying(255)
);


ALTER TABLE public.logrecord OWNER TO messagelog;

--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: messagelog
--

COPY databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase) FROM stdin;
0-initial	toja	messagelog/0-initial.xml	2016-02-29 20:55:09.826905	1	EXECUTED	7:c3cca3ae1a32b9f905b114ea2c0bf7a2	createTable, addPrimaryKey, addForeignKeyConstraint, createSequence		\N	3.3.2
1-indices	toja	messagelog/1-indices.xml	2016-02-29 20:55:10.081111	2	EXECUTED	7:001bf6ffa7b837d49f2dd8347dcb5176	createIndex		\N	3.3.2
2-lastarchive	toja	messagelog/2-lastarchive.xml	2016-02-29 20:55:10.151274	3	EXECUTED	7:b2d8f22fe898ae2c0003d42e7388e464	createTable, addPrimaryKey		\N	3.3.2
3-clientids	toja	messagelog/3-clientids.xml	2016-02-29 20:55:10.214849	4	EXECUTED	7:4396021fa86fd035b0ec6f07420c690c	addColumn (x3)		\N	3.3.2
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: messagelog
--

COPY databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: messagelog
--

SELECT pg_catalog.setval('hibernate_sequence', 99, true);


--
-- Data for Name: last_archive_digest; Type: TABLE DATA; Schema: public; Owner: messagelog
--

COPY last_archive_digest (id, digest, filename) FROM stdin;
99	12342dbdc31966683831d021bceaae49a66538daefc33434033eb822ec98b080ee51bed32e339608762d823a3c2c9e1aa3d6dbd7c81ba526770e96473958b570	mlog-20160322154600-20160324220809-Ch3z0KW6we.zip
\.


--
-- Name: last_archive_digestpk; Type: CONSTRAINT; Schema: public; Owner: messagelog; Tablespace: 
--

ALTER TABLE ONLY last_archive_digest
    ADD CONSTRAINT last_archive_digestpk PRIMARY KEY (id);


--
-- Name: logrecordpk; Type: CONSTRAINT; Schema: public; Owner: messagelog; Tablespace: 
--

ALTER TABLE ONLY logrecord
    ADD CONSTRAINT logrecordpk PRIMARY KEY (id);


--
-- Name: pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: messagelog; Tablespace: 
--

ALTER TABLE ONLY databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: LOGRECORD_TIMESTAMPRECORD_fkey; Type: INDEX; Schema: public; Owner: messagelog; Tablespace: 
--

CREATE INDEX "LOGRECORD_TIMESTAMPRECORD_fkey" ON logrecord USING btree (timestamprecord);


--
-- Name: fk_qo6ack8sad6fqib90xghdaylh; Type: FK CONSTRAINT; Schema: public; Owner: messagelog
--

ALTER TABLE ONLY logrecord
    ADD CONSTRAINT fk_qo6ack8sad6fqib90xghdaylh FOREIGN KEY (timestamprecord) REFERENCES logrecord(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect serverconf

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = public, pg_catalog;

--
-- Name: changed_field_type; Type: TYPE; Schema: public; Owner: serverconf
--

CREATE TYPE changed_field_type AS (
	field_key text,
	field_value text
);


ALTER TYPE public.changed_field_type OWNER TO serverconf;

--
-- Name: add_history_rows(); Type: FUNCTION; Schema: public; Owner: serverconf
--

CREATE FUNCTION add_history_rows() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE
  _record_id bigint;
  _old_data hstore;
  _new_data hstore;
  _changed_fields hstore;
  _field_data changed_field_type;
  _user_name text;
  _operation text;

BEGIN
  IF TG_WHEN <> 'AFTER' THEN
    RAISE EXCEPTION 'add_history_rows() may only be used as an AFTER trigger';
  END IF;

  IF TG_LEVEL <> 'ROW' THEN
    RAISE EXCEPTION 'add_history_rows() may only be used as a row-level trigger';
  END IF;

  _operation := TG_OP::text;

  -- Detect the type of operation, the changed fields and the ID of the changed record.
  IF (_operation = 'UPDATE') THEN
    _changed_fields := (hstore(NEW.*) - hstore(OLD.*));
    IF _changed_fields = hstore('') THEN
      -- There are no changes to record in the history table.
      RETURN NULL;
    END IF;
    _old_data := hstore(OLD.*);
    _new_data := hstore(NEW.*);
    _record_id := OLD.id;
  ELSIF (_operation = 'DELETE') THEN
    _changed_fields := hstore(OLD.*);
    _old_data := _changed_fields;
    _record_id := OLD.id;
  ELSIF (_operation = 'INSERT') THEN
    _changed_fields := hstore(NEW.*);
    _new_data := _changed_fields;
    _record_id := NEW.id;
  ELSE
    RAISE EXCEPTION 'add_history_rows() supports only INSERT, UPDATE and DELETE';
  END IF;

  -- Detect the name of the user if present.
  BEGIN
    _user_name := current_setting('xroad.user_name');
  EXCEPTION WHEN undefined_object THEN
    _user_name := session_user::text;
  END;

  -- Fill and insert a history record for each changed field.
  FOR _field_data IN SELECT kv."key", kv."value" FROM each(_changed_fields) kv
  LOOP
    PERFORM insert_history_row(
      _user_name, _operation, TG_TABLE_NAME::text,
    _field_data, _old_data, _new_data, _record_id);
  END LOOP;

  RETURN NULL;
END;
$$;


ALTER FUNCTION public.add_history_rows() OWNER TO serverconf;

--
-- Name: insert_history_row(text, text, text, changed_field_type, hstore, hstore, bigint); Type: FUNCTION; Schema: public; Owner: serverconf
--

CREATE FUNCTION insert_history_row(user_name text, operation text, table_name text, field_data changed_field_type, old_data hstore, new_data hstore, record_id bigint) RETURNS void
    LANGUAGE plpgsql
    AS $$

DECLARE
  _history_row history;

BEGIN

  _history_row = ROW(
    NEXTVAL('history_id_seq'),
    operation, table_name, record_id,
    field_data.field_key, -- name of the field that was changed
    NULL, -- old value
    NULL, -- new value
    user_name,
    statement_timestamp()
  );

  IF (operation = 'UPDATE') THEN
    _history_row.old_value = old_data -> field_data.field_key;
    _history_row.new_value = field_data.field_value;
  ELSIF (operation = 'DELETE') THEN
    _history_row.old_value = old_data -> field_data.field_key;
  ELSIF (operation = 'INSERT') THEN
    _history_row.new_value = field_data.field_value;
  END IF;

  INSERT INTO history VALUES (_history_row.*);
END;
$$;


ALTER FUNCTION public.insert_history_row(user_name text, operation text, table_name text, field_data changed_field_type, old_data hstore, new_data hstore, record_id bigint) OWNER TO serverconf;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accessright; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE accessright (
    id bigint NOT NULL,
    subjectid bigint NOT NULL,
    rightsgiven timestamp without time zone NOT NULL,
    acl_id bigint,
    servicecode character varying(255) NOT NULL,
    client_id bigint
);


ALTER TABLE public.accessright OWNER TO serverconf;

--
-- Name: certificate; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE certificate (
    id bigint NOT NULL,
    data bytea,
    client_id bigint
);


ALTER TABLE public.certificate OWNER TO serverconf;

--
-- Name: client; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE client (
    id bigint NOT NULL,
    conf_id bigint,
    identifier bigint,
    clientstatus character varying(255),
    isauthentication character varying(255)
);


ALTER TABLE public.client OWNER TO serverconf;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20)
);


ALTER TABLE public.databasechangelog OWNER TO serverconf;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO serverconf;

--
-- Name: groupmember; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE groupmember (
    id bigint NOT NULL,
    groupmemberid bigint NOT NULL,
    added timestamp without time zone NOT NULL,
    localgroup_id bigint
);


ALTER TABLE public.groupmember OWNER TO serverconf;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: serverconf
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO serverconf;

--
-- Name: history; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE history (
    id bigint NOT NULL,
    operation character varying(255) NOT NULL,
    table_name character varying(255) NOT NULL,
    record_id bigint NOT NULL,
    field_name character varying(255) NOT NULL,
    old_value text,
    new_value text,
    user_name character varying(255) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


ALTER TABLE public.history OWNER TO serverconf;

--
-- Name: history_id_seq; Type: SEQUENCE; Schema: public; Owner: serverconf
--

CREATE SEQUENCE history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.history_id_seq OWNER TO serverconf;

--
-- Name: history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serverconf
--

ALTER SEQUENCE history_id_seq OWNED BY history.id;


--
-- Name: identifier; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE identifier (
    id bigint NOT NULL,
    discriminator character varying(255) NOT NULL,
    type character varying(255),
    xroadinstance character varying(255),
    memberclass character varying(255),
    membercode character varying(255),
    subsystemcode character varying(255),
    serviceversion character varying(255),
    servicecode character varying(255),
    groupcode character varying(255),
    securitycategory character varying(255),
    servercode character varying(255)
);


ALTER TABLE public.identifier OWNER TO serverconf;

--
-- Name: localgroup; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE localgroup (
    id bigint NOT NULL,
    groupcode character varying(255) NOT NULL,
    description character varying(255) NOT NULL,
    updated timestamp without time zone NOT NULL,
    client_id bigint
);


ALTER TABLE public.localgroup OWNER TO serverconf;

--
-- Name: serverconf; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE serverconf (
    id bigint NOT NULL,
    servercode character varying(255),
    owner bigint
);


ALTER TABLE public.serverconf OWNER TO serverconf;

--
-- Name: service; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE service (
    id bigint NOT NULL,
    wsdl_id bigint,
    servicecode character varying(255) NOT NULL,
    serviceversion character varying(255),
    title character varying(255),
    url character varying(255),
    sslauthentication boolean,
    timeout integer
);


ALTER TABLE public.service OWNER TO serverconf;

--
-- Name: service_securitycategories; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE service_securitycategories (
    service_id bigint NOT NULL,
    security_cat_id bigint NOT NULL,
    id bigint NOT NULL
);


ALTER TABLE public.service_securitycategories OWNER TO serverconf;

--
-- Name: service_securitycategories_id_seq; Type: SEQUENCE; Schema: public; Owner: serverconf
--

CREATE SEQUENCE service_securitycategories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_securitycategories_id_seq OWNER TO serverconf;

--
-- Name: service_securitycategories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: serverconf
--

ALTER SEQUENCE service_securitycategories_id_seq OWNED BY service_securitycategories.id;


--
-- Name: tsp; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE tsp (
    id bigint NOT NULL,
    name character varying(255),
    url character varying(255) NOT NULL,
    conf_id bigint
);


ALTER TABLE public.tsp OWNER TO serverconf;

--
-- Name: uiuser; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE uiuser (
    id bigint NOT NULL,
    username character varying(255) NOT NULL,
    locale character varying(255)
);


ALTER TABLE public.uiuser OWNER TO serverconf;

--
-- Name: wsdl; Type: TABLE; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE TABLE wsdl (
    id bigint NOT NULL,
    client_id bigint,
    url character varying(255) NOT NULL,
    wsdllocation character varying(255),
    disabled boolean NOT NULL,
    disablednotice character varying(255),
    refresheddate timestamp without time zone
);


ALTER TABLE public.wsdl OWNER TO serverconf;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY history ALTER COLUMN id SET DEFAULT nextval('history_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY service_securitycategories ALTER COLUMN id SET DEFAULT nextval('service_securitycategories_id_seq'::regclass);


--
-- Data for Name: accessright; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY accessright (id, subjectid, rightsgiven, acl_id, servicecode, client_id) FROM stdin;
17	11	2016-03-15 14:05:41.142	\N	getRandom	10
\.


--
-- Data for Name: certificate; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY certificate (id, data, client_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY client (id, conf_id, identifier, clientstatus, isauthentication) FROM stdin;
4	1	5	saved	NOSSL
6	1	7	saved	NOSSL
2	1	3	registered	NOSSL
10	1	11	registered	NOSSL
8	1	9	registered	NOSSL
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase) FROM stdin;
0-initial	toja	serverconf/0-initial.xml	2016-02-29 20:54:35.988787	1	EXECUTED	7:ca14efca02ab14532acef7618f68e490	createTable (x14), addPrimaryKey (x12), addForeignKeyConstraint (x16), createSequence		\N	3.3.2
1-indices	toja	serverconf/1-indices.xml	2016-02-29 20:54:36.302809	2	EXECUTED	7:10127a74cdfc6aac3ab66300605af658	createIndex (x16)		\N	3.3.2
2-wsdlpublish	toja	serverconf/2-wsdlpublish.xml	2016-02-29 20:54:36.330094	3	EXECUTED	7:979cf578aa74792d429496759c240594	dropColumn (x2)		\N	3.3.2
3-history	toja	serverconf/3-history.xml	2016-02-29 20:54:36.429939	4	EXECUTED	7:293454bdc936a6dd06069b9435f0bc90	createTable, addPrimaryKey, sql		\N	3.3.2
4.pre1-refactoring	toja	serverconf/4-refactoring.xml	2016-02-29 20:54:37.395065	5	MARK_RAN	7:e8e2c95031dfcbff167cad9f5b903cd3	dropUniqueConstraint		\N	3.3.2
4.pre2-refactoring	toja	serverconf/4-refactoring.xml	2016-02-29 20:54:37.734393	6	EXECUTED	7:7fc7c2a0871a74c25076678797a3fbc8	dropUniqueConstraint		\N	3.3.2
4-refactoring	toja	serverconf/4-refactoring.xml	2016-02-29 20:54:37.793655	7	EXECUTED	7:9e829c6d1eb9da7d9f7cfedaebffa173	renameTable, addColumn, addPrimaryKey, addForeignKeyConstraint, dropForeignKeyConstraint, sql, addNotNullConstraint, addColumn, addForeignKeyConstraint, sql, dropForeignKeyConstraint (x2), dropUniqueConstraint, dropTable, dropForeignKeyConstraint,...		\N	3.3.2
5-backendtype	toja	serverconf/5-backendtype.xml	2016-02-29 20:54:37.814259	8	EXECUTED	7:145b66704871152f2f9421e39efb19c7	dropColumn (x2)		\N	3.3.2
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
\.


--
-- Data for Name: groupmember; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY groupmember (id, groupmemberid, added, localgroup_id) FROM stdin;
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: serverconf
--

SELECT pg_catalog.setval('hibernate_sequence', 17, true);


--
-- Data for Name: history; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY history (id, operation, table_name, record_id, field_name, old_value, new_value, user_name, "timestamp") FROM stdin;
1	INSERT	serverconf	1	id	\N	1	xtee	2016-02-29 21:50:17.568167
2	INSERT	serverconf	1	owner	\N	\N	xtee	2016-02-29 21:50:17.568167
3	INSERT	serverconf	1	servercode	\N	xtee	xtee	2016-02-29 21:50:17.568167
4	INSERT	identifier	3	id	\N	3	xtee	2016-02-29 21:50:17.576949
5	INSERT	identifier	3	type	\N	MEMBER	xtee	2016-02-29 21:50:17.576949
6	INSERT	identifier	3	groupcode	\N	\N	xtee	2016-02-29 21:50:17.576949
7	INSERT	identifier	3	membercode	\N	TTUTUDENGID1	xtee	2016-02-29 21:50:17.576949
8	INSERT	identifier	3	servercode	\N	\N	xtee	2016-02-29 21:50:17.576949
9	INSERT	identifier	3	memberclass	\N	NGO	xtee	2016-02-29 21:50:17.576949
10	INSERT	identifier	3	servicecode	\N	\N	xtee	2016-02-29 21:50:17.576949
11	INSERT	identifier	3	discriminator	\N	C	xtee	2016-02-29 21:50:17.576949
12	INSERT	identifier	3	subsystemcode	\N	\N	xtee	2016-02-29 21:50:17.576949
13	INSERT	identifier	3	xroadinstance	\N	ee-dev	xtee	2016-02-29 21:50:17.576949
14	INSERT	identifier	3	serviceversion	\N	\N	xtee	2016-02-29 21:50:17.576949
15	INSERT	identifier	3	securitycategory	\N	\N	xtee	2016-02-29 21:50:17.576949
16	INSERT	client	2	id	\N	2	xtee	2016-02-29 21:50:17.5801
17	INSERT	client	2	conf_id	\N	1	xtee	2016-02-29 21:50:17.5801
18	INSERT	client	2	identifier	\N	3	xtee	2016-02-29 21:50:17.5801
19	INSERT	client	2	clientstatus	\N	saved	xtee	2016-02-29 21:50:17.5801
20	INSERT	client	2	isauthentication	\N	NOSSL	xtee	2016-02-29 21:50:17.5801
21	UPDATE	serverconf	1	owner	\N	2	xtee	2016-02-29 21:50:17.583805
22	INSERT	identifier	5	id	\N	5	xtee	2016-02-29 21:53:16.562887
23	INSERT	identifier	5	type	\N	SUBSYSTEM	xtee	2016-02-29 21:53:16.562887
24	INSERT	identifier	5	groupcode	\N	\N	xtee	2016-02-29 21:53:16.562887
25	INSERT	identifier	5	membercode	\N	70000002	xtee	2016-02-29 21:53:16.562887
26	INSERT	identifier	5	servercode	\N	\N	xtee	2016-02-29 21:53:16.562887
27	INSERT	identifier	5	memberclass	\N	GOV	xtee	2016-02-29 21:53:16.562887
28	INSERT	identifier	5	servicecode	\N	\N	xtee	2016-02-29 21:53:16.562887
29	INSERT	identifier	5	discriminator	\N	C	xtee	2016-02-29 21:53:16.562887
30	INSERT	identifier	5	subsystemcode	\N	generic-consumer	xtee	2016-02-29 21:53:16.562887
31	INSERT	identifier	5	xroadinstance	\N	ee-dev	xtee	2016-02-29 21:53:16.562887
32	INSERT	identifier	5	serviceversion	\N	\N	xtee	2016-02-29 21:53:16.562887
33	INSERT	identifier	5	securitycategory	\N	\N	xtee	2016-02-29 21:53:16.562887
34	INSERT	client	4	id	\N	4	xtee	2016-02-29 21:53:16.571836
35	INSERT	client	4	conf_id	\N	1	xtee	2016-02-29 21:53:16.571836
36	INSERT	client	4	identifier	\N	5	xtee	2016-02-29 21:53:16.571836
37	INSERT	client	4	clientstatus	\N	saved	xtee	2016-02-29 21:53:16.571836
38	INSERT	client	4	isauthentication	\N	NOSSL	xtee	2016-02-29 21:53:16.571836
39	INSERT	identifier	7	id	\N	7	xtee	2016-03-01 14:26:24.634292
40	INSERT	identifier	7	type	\N	SUBSYSTEM	xtee	2016-03-01 14:26:24.634292
41	INSERT	identifier	7	groupcode	\N	\N	xtee	2016-03-01 14:26:24.634292
42	INSERT	identifier	7	membercode	\N	TTUTUDENGID2	xtee	2016-03-01 14:26:24.634292
43	INSERT	identifier	7	servercode	\N	\N	xtee	2016-03-01 14:26:24.634292
44	INSERT	identifier	7	memberclass	\N	NGO	xtee	2016-03-01 14:26:24.634292
45	INSERT	identifier	7	servicecode	\N	\N	xtee	2016-03-01 14:26:24.634292
46	INSERT	identifier	7	discriminator	\N	C	xtee	2016-03-01 14:26:24.634292
47	INSERT	identifier	7	subsystemcode	\N	2	xtee	2016-03-01 14:26:24.634292
48	INSERT	identifier	7	xroadinstance	\N	ee-dev	xtee	2016-03-01 14:26:24.634292
49	INSERT	identifier	7	serviceversion	\N	\N	xtee	2016-03-01 14:26:24.634292
50	INSERT	identifier	7	securitycategory	\N	\N	xtee	2016-03-01 14:26:24.634292
51	INSERT	client	6	id	\N	6	xtee	2016-03-01 14:26:24.656939
52	INSERT	client	6	conf_id	\N	1	xtee	2016-03-01 14:26:24.656939
53	INSERT	client	6	identifier	\N	7	xtee	2016-03-01 14:26:24.656939
54	INSERT	client	6	clientstatus	\N	saved	xtee	2016-03-01 14:26:24.656939
55	INSERT	client	6	isauthentication	\N	NOSSL	xtee	2016-03-01 14:26:24.656939
56	INSERT	identifier	9	id	\N	9	xtee	2016-03-07 07:21:04.43791
57	INSERT	identifier	9	type	\N	SUBSYSTEM	xtee	2016-03-07 07:21:04.43791
58	INSERT	identifier	9	groupcode	\N	\N	xtee	2016-03-07 07:21:04.43791
59	INSERT	identifier	9	membercode	\N	TTUTUDENGID1	xtee	2016-03-07 07:21:04.43791
60	INSERT	identifier	9	servercode	\N	\N	xtee	2016-03-07 07:21:04.43791
61	INSERT	identifier	9	memberclass	\N	NGO	xtee	2016-03-07 07:21:04.43791
62	INSERT	identifier	9	servicecode	\N	\N	xtee	2016-03-07 07:21:04.43791
63	INSERT	identifier	9	discriminator	\N	C	xtee	2016-03-07 07:21:04.43791
64	INSERT	identifier	9	subsystemcode	\N	xtee2	xtee	2016-03-07 07:21:04.43791
65	INSERT	identifier	9	xroadinstance	\N	ee-dev	xtee	2016-03-07 07:21:04.43791
66	INSERT	identifier	9	serviceversion	\N	\N	xtee	2016-03-07 07:21:04.43791
67	INSERT	identifier	9	securitycategory	\N	\N	xtee	2016-03-07 07:21:04.43791
68	INSERT	client	8	id	\N	8	xtee	2016-03-07 07:21:04.502558
69	INSERT	client	8	conf_id	\N	1	xtee	2016-03-07 07:21:04.502558
70	INSERT	client	8	identifier	\N	9	xtee	2016-03-07 07:21:04.502558
71	INSERT	client	8	clientstatus	\N	saved	xtee	2016-03-07 07:21:04.502558
72	INSERT	client	8	isauthentication	\N	NOSSL	xtee	2016-03-07 07:21:04.502558
73	INSERT	identifier	11	id	\N	11	xtee	2016-03-07 08:18:38.621421
74	INSERT	identifier	11	type	\N	SUBSYSTEM	xtee	2016-03-07 08:18:38.621421
75	INSERT	identifier	11	groupcode	\N	\N	xtee	2016-03-07 08:18:38.621421
76	INSERT	identifier	11	membercode	\N	TTUTUDENGID1	xtee	2016-03-07 08:18:38.621421
77	INSERT	identifier	11	servercode	\N	\N	xtee	2016-03-07 08:18:38.621421
78	INSERT	identifier	11	memberclass	\N	NGO	xtee	2016-03-07 08:18:38.621421
79	INSERT	identifier	11	servicecode	\N	\N	xtee	2016-03-07 08:18:38.621421
80	INSERT	identifier	11	discriminator	\N	C	xtee	2016-03-07 08:18:38.621421
81	INSERT	identifier	11	subsystemcode	\N	xtee	xtee	2016-03-07 08:18:38.621421
82	INSERT	identifier	11	xroadinstance	\N	ee-dev	xtee	2016-03-07 08:18:38.621421
83	INSERT	identifier	11	serviceversion	\N	\N	xtee	2016-03-07 08:18:38.621421
84	INSERT	identifier	11	securitycategory	\N	\N	xtee	2016-03-07 08:18:38.621421
85	INSERT	client	10	id	\N	10	xtee	2016-03-07 08:18:38.631289
86	INSERT	client	10	conf_id	\N	1	xtee	2016-03-07 08:18:38.631289
87	INSERT	client	10	identifier	\N	11	xtee	2016-03-07 08:18:38.631289
88	INSERT	client	10	clientstatus	\N	saved	xtee	2016-03-07 08:18:38.631289
89	INSERT	client	10	isauthentication	\N	NOSSL	xtee	2016-03-07 08:18:38.631289
90	INSERT	wsdl	12	id	\N	12	xtee	2016-03-07 08:23:13.080141
91	INSERT	wsdl	12	url	\N	https://dl.dropboxusercontent.com/u/101439351/getRandomPortSoap11.wsdl	xtee	2016-03-07 08:23:13.080141
92	INSERT	wsdl	12	disabled	\N	t	xtee	2016-03-07 08:23:13.080141
93	INSERT	wsdl	12	client_id	\N	10	xtee	2016-03-07 08:23:13.080141
94	INSERT	wsdl	12	wsdllocation	\N	\N	xtee	2016-03-07 08:23:13.080141
95	INSERT	wsdl	12	refresheddate	\N	2016-03-07 08:23:11.746	xtee	2016-03-07 08:23:13.080141
96	INSERT	wsdl	12	disablednotice	\N	Out of order	xtee	2016-03-07 08:23:13.080141
97	INSERT	service	13	id	\N	13	xtee	2016-03-07 08:23:13.088012
98	INSERT	service	13	url	\N	http://localhost:8080/provider/services/getRandomPortSoap11	xtee	2016-03-07 08:23:13.088012
99	INSERT	service	13	title	\N	getRandom	xtee	2016-03-07 08:23:13.088012
100	INSERT	service	13	timeout	\N	60	xtee	2016-03-07 08:23:13.088012
101	INSERT	service	13	wsdl_id	\N	12	xtee	2016-03-07 08:23:13.088012
102	INSERT	service	13	servicecode	\N	getRandom	xtee	2016-03-07 08:23:13.088012
103	INSERT	service	13	serviceversion	\N	v1	xtee	2016-03-07 08:23:13.088012
104	INSERT	service	13	sslauthentication	\N	\N	xtee	2016-03-07 08:23:13.088012
105	UPDATE	wsdl	12	disabled	t	f	xtee	2016-03-07 08:24:44.964781
106	DELETE	service	13	id	13	\N	xtee	2016-03-07 08:47:46.592517
107	DELETE	service	13	url	http://localhost:8080/provider/services/getRandomPortSoap11	\N	xtee	2016-03-07 08:47:46.592517
108	DELETE	service	13	title	getRandom	\N	xtee	2016-03-07 08:47:46.592517
109	DELETE	service	13	timeout	60	\N	xtee	2016-03-07 08:47:46.592517
110	DELETE	service	13	wsdl_id	12	\N	xtee	2016-03-07 08:47:46.592517
111	DELETE	service	13	servicecode	getRandom	\N	xtee	2016-03-07 08:47:46.592517
112	DELETE	service	13	serviceversion	v1	\N	xtee	2016-03-07 08:47:46.592517
113	DELETE	service	13	sslauthentication	\N	\N	xtee	2016-03-07 08:47:46.592517
114	DELETE	wsdl	12	id	12	\N	xtee	2016-03-07 08:47:46.601978
115	DELETE	wsdl	12	url	https://dl.dropboxusercontent.com/u/101439351/getRandomPortSoap11.wsdl	\N	xtee	2016-03-07 08:47:46.601978
116	DELETE	wsdl	12	disabled	f	\N	xtee	2016-03-07 08:47:46.601978
117	DELETE	wsdl	12	client_id	10	\N	xtee	2016-03-07 08:47:46.601978
118	DELETE	wsdl	12	wsdllocation	\N	\N	xtee	2016-03-07 08:47:46.601978
119	DELETE	wsdl	12	refresheddate	2016-03-07 08:23:11.746	\N	xtee	2016-03-07 08:47:46.601978
120	DELETE	wsdl	12	disablednotice	Out of order	\N	xtee	2016-03-07 08:47:46.601978
121	INSERT	wsdl	14	id	\N	14	xtee	2016-03-07 08:47:50.944665
122	INSERT	wsdl	14	url	\N	https://dl.dropboxusercontent.com/u/101439351/getRandomPortSoap11.wsdl	xtee	2016-03-07 08:47:50.944665
123	INSERT	wsdl	14	disabled	\N	t	xtee	2016-03-07 08:47:50.944665
124	INSERT	wsdl	14	client_id	\N	10	xtee	2016-03-07 08:47:50.944665
125	INSERT	wsdl	14	wsdllocation	\N	\N	xtee	2016-03-07 08:47:50.944665
126	INSERT	wsdl	14	refresheddate	\N	2016-03-07 08:47:50.054	xtee	2016-03-07 08:47:50.944665
127	INSERT	wsdl	14	disablednotice	\N	Out of order	xtee	2016-03-07 08:47:50.944665
128	INSERT	service	15	id	\N	15	xtee	2016-03-07 08:47:50.947528
129	INSERT	service	15	url	\N	http://localhost:8085/provider/services/getRandomPortSoap11	xtee	2016-03-07 08:47:50.947528
130	INSERT	service	15	title	\N	getRandom	xtee	2016-03-07 08:47:50.947528
131	INSERT	service	15	timeout	\N	60	xtee	2016-03-07 08:47:50.947528
132	INSERT	service	15	wsdl_id	\N	14	xtee	2016-03-07 08:47:50.947528
133	INSERT	service	15	servicecode	\N	getRandom	xtee	2016-03-07 08:47:50.947528
134	INSERT	service	15	serviceversion	\N	v1	xtee	2016-03-07 08:47:50.947528
135	INSERT	service	15	sslauthentication	\N	\N	xtee	2016-03-07 08:47:50.947528
136	UPDATE	wsdl	14	disabled	t	f	xtee	2016-03-07 08:48:53.109448
137	UPDATE	client	2	clientstatus	saved	registered	serverconf	2016-03-11 22:42:56.185791
138	UPDATE	client	10	clientstatus	saved	registered	serverconf	2016-03-11 22:42:56.229511
139	INSERT	tsp	16	id	\N	16	xtee	2016-03-13 15:28:36.892896
140	INSERT	tsp	16	url	\N	http://demo.sk.ee/tsa/	xtee	2016-03-13 15:28:36.892896
141	INSERT	tsp	16	name	\N	/C=EE/O=AS Sertifitseerimiskeskus/OU=TSA/CN=DEMO of SK TSA 2014	xtee	2016-03-13 15:28:36.892896
142	INSERT	tsp	16	conf_id	\N	\N	xtee	2016-03-13 15:28:36.892896
143	UPDATE	tsp	16	conf_id	\N	1	xtee	2016-03-13 15:28:36.902219
144	INSERT	accessright	17	id	\N	17	xtee	2016-03-15 14:05:41.181219
145	INSERT	accessright	17	acl_id	\N	\N	xtee	2016-03-15 14:05:41.181219
146	INSERT	accessright	17	client_id	\N	\N	xtee	2016-03-15 14:05:41.181219
147	INSERT	accessright	17	subjectid	\N	11	xtee	2016-03-15 14:05:41.181219
148	INSERT	accessright	17	rightsgiven	\N	2016-03-15 14:05:41.142	xtee	2016-03-15 14:05:41.181219
149	INSERT	accessright	17	servicecode	\N	getRandom	xtee	2016-03-15 14:05:41.181219
150	UPDATE	accessright	17	client_id	\N	10	xtee	2016-03-15 14:05:41.208795
151	UPDATE	service	15	url	http://localhost:8085/provider/services/getRandomPortSoap11	http://192.168.33.1:8085/provider/services/getRandomPortSoap11	xtee	2016-03-15 14:21:36.921363
152	UPDATE	service	15	url	http://192.168.33.1:8085/provider/services/getRandomPortSoap11	http://10.1.10.1:8085/provider/services/getRandomPortSoap11	xtee	2016-03-15 14:23:21.88341
153	UPDATE	service	15	url	http://10.1.10.1:8085/provider/services/getRandomPortSoap11	http://10.1.10.1:8085/services/getRandomPortSoap11	xtee	2016-03-15 14:25:19.274685
154	UPDATE	service	15	url	http://10.1.10.1:8085/services/getRandomPortSoap11	http://10.1.10.1:8085/getRandomPortSoap11	xtee	2016-03-15 14:25:50.629767
155	UPDATE	service	15	url	http://10.1.10.1:8085/getRandomPortSoap11	http://10.1.10.1:8085/mockgetRandomPortSoap11	xtee	2016-03-15 14:28:07.714967
156	UPDATE	client	8	clientstatus	saved	registration in progress	xtee	2016-03-15 15:18:30.475749
157	UPDATE	client	8	clientstatus	registration in progress	registered	serverconf	2016-03-21 21:56:42.422659
\.


--
-- Name: history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serverconf
--

SELECT pg_catalog.setval('history_id_seq', 157, true);


--
-- Data for Name: identifier; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY identifier (id, discriminator, type, xroadinstance, memberclass, membercode, subsystemcode, serviceversion, servicecode, groupcode, securitycategory, servercode) FROM stdin;
3	C	MEMBER	ee-dev	NGO	TTUTUDENGID1	\N	\N	\N	\N	\N	\N
5	C	SUBSYSTEM	ee-dev	GOV	70000002	generic-consumer	\N	\N	\N	\N	\N
7	C	SUBSYSTEM	ee-dev	NGO	TTUTUDENGID2	2	\N	\N	\N	\N	\N
9	C	SUBSYSTEM	ee-dev	NGO	TTUTUDENGID1	xtee2	\N	\N	\N	\N	\N
11	C	SUBSYSTEM	ee-dev	NGO	TTUTUDENGID1	xtee	\N	\N	\N	\N	\N
\.


--
-- Data for Name: localgroup; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY localgroup (id, groupcode, description, updated, client_id) FROM stdin;
\.


--
-- Data for Name: serverconf; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY serverconf (id, servercode, owner) FROM stdin;
1	xtee	2
\.


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY service (id, wsdl_id, servicecode, serviceversion, title, url, sslauthentication, timeout) FROM stdin;
15	14	getRandom	v1	getRandom	http://10.1.10.1:8085/mockgetRandomPortSoap11	\N	60
\.


--
-- Data for Name: service_securitycategories; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY service_securitycategories (service_id, security_cat_id, id) FROM stdin;
\.


--
-- Name: service_securitycategories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: serverconf
--

SELECT pg_catalog.setval('service_securitycategories_id_seq', 1, false);


--
-- Data for Name: tsp; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY tsp (id, name, url, conf_id) FROM stdin;
16	/C=EE/O=AS Sertifitseerimiskeskus/OU=TSA/CN=DEMO of SK TSA 2014	http://demo.sk.ee/tsa/	1
\.


--
-- Data for Name: uiuser; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY uiuser (id, username, locale) FROM stdin;
\.


--
-- Data for Name: wsdl; Type: TABLE DATA; Schema: public; Owner: serverconf
--

COPY wsdl (id, client_id, url, wsdllocation, disabled, disablednotice, refresheddate) FROM stdin;
14	10	https://dl.dropboxusercontent.com/u/101439351/getRandomPortSoap11.wsdl	\N	f	Out of order	2016-03-07 08:47:50.054
\.


--
-- Name: accessrightpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY accessright
    ADD CONSTRAINT accessrightpk PRIMARY KEY (id);


--
-- Name: certificatepk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY certificate
    ADD CONSTRAINT certificatepk PRIMARY KEY (id);


--
-- Name: clientpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY client
    ADD CONSTRAINT clientpk PRIMARY KEY (id);


--
-- Name: groupmemberpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT groupmemberpk PRIMARY KEY (id);


--
-- Name: history_pkey; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY history
    ADD CONSTRAINT history_pkey PRIMARY KEY (id);


--
-- Name: identifierpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY identifier
    ADD CONSTRAINT identifierpk PRIMARY KEY (id);


--
-- Name: localgrouppk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY localgroup
    ADD CONSTRAINT localgrouppk PRIMARY KEY (id);


--
-- Name: pk_databasechangeloglock; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: serverconfpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY serverconf
    ADD CONSTRAINT serverconfpk PRIMARY KEY (id);


--
-- Name: service_securitycategories_id_key; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY service_securitycategories
    ADD CONSTRAINT service_securitycategories_id_key UNIQUE (id);


--
-- Name: servicepk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY service
    ADD CONSTRAINT servicepk PRIMARY KEY (id);


--
-- Name: tsppk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY tsp
    ADD CONSTRAINT tsppk PRIMARY KEY (id);


--
-- Name: uiuserpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY uiuser
    ADD CONSTRAINT uiuserpk PRIMARY KEY (id);


--
-- Name: wsdlpk; Type: CONSTRAINT; Schema: public; Owner: serverconf; Tablespace: 
--

ALTER TABLE ONLY wsdl
    ADD CONSTRAINT wsdlpk PRIMARY KEY (id);


--
-- Name: AUTHORIZEDSUBJECT_ACL_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "AUTHORIZEDSUBJECT_ACL_ID_fkey" ON accessright USING btree (acl_id);


--
-- Name: AUTHORIZEDSUBJECT_SUBJECTID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "AUTHORIZEDSUBJECT_SUBJECTID_fkey" ON accessright USING btree (subjectid);


--
-- Name: CLIENT_CONF_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "CLIENT_CONF_ID_fkey" ON client USING btree (conf_id);


--
-- Name: CLIENT_IDENTIFIER_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "CLIENT_IDENTIFIER_fkey" ON client USING btree (identifier);


--
-- Name: GROUPMEMBER_GROUPMEMBERID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "GROUPMEMBER_GROUPMEMBERID_fkey" ON groupmember USING btree (groupmemberid);


--
-- Name: GROUPMEMBER_LOCALGROUP_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "GROUPMEMBER_LOCALGROUP_ID_fkey" ON groupmember USING btree (localgroup_id);


--
-- Name: LOCALGROUP_CLIENT_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "LOCALGROUP_CLIENT_ID_fkey" ON localgroup USING btree (client_id);


--
-- Name: SERVERCONF_OWNER_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "SERVERCONF_OWNER_fkey" ON serverconf USING btree (owner);


--
-- Name: SERVICE_SECURITYCATEGORIES_SECURITY_CAT_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "SERVICE_SECURITYCATEGORIES_SECURITY_CAT_ID_fkey" ON service_securitycategories USING btree (security_cat_id);


--
-- Name: SERVICE_SECURITYCATEGORIES_SERVICE_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "SERVICE_SECURITYCATEGORIES_SERVICE_ID_fkey" ON service_securitycategories USING btree (service_id);


--
-- Name: SERVICE_WSDL_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "SERVICE_WSDL_ID_fkey" ON service USING btree (wsdl_id);


--
-- Name: TSP_CONF_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "TSP_CONF_ID_fkey" ON tsp USING btree (conf_id);


--
-- Name: WSDL_CLIENT_ID_fkey; Type: INDEX; Schema: public; Owner: serverconf; Tablespace: 
--

CREATE INDEX "WSDL_CLIENT_ID_fkey" ON wsdl USING btree (client_id);


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON accessright FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON certificate FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON client FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON groupmember FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON identifier FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON localgroup FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON serverconf FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON service FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON service_securitycategories FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON tsp FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON uiuser FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: update_history; Type: TRIGGER; Schema: public; Owner: serverconf
--

CREATE TRIGGER update_history AFTER INSERT OR DELETE OR UPDATE ON wsdl FOR EACH ROW EXECUTE PROCEDURE add_history_rows();


--
-- Name: fk_1feb23ww1ondj5dv8eu6gp86o; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY serverconf
    ADD CONSTRAINT fk_1feb23ww1ondj5dv8eu6gp86o FOREIGN KEY (owner) REFERENCES client(id);


--
-- Name: fk_3hbuo4vaqc9xpgq8ombxsfuqa; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY localgroup
    ADD CONSTRAINT fk_3hbuo4vaqc9xpgq8ombxsfuqa FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: fk_5vnw7dq6i9x7eob71bp7jym42; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY accessright
    ADD CONSTRAINT fk_5vnw7dq6i9x7eob71bp7jym42 FOREIGN KEY (subjectid) REFERENCES identifier(id);


--
-- Name: fk_7htiw4bes7513yvehdp9o1ccn; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT fk_7htiw4bes7513yvehdp9o1ccn FOREIGN KEY (groupmemberid) REFERENCES identifier(id);


--
-- Name: fk_9s9v7iqgb50l3n9xnxfia82p1; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY groupmember
    ADD CONSTRAINT fk_9s9v7iqgb50l3n9xnxfia82p1 FOREIGN KEY (localgroup_id) REFERENCES localgroup(id);


--
-- Name: fk_cs3rkn8ilbyf6hnoeto8xhu68; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY accessright
    ADD CONSTRAINT fk_cs3rkn8ilbyf6hnoeto8xhu68 FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: fk_i1xxlscwb84540u33au7ff9vb; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY wsdl
    ADD CONSTRAINT fk_i1xxlscwb84540u33au7ff9vb FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: fk_jug7e4jtiiojg492hh1i10qr0; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY service_securitycategories
    ADD CONSTRAINT fk_jug7e4jtiiojg492hh1i10qr0 FOREIGN KEY (security_cat_id) REFERENCES identifier(id);


--
-- Name: fk_or1xkhdc830058rh2kpdgjvd0; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY service_securitycategories
    ADD CONSTRAINT fk_or1xkhdc830058rh2kpdgjvd0 FOREIGN KEY (service_id) REFERENCES service(id);


--
-- Name: fk_p1n5e2yp385eusd471lon18lm; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY client
    ADD CONSTRAINT fk_p1n5e2yp385eusd471lon18lm FOREIGN KEY (conf_id) REFERENCES serverconf(id);


--
-- Name: fk_q2me40qtkcypwyh6ttrrpf5xf; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY service
    ADD CONSTRAINT fk_q2me40qtkcypwyh6ttrrpf5xf FOREIGN KEY (wsdl_id) REFERENCES wsdl(id);


--
-- Name: fk_sae6cb6emxn9iy5h56bq8ier2; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY tsp
    ADD CONSTRAINT fk_sae6cb6emxn9iy5h56bq8ier2 FOREIGN KEY (conf_id) REFERENCES serverconf(id);


--
-- Name: fk_sm6kk4lbadiiis4mj1ltp15h9; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY certificate
    ADD CONSTRAINT fk_sm6kk4lbadiiis4mj1ltp15h9 FOREIGN KEY (client_id) REFERENCES client(id);


--
-- Name: fk_th1b8viw7rfxp53t1b27oscv1; Type: FK CONSTRAINT; Schema: public; Owner: serverconf
--

ALTER TABLE ONLY client
    ADD CONSTRAINT fk_th1b8viw7rfxp53t1b27oscv1 FOREIGN KEY (identifier) REFERENCES identifier(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

