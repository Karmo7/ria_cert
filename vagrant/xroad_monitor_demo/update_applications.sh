#!/bin/bash

if [ ! -d ria_cert ]; then
  git clone --depth 1 https://Karmo7@bitbucket.org/Karmo7/ria_cert.git
fi

cd ria_cert
git pull
cd RecordlogCreator/
mvn clean compile assembly:single

cd ../xtee6parser/
mvn clean compile assembly:single
cd ../..