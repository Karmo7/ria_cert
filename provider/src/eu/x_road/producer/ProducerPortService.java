/**
 * ProducerPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.x_road.producer;

public interface ProducerPortService extends javax.xml.rpc.Service {
    public java.lang.String getgetRandomPortSoap11Address();

    public eu.x_road.producer.GetRandomPort getgetRandomPortSoap11() throws javax.xml.rpc.ServiceException;

    public eu.x_road.producer.GetRandomPort getgetRandomPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
