/**
 * ProducerPortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.x_road.producer;

public class ProducerPortServiceLocator extends org.apache.axis.client.Service implements eu.x_road.producer.ProducerPortService {

    public ProducerPortServiceLocator() {
    }


    public ProducerPortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProducerPortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for getRandomPortSoap11
    private java.lang.String getRandomPortSoap11_address = "http://localhost";

    public java.lang.String getgetRandomPortSoap11Address() {
        return getRandomPortSoap11_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String getRandomPortSoap11WSDDServiceName = "getRandomPortSoap11";

    public java.lang.String getgetRandomPortSoap11WSDDServiceName() {
        return getRandomPortSoap11WSDDServiceName;
    }

    public void setgetRandomPortSoap11WSDDServiceName(java.lang.String name) {
        getRandomPortSoap11WSDDServiceName = name;
    }

    public eu.x_road.producer.GetRandomPort getgetRandomPortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(getRandomPortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getgetRandomPortSoap11(endpoint);
    }

    public eu.x_road.producer.GetRandomPort getgetRandomPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            eu.x_road.producer.GetRandomPortSoap11Stub _stub = new eu.x_road.producer.GetRandomPortSoap11Stub(portAddress, this);
            _stub.setPortName(getgetRandomPortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setgetRandomPortSoap11EndpointAddress(java.lang.String address) {
        getRandomPortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (eu.x_road.producer.GetRandomPort.class.isAssignableFrom(serviceEndpointInterface)) {
                eu.x_road.producer.GetRandomPortSoap11Stub _stub = new eu.x_road.producer.GetRandomPortSoap11Stub(new java.net.URL(getRandomPortSoap11_address), this);
                _stub.setPortName(getgetRandomPortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("getRandomPortSoap11".equals(inputPortName)) {
            return getgetRandomPortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://producer.x-road.eu", "producerPortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://producer.x-road.eu", "getRandomPortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("getRandomPortSoap11".equals(portName)) {
            setgetRandomPortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
