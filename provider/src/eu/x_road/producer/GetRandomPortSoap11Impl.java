/**
 * GetRandomPortSoap11Impl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.x_road.producer;

import java.util.Random;

public class GetRandomPortSoap11Impl implements eu.x_road.producer.GetRandomPort{
    public eu.x_road.producer.GetRandomResponseResponse getRandom(eu.x_road.producer.GetRandomRequest request) throws java.rmi.RemoteException {
        return new GetRandomResponseResponse("Random long: " +  new Random().nextLong());
    }

}
