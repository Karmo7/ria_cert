/**
 * GetRandomPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.x_road.producer;

public interface GetRandomPort extends java.rmi.Remote {

    /**

     */
    public eu.x_road.producer.GetRandomResponseResponse getRandom(eu.x_road.producer.GetRandomRequest request) throws java.rmi.RemoteException;
}
