/**
 * GetRandomPortSoap11Skeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.x_road.producer;

public class GetRandomPortSoap11Skeleton implements eu.x_road.producer.GetRandomPort, org.apache.axis.wsdl.Skeleton {
    private eu.x_road.producer.GetRandomPort impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://producer.x-road.eu", ">>getRandom>request"), eu.x_road.producer.GetRandomRequest.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("getRandom", _params, new javax.xml.namespace.QName("", "response"));
        _oper.setReturnType(new javax.xml.namespace.QName("http://producer.x-road.eu", ">>getRandomResponse>response"));
        _oper.setElementQName(new javax.xml.namespace.QName("http://producer.x-road.eu", "getRandom"));
        _oper.setSoapAction("");
        _myOperationsList.add(_oper);
        if (_myOperations.get("getRandom") == null) {
            _myOperations.put("getRandom", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("getRandom")).add(_oper);
    }

    public GetRandomPortSoap11Skeleton() {
        this.impl = new eu.x_road.producer.GetRandomPortSoap11Impl();
    }

    public GetRandomPortSoap11Skeleton(eu.x_road.producer.GetRandomPort impl) {
        this.impl = impl;
    }
    public eu.x_road.producer.GetRandomResponseResponse getRandom(eu.x_road.producer.GetRandomRequest request) throws java.rmi.RemoteException
    {
        eu.x_road.producer.GetRandomResponseResponse ret = impl.getRandom(request);
        return ret;
    }

}
