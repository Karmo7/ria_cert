The goals of this project are described in `documentation/summary.pdf`

In this project:

* deb-package – Installs X-Road monitor with its dependencies
* documentation – Description of project goals and development methods
* provider – For sending test data to X-Road security server. Hasn't been used much, was replaced with `soap_ui_project_testing_security_server.xml`
* RecordLogCreator – Simulates X-Road security server (creates logs). Can be used to test the monitor software without setting up X-Road security server
* vagrant – Several Vagrant scripts for setting up test VMs. Also contains a demo project.
* xtee6parser – source code for the main application: X-Road monitor
